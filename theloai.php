<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thể loại</title>
    <?php require_once('Layout_page/Layout_file_top.php'); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>
</head>
<body>
    <?php 
        require_once('Layout_page/Layout_header.php');  
        require_once('./Model/config.php');
    ?>
    
    <!-- LATEST MOVIES SECTION -->
    <div class="section">
        <div class="container">
            <div class="section-header">
                <span class="col-md-4 col-sm-12 text-sm">Thể loại</span>
                <div class="input-group mx-2 mx-md-0">
                    <input type="text" class="form-control" placeholder="Search" id="search">
                    <button class="btn btn-outline-orange" type="button" id="button-addon2">Tìm kiếm</button>
                </div>
            </div>
            <div class="row noidung">
                <!-- MOVIE ITEM -->
                <?php
                    if(!isset($_GET['page'])){
                        $_GET['page'] = 1;
                    }
                    $rowPerPage = 6;
                    // vị trí của mẩu tin đầu tiên trên mỗi trang
                    $offset = ($_GET['page'] - 1) * $rowPerPage;
                    $ten_tl = $_GET["ten_tl"];

                    $query = "SELECT * FROM phim,the_loai WHERE phim.ID_TL = the_loai.ID_TL AND Ten_TL = '$ten_tl' LIMIT $offset, $rowPerPage";
                    $result = $conn->query($query);

                    if(!$result) echo 'Câu truy vấn bị lỗi';
                   

                    if($result->num_rows != 0){
                        while($row = $result->fetch_array()) { ?>
                            <div class="col-md-2 col-sm-6 mt-3">
                                <a href="details.php?id_phim=<?= $row['ID_phim'] ?>" class="movie-item">
                                    <img src="./images/<?= $row['Hinh'] ?>" alt="">
                                    <div class="movie-item-content">
                                        <div class="movie-item-title mx-3">
                                            <?= $row['Ten_phim'] ?>
                                        </div>
                                        <div class="movie-infos mx-3">
                                            <div class="movie-info">
                                                <i class="bx bxs-star"></i>
                                                <span>9.5</span>
                                            </div>
                                            <div class="movie-info">
                                                <i class="bx bxs-time"></i>
                                                <span><?= $row['Thoi_luong'] ?></span>
                                            </div>
                                            <div class="movie-info">
                                                <span>HD</span>
                                            </div>
                                            <div class="movie-info">
                                                <span>16+</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php }
                    }
                ?>
                <!-- END MOVIE ITEM -->
            </div>
        </div>
    </div>
    <!-- END LATEST MOVIES SECTION -->
    
    <?php
        $re = $conn->query("SELECT * FROM phim,the_loai WHERE phim.ID_TL = the_loai.ID_TL AND Ten_TL = '$ten_tl' ");
        $numRows = mysqli_num_rows($re);
        $maxPage = ceil($numRows/$rowPerPage);
    ?>

    <nav class="d-flex justify-content-end p-4" aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item">
                <?php
                    if($_GET["page"] > 1){ ?>
                        <a class="page-link" href="theloai.php?ten_tl=<?= $ten_tl ?>&page=<?= $_GET['page'] - 1 ?>" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    <?php }
                ?>
            </li>
            <?php 
                for ($i=1 ; $i<=$maxPage ; $i++)
                {
                    if($i == $_GET['page']){ ?>
                        <li class="page-item">
                            <a class="page-link active" href="theloai.php?ten_tl=<?= $ten_tl ?>&page=<?= $i ?>"><?= $i ?></a> <!-- trang hiện tại sẽ được style -->
                        </li>
                    <?php }
                    else { ?>
                        <li class="page-item">
                            <a class="page-link" href="theloai.php?ten_tl=<?= $ten_tl ?>&page=<?= $i ?>"><?= $i ?></a> 
                        </li>
                    <?php }
                }
            ?>
            <li class="page-item">
                <?php
                    if($_GET["page"] < $maxPage){ ?>
                        <a class="page-link" href="theloai.php?ten_tl=<?= $ten_tl ?>&page=<?= $_GET['page'] + 1 ?>" aria-label="Previous">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    <?php }
                    
                    $conn->close();
                ?>
            </li>
        </ul>
    </nav>
    
    <?php require_once('Layout_page/Layout_footer.php');  ?>
</body>
</html>