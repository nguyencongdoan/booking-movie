<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hệ thống rạp chiếu phim</title>
    <?php require_once('Layout_page/Layout_file_top.php'); ?>
</head>
<body>
    <?php 
        require_once('Layout_page/Layout_header.php'); 
        require_once('./Model/config.php');
    ?>
    
    <div class="container" style="max-width: 1250px;">
        <div class="row">
            <h2 class="fw-bold mt-4 text-title">Hệ thống rạp chiếu phim</h2>
            <div class="col-md-12">
                <ul class="nav nav-pills mb-3 col-md-12 col-sm-12" id="pills-tab" role="tablist" style="justify-content:center;">
                    <li class="nav-item mt-3" role="presentation">
                        <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-hcm" 
                            type="button" role="tab" aria-controls="pills-hcm" aria-selected="true">
                            <box-icon name='map' animation='flashing' color='rgba(178,0,0,0.86)' ></box-icon>
                            TP Hồ Chí Minh
                        </button>
                    </li>
                    <li class="nav-item mt-3" role="presentation">
                        <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-dn" 
                            type="button" role="tab" aria-controls="pills-dn" aria-selected="false">
                            <box-icon name='map' animation='flashing' color='rgba(178,0,0,0.86)' ></box-icon>
                            TP Đà Nẵng
                        </button>
                    </li>
                    <li class="nav-item mt-3" role="presentation">
                        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-hn" 
                            type="button" role="tab" aria-controls="pills-hn" aria-selected="false">
                            <box-icon name='map' animation='flashing' color='rgba(178,0,0,0.86)' ></box-icon>
                            Hà Nội
                        </button>
                    </li>
                </ul>
            </div>
            <div class="tab-content mt-4 mt-md-0" id="pills-tabContent">
                <!-- Tab TP Hồ Chí Minh -->
                <div class="tab-pane mt-4 mt-md-0 fade show active" id="pills-hcm" role="tabpanel" aria-labelledby="pills-hcm-tab">
                    <div class="row">
                        <?php
                            $query = "SELECT * FROM rap_phim WHERE Dia_chi LIKE '%Tp. HỒ Chí Minh%' ";
                            $result = $conn->query($query);

                            if(!$result) echo 'Câu truy vấn bị lỗi';

                            if($result->num_rows != 0){
                                while($row = $result->fetch_array()) { ?>
                                    <div class="card col-md-r-2 col-sm-r-12 mt-4 mx-4 mx-md-3">
                                        <h5 class="fw-bold card-header"><?= $row['Ten_rap']; ?></h4>
                                        <div class="card-body">
                                            <?= $row['Dia_chi']; ?>
                                        </div>
                                        <div class="card-footer">
                                            <a href="details_cinema.php?id_rap=<?= $row['ID_Rap']; ?>" class="fw-bold">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                    </div>
                                <?php }
                            }
                        ?>
                    </div>
                </div>
                <!-- End Tab TP Hồ Chí Minh -->
               

                <!-- Tab TP Đà Nẵng -->
                <div class="tab-pane fade mt-4 mt-md-0" id="pills-dn" role="tabpanel" aria-labelledby="pills-dn-tab">
                    <div class="row">
                        <?php
                            $query = "SELECT * FROM rap_phim WHERE Dia_chi LIKE '%Tp. Đà Nẵng%' ";
                            $result = $conn->query($query);

                            if(!$result) echo 'Câu truy vấn bị lỗi';

                            if($result->num_rows != 0){
                                while($row = $result->fetch_array()) { ?>
                                    <div class="card col-md-r-2 col-sm-r-12 mt-4 mx-4 mx-md-3">
                                        <h5 class="fw-bold card-header"><?= $row['Ten_rap']; ?></h4>
                                        <div class="card-body">
                                            <?= $row['Dia_chi']; ?>
                                        </div>
                                        <div class="card-footer">
                                            <a href="details_cinema.php?id_rap=<?= $row['ID_Rap']; ?>" class="fw-bold">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                    </div>
                                <?php }
                            }
                        ?>
                    </div>
                </div>
                <!-- End Tab TP Đà Nẵng -->

                <!-- Tab Hà Nội -->
                <div class="tab-pane fade mt-4 mt-md-0 mt-md-0" id="pills-hn" role="tabpanel" aria-labelledby="pills-hn-tab">
                    <div class="row">
                        <?php
                            $query = "SELECT * FROM rap_phim WHERE Dia_chi LIKE '%Tp. Hà Nội%' ";
                            $result = $conn->query($query);

                            if(!$result) echo 'Câu truy vấn bị lỗi';

                            if($result->num_rows != 0){
                                while($row = $result->fetch_array()) { ?>
                                    <div class="card col-md-r-2 col-sm-r-12 mt-4 mx-4 mx-md-3">
                                        <h5 class="fw-bold card-header"><?= $row['Ten_rap']; ?></h4>
                                        <div class="card-body">
                                            <?= $row['Dia_chi']; ?>
                                        </div>
                                        <div class="card-footer">
                                            <a href="details_cinema.php?id_rap=<?= $row['ID_Rap']; ?>" class="fw-bold">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                    </div>
                                <?php }
                            }
                            $conn->close();
                        ?>
                    </div>
                </div>
            </div>
            <!-- End Tab Hà Nội -->

            <!-- Khuyến mãi -->
            <nav class="mt-4">
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <button class="nav-link active nav-active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" 
                        type="button" role="tab" aria-controls="nav-home" aria-selected="true">
                        Khuyến mãi
                    </button>
                    <span style="color:red; font-style: 40px;">|</span>
                    <button class="nav-link nav-active" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" 
                        type="button" role="tab" aria-controls="nav-profile" aria-selected="false">
                        Sự kiện
                    </button>
                </div>
             </nav>
            <div class="tab-content mt-4" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="row">
                        <!-- Tab khuyến mãi -->
                        <div class="col-md-3 col-sm-12 mx-5 mt-3 mx-md-0 mt-md-0">
                            <img src="https://www.bhdstar.vn/wp-content/uploads/2018/03/BHD-Star-ScanQR-710x320.jpg" class="img-sk" alt="">
                        </div>
                        <div class="col-md-3 col-sm-12 mx-5 mt-3 mx-md-0 mt-md-0">
                            <img src="https://www.bhdstar.vn/wp-content/uploads/2018/03/admin-ajax.jpeg" class="img-sk" alt="">
                        </div>
                        <div class="col-md-3 col-sm-12 mx-5 mt-3 mx-md-0 mt-md-0">
                            <img src="https://www.bhdstar.vn/wp-content/uploads/2017/03/BHDStar-Movie365_710x320.jpg" class="img-sk" alt="">
                        </div>
                        <div class="col-md-3 col-sm-12 mx-5 mt-3 mx-md-0 mt-md-0">
                            <img src="https://www.bhdstar.vn/wp-content/uploads/2018/12/BHD-Star-FBCover-Onyx-710x320.jpg" class="img-sk" alt="">
                        </div>
                        <!-- END Tab khuyến mãi -->
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <div class="row">
                        <!-- Tab sự kiện -->
                        <div class="col-md-3 col-sm-12 mx-5 mt-3 mx-md-0 mt-md-0">
                            <img src="https://www.bhdstar.vn/wp-content/uploads/2018/03/BHD-Star-ScanQR-710x320.jpg" class="img-sk" alt="">
                        </div>
                        <div class="col-md-3 col-sm-12 mx-5 mt-3 mx-md-0 mt-md-0">
                            <img src="https://www.bhdstar.vn/wp-content/uploads/2018/03/BHD-Star-APP-710x320.jpg" class="img-sk" alt="">
                        </div>
                        <div class="col-md-3 col-sm-12 mx-5 mt-3 mx-md-0 mt-md-0">
                            <img src="https://www.bhdstar.vn/wp-content/uploads/2017/10/BHD-Star_Hotline_710x320-2.jpg" class="img-sk" alt="">
                        </div>
                        <div class="col-md-3 col-sm-12 mx-5 mt-3 mx-md-0 mt-md-0">
                            <img src="https://www.bhdstar.vn/wp-content/uploads/2018/12/BHD-Star-FBCover-Onyx-710x320.jpg" class="img-sk" alt="">
                        </div>
                        <!-- END Tab sự kiện -->
                    </div>
                </div>
             </div>
            <!-- End khuyến mãi -->
        </div>
    </div>

    <?php require_once('Layout_page/Layout_footer.php');  ?>
</body>
</html>