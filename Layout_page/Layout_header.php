 
    <?php  
        include('./Model/config.php'); 
        include('./Model/action.php'); 
    ?>
    
    <!-- NAV -->
    <div class="nav-wrapper">
        <div class="container">
            <div class="nav">
                <a href="index.php" class="logo">
                    <i class='bx bx-movie-play bx-tada main-color'></i>Fl<span class="main-color">i</span>x
                </a>
                <ul class="nav-menu mt-2" id="nav-menu">
                    <li><a href="index.php">Trang chủ</a></li>
                    <li><a href="lichchieu.php">Lịch chiếu</a></li>
                    <li><a href="cinema.php">Rạp chiếu phim</a></li>
                    <li><a href="movie_play.php">Phim đang chiếu</a></li>
                    <li class="dropdown" style="z-index: 99999999;">
                        <a class="dropdown-toggle" href="#" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                            Thể loại
                        </a>
                        <div class="dropdown-menu menu" aria-labelledby="dropdownMenuLink">
                            <?php 
                                require_once('./Model/config.php');
                                $query = "SELECT * FROM the_loai";
                                $result = $conn->query($query);
                                if(!$result) echo "Câu truy vấn bị lỗi";

                                if($result->num_rows != 0){
                                    while($row = $result->fetch_array()) { ?>
                                        <a class="dropdown-item" href="theloai.php?ten_tl=<?=$row['Ten_TL']?>"><?= $row['Ten_TL'] ?></a>
                                    <?php }
                                }
                            ?>
                        </div>
                    </li>
                    
                    <?php
                        if(isset($_SESSION['HoTen'])){ ?>
                            <li class="dropdown">
                                <a class="dropdown-toggle" href="#" data-bs-toggle="dropdown">
                                    <img src="./images/<?= $_SESSION['Hinh'] ?>" style="width: 40px; height: 40px; border-radius: 50%;" alt="">
                                </a>
                                <div class="dropdown-menu menu" aria-labelledby="baitap">
                                    <a class="dropdown-item" href="#">Hi: <?= $_SESSION['HoTen'] ?> </a>
                                    <a class="dropdown-item" href="./thongtin_user.php?id_kh=<?= $_SESSION['ID_User'] ?>">Quản lý tài khoản</a>
                                    <a class="dropdown-item" href="login.php?logout">
                                        <img src="./images/logout.png" width="20px" height="20px" alt="">
                                        Đăng xuất
                                    </a>
                                </div>
                            </li>
                        <?php }
                        else { ?>
                            <li>
                                <a href="login.php" class="btn btn-hover">
                                    <span>Đăng nhập</span>
                                </a>
                            </li>
                            <li>
                                <a href="register.php" class="btn btn-hover">
                                    <span>Đăng ký</span>
                                </a>
                            </li>
                        <?php }
                    ?>
                </ul>
                <!-- MOBILE MENU TOGGLE -->
                <div class="hamburger-menu" id="hamburger-menu">
                    <div class="hamburger"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- END NAV -->