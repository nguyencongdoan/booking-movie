<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail Movie</title>
    <?php require_once('Layout_page/Layout_file_top.php'); ?>
</head>
<body>
    <?php 
        require_once('./Model/config.php');
        require_once('Layout_page/Layout_header.php'); 

        if(isset($_GET['id_phim'])){
            $id_phim = $_GET['id_phim'];
            
            // thêm phim vao danh sách đang xem;
            if(isset($_SESSION['ID_User'])){ // nếu có tài khoản user mới thêm vào danh sách
                $id_kh = $_SESSION['ID_User'];
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $date = date('Y-m-d H:i:s'); 
                
                // kiểm tra nếu phim đã tồn tại trong danh sách rồi thì không thêm nữa
                $ds_phim = "SELECT * FROM ds_daxem WHERE ID_Phim = '$id_phim' AND ID_KH = '$id_kh'";
                $ds = $conn->query($ds_phim);
                if(!$ds) echo "Câu truy vấn bị lỗi";

                if($ds->num_rows == 0){
                    $query_dsxem = "INSERT INTO ds_daxem (ID_KH, ID_Phim, Ngay_xem) VALUES ('$id_kh', '$id_phim', '$date')";
                    $conn->query($query_dsxem);
                }
            }
            
            $query = "SELECT * FROM phim, the_loai WHERE phim.ID_TL = the_loai.ID_TL AND ID_Phim = '$id_phim'";
            $result = $conn->query($query);
    
            if(!$result) echo 'Cau truy van bi sai';
            $row = $result->fetch_assoc();
    
            $ten_phim = $row['Ten_phim'];
            $tom_tat = $row['Tom_tat'];
            $thoi_luong = $row['Thoi_luong'];
            $dien_vien = $row['Dien_vien'];
            $dao_dien = $row['Dao_dien'];
            $the_loai = $row['Ten_TL'];
            $quoc_gia = $row['Quoc_gia'];
            $nam_phat_hanh = $row['Nam_phat_hanh'];

            if(isset($_GET['id_tap'])) $id_tap = $_GET['id_tap'];
        }
    ?>

    <!-- Main -->
    <div class="container" style="max-width: 1240px;">
        <div class="row">
            <div class="col-md-7 col-sm-12 mt-5 main-left">
                <?php
                    $query = "SELECT * FROM tap_phim, phim WHERE Phim.ID_phim = tap_phim.ID_Phim AND phim.ID_phim = '$id_phim' LIMIT 1";
                    $result = $conn->query($query);
                    if(!$result) echo "Câu truy vấn bị lỗi";
                    $row = $result->fetch_assoc();
                    $url = $row['Url_video'];
                ?>
                <video src="./videos/<?= $url ?>" controls></video>
            </div>

            <div class="col-md-5 col-sm-12 mt-5 main-right">
                <h3>
                    <?= $ten_phim; ?>
                </h2>
                <p style="line-height: 24px;" class="text-justify text-main-p">
                    <?= $tom_tat; ?>
                </p>
                <div class="text-p mt-4">
                    <p>Thời lượng: <?= $thoi_luong; ?></p>
                    <p>Số tập: 6/6</p>
                    <p>Diễn viên: <?= $dien_vien; ?></p>
                    <p>Thể loại: <?= $the_loai; ?></p>
                    <p>Đạo diễn: <?= $dao_dien; ?></p>
                    <p>Quốc gia: <?= $quoc_gia; ?></p>
                    <p>Năm phát hành: <?= $nam_phat_hanh; ?></p>
                </div>
                <div class="item-action delay-6">
                    <?php
                        if($the_loai == 'Phim chiếu rạp'){ ?>
                            <a href="dat_ve.php?id_phim=<?php echo $id_phim; ?>" class="btn btn-buy" type="button">
                                Đặt vé ngay
                            </a>
                        <?php } else { ?>
                            <a href="#" class="btn btn-buy" type="button">
                                <box-icon name='play-circle' animation='tada' style="margin-right: 10px; font-size: 20px;" rotate='90' color='#c0392b' ></box-icon>
                                Xem phim
                            </a>
                        <?php }
                    ?>
                    <a href="#" class="btn btn-buy mx-2" type="button">
                        <i class="bx bxs-video"></i>
                        <span class="text-watch">Xem trailer</span>
                    </a> 
                </div>
            </div>
            <div class="col-md-12 menu-item">
                <button class="btn btn-trailer">
                    <i class="bx bxs-like"></i>
                    <span>Yêu thích</span>
                </button>   
                <button class="btn btn-trailer">
                    <i class="bx bxs-dislike"></i>
                    <span>Không thích</span>
                </button>
                <button class="btn btn-trailer">
                    <i class="bx bxs-alarm-add"></i>
                    <span>Xem sau</span>
                </button>
                <button class="btn btn-trailer">
                    <box-icon name='share' type='solid' flip='horizontal' color='#faf6f6' ></box-icon>
                    <span style="margin-left: 16px;">Chia sẻ</span>
                </button>
            </div>
            <div class="mt-2">
                <h3 class="section-h3 col-md-1 col-sm-2">Các tập</h3>
            </div>
            <div class="col-md-12">
                <?php 
                    $query = "SELECT * FROM tap_phim, phim WHERE Phim.ID_phim = tap_phim.ID_Phim AND phim.ID_phim = '$id_phim'";
                    $result = $conn->query($query);
                    if(!$result) echo "Câu truy vấn bị lỗi";

                    if($result->num_rows != 0) {
                        while($row = $result->fetch_array()) { 
                            if(isset($id_tap) && $row['ID_Tap'] == $id_tap) { ?>
                               <a href="details.php?id_phim=<?= $id_phim ?>&id_tap=<?= $row['ID_Tap'] ?>" class="btn btn-watch col-md-1 col-sm-1 mt-2" style="width:114px; height:40px; margin-left: 2px;">
                                    <box-icon name='play-circle' animation='tada' rotate='90' color='red' style="margin-right: 2px;" ></box-icon>
                                    <span style="font-size:13px; margin:auto;"><?= $row['Ten_tap'] ?></span>
                                </a>
                            <?php }
                            else { ?>
                                 <a href="details.php?id_phim=<?= $id_phim ?>&id_tap=<?= $row['ID_Tap'] ?>" class="btn btn-watch col-md-1 col-sm-1 mt-2" style="width:114px; height:40px; margin-left: 2px;">
                                    <span style="font-size:13px; margin: auto;"><?= $row['Ten_tap'] ?></span>
                                </a>
                            <?php }
                        }
                    }
                ?>
            </div>

            <!-- Bình luận -->
            <div class="col-md-7 col-sm-12 mt-4 section-content">
                <form action="./Model/action_binhluan.php" method="post">
                    <div class="section-bl col-md-12">
                        <p class="mt-1" class="text-start">20 bình luận</p>
                        <p class="mt-1" style="margin-left: auto;">Sắp xếp theo:</p>
                        <select class="form-select-d" aria-label="Default select example">
                            <option selected>Mới nhất</option>
                            <option value="1">Nhiều like nhất</option>
                            <option value="2">Cũ nhất</option>
                        </select>
                    </div>
                    <div class="section-nd col-md-12 mt-3">
                        <img src="./images/<?php if(isset($_SESSION['Hinh'])) echo $_SESSION["Hinh"]; ?>"/>
                        <input type="text" name="noidung" placeholder=" Viết bình luận">
                        <input type="hidden" name="id_phim" value="<?= $id_phim ?>">
                        <input type="hidden" name="id_kh" value="<?= $id_kh ?>">
                        <button type="submit" name="create" class="btn">
                            <box-icon name='send' color='#fdfdfd' ></box-icon>
                        </button>
                    </div>
                    <div class="binhluan">
                        <?php
                            $query = "SELECT NOW() - Ngay thoigian, Ngay, Hinh, Ho_ten, Noi_dung FROM binh_luan, khach_hang WHERE binh_luan.ID_KH = khach_hang.ID_KH AND ID_Phim = '$id_phim'";
                            $re_bl = $conn->query($query);
                            if(!$re_bl) echo "Câu truy vấn bị lỗi";

                            if($re_bl->num_rows != 0){
                                while($row = $re_bl->fetch_array()){ 
                                    ?>
                                    <div class="col-md-12">
                                        <div class="section-nd col-md-12 mt-4">
                                            <img src="./images/<?= $row['Hinh'] ?>"/>
                                            <p class="fw-bold"><?= $row['Ho_ten'] ?></p>
                                            <span><?= $row['Ngay'] ?></span><br/>
                                            <p class="text-nd"><?= $row['Noi_dung'] ?></p>
                                        </div>
                                        <div style="display: inline-flex">
                                            <button class="btn section-btn" type="button">
                                                <i class="bx bxs-like"></i> 
                                            </button>
                                            <span>0</span>
                                            <button class="btn section-btn m-0" type="button">
                                                <i class="bx bxs-chat"></i> 
                                            </button>
                                            <span>0</span>
                                            <button class="btn section-btn m-0" type="button">
                                                <i class="bx bxs-flag-alt"></i> 
                                            </button>
                                        </div>
                                    </div>
                                <?php }
                            }
                        ?>
                    </div>
                </form>
            </div>
            <!-- End bình luận -->

            <!-- Nội dung tương tự -->
            <div class="col-md-12">
                <!-- LATEST MOVIES SECTION -->
                <div class="section">
                    <div class="">
                        <div class="section-header">
                            Phim tương tự
                        </div>
                        <div class="movies-slide carousel-nav-center owl-carousel">
                            <!-- MOVIE ITEM -->
                            <?php 
                                $query = "SELECT * FROM phim, the_loai WHERE phim.ID_TL = the_loai.ID_TL AND Ten_TL = '$the_loai' AND ID_Phim != '$id_phim'";
                                $result = $conn->query($query);
                        
                                if(!$result) echo 'Cau truy van bi sai';
                                
                                if($result->num_rows != 0){
                                    while($row = $result->fetch_array()) { ?>
                                        <a href="details.php?id_phim=<?= $row['ID_phim'] ?>" class="movie-item">
                                            <img src="./images/<?= $row['Hinh'] ?>" alt="">
                                            <div class="movie-item-content">
                                                <div class="movie-item-title mx-3">
                                                    <?= $row['Ten_phim'] ?>
                                                </div>
                                                <div class="movie-infos mx-3">
                                                    <div class="movie-info">
                                                        <i class="bx bxs-star"></i>
                                                        <span>9.5</span>
                                                    </div>
                                                    <div class="movie-info">
                                                        <i class="bx bxs-time"></i>
                                                        <span><?= $row['Thoi_luong'] ?></span>
                                                    </div>
                                                    <div class="movie-info">
                                                        <span>HD</span>
                                                    </div>
                                                    <div class="movie-info">
                                                        <span>16+</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    <?php }
                                }
                                $conn->close();
                            ?>
                            <!-- END MOVIE ITEM -->
                        </div>
                    </div>
                </div>
                <!-- END LATEST MOVIES SECTION -->
            </div>
            <!-- End Nội dung-->
        </div>
    </div>
    <!-- End main -->

    <?php require_once('Layout_page/Layout_footer.php'); ?>
</body>
</html>