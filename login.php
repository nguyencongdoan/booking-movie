<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Login and Registration Form in HTML | CodingNepal</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" href="./css/login.css">
  <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<html>

<body>
  <?php
    include './Model/action.php';
  ?>

  <form action="./Model/action.php" method="post">
    <?php
        if(isset($_SESSION['thongbao'])){
          if(isset($message) && $message == 'success' || isset($_GET['logout'])){
            echo "<label class='success'>";
          }
          else echo "<label class='thongbao'>";
              echo $_SESSION['thongbao'];
          echo "</label>";
          unset($_SESSION['thongbao']);
        }
    ?>

    <div id="login">
      <div class="login-form">
        <div class="form-title">
          <h1>Đăng nhập</h1>
        </div>

        <label for="">
          <input type="email" placeholder="Email" name="email" value="<?php if(isset($_SESSION['email'])) echo $_SESSION['email']; ?>" id="input">
        </label>

        <label for="">
          <input type="password" placeholder="Mật khẩu" name="password" value="<?php if(isset($_SESSION['password'])) echo $_SESSION['password']; ?>" id="input">
        </label>

        <div class="remember-me">
          <input type="checkbox" class="form-check-input" checked id="checkbox">
          <label for="checkbox" class="label-checkbox">
            <span class="txt1">Ghi nhớ</span>
          </label>
        </div>

        <button class="red" type="submit" name="submit">
          <i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;Login
        </button>

        <p class="login-with-social">Đăng nhập với...</p>
        <div class="social-media" style="padding-top: 10px;">
          <button class="social-icons" type="button">
            <i class="fa fa-facebook" style="color: blue;" aria-hidden="true"></i>
          </button>

          <button class="social-icons" type="button">
            <i class="fa fa-twitter" style="color: aqua;" aria-hidden="true"></i>
          </button>

          <button class="social-icons" type="button">
            <i class="fa fa-google-plus" style="color: red;" aria-hidden="true"></i>
          </button>
        </div>

        <div style="display: flex; justify-content: center;">
          <button class="red" type="submit" name="register" style="margin-right: 25px;">
            <i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Đăng ký
          </button>
          <button class="red" type="submit">
            <i class="fa fa-unlock-alt"></i>&nbsp;Quên mật khẩu
          </button>
        </div>
      </div>
    </div>
  </form>
</body>

</html>