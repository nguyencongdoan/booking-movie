<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thanh toán</title>
    <?php require_once('Layout_page/Layout_file_top.php'); ?>
</head>
<body>
    <?php require_once('Layout_page/Layout_header.php');  ?>

    <div class="container" style="max-width: 1240px;">
        <h3 class="fw-bold mt-4">Thanh toán</h3>
        <div class="row">
            <div class="col-md-7 col-sm-12 main-pay-left mt-3">
                <div class="section-pay">
                    <h5 class="fw-bold">Gói VIP</h5>
                    <div class="col-md-12 mt-3">
                        <div class="section-radio">
                            <div class="form-check section-input">
                                <div class="" style="position: relative; display: flex;">
                                    <input class="form-check-input section-b1" type="radio" name="radio1"
                                        id="1" value="option1" style="position: absolute; bottom: 17px; left: 8px;" checked>
                                    <label class="form-check-label fw-bold" for="1" style="margin-left: 8px;">
                                           1 Tháng
                                    </label>
                                </div>
                            </div>
                            <p class="fw-bold mt-md-0 mt-2">80.000 VND</p>
                        </div>
                    </div>
                    <div class="col-md-12 mt-3">
                        <div class="section-radio">
                            <div class="form-check section-input">
                                <div class="form-check" style="position: relative; display: flex;">
                                    <input class="form-check-input" type="radio" name="radio1" 
                                        id="6" value="option1" style="position: absolute; bottom: 17px; left: 8px">
                                    <label class="form-check-label fw-bold ml-2 mb-1 mb-md-0" for="6">
                                        &nbsp; &nbsp; 6 Tháng
                                    </label>
                                </div>
                            </div>
                            <p class="fw-bold mt-md-0 mt-2">528.000 VND</p>
                        </div>
                    </div>
                    <div class="col-md-12 mt-3">
                        <div class="section-radio">
                            <div class="form-check section-input">
                                <div class="form-check" style="position: relative; display: flex;">
                                    <input class="form-check-input" type="radio" name="radio1" 
                                        id="12" value="option1" style="position: absolute; bottom: 17px; left: 8px">
                                    <label class="form-check-label fw-bold ml-2 mb-1 mb-md-0" for="12">
                                            &nbsp; &nbsp; 12 Tháng
                                    </label>
                                </div>
                            </div>
                            <p class="fw-bold mt-md-0 mt-2">1.056.000 VND</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-3 main-pay-left-pt">
                    <h5 class="fw-bold">Chọn hình thức thanh toán</h5>
                    <div class="col-md-12 mt-3">
                        <div class="main-pay-nav">
                            <div class="section-pay-text">
                                <img src="https://static.fptplay.net/static/img/share/promotion/27_08_2021/logo_foxpay27-08-2021_09g47-10.png" 
                                    class="section-img" alt="">
                                <label class="fw-bold form-check-label fw-bold" for="vifox">Ví Foxpay</label>
                            </div>
                            <div class="section-pay-radio">
                                <input class="form-check-input" type="radio" name="radio2" 
                                    id="vifox" value="option1" checked>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mt-3">
                        <div class="main-pay-nav">
                            <div class="section-pay-text">
                                <img src="https://static.fptplay.net/static/img/share/video/21_10_2020/zalopay21-10-2020_17g16-23.png" 
                                    class="section-img" alt="">
                                <div style="height:58px;">
                                    <label class="fw-bold form-check-label fw-bold" for="vizalo">Ví ZaloPay</label>
                                    <p class="text-break text-sm-wrap" style="font-size:12px; padding: 0 28px 0 0;" for="vizalo">
                                        Hoàn ngay 85K cho lần đầu thanh toán. Gia hạn hoàn 35K cho đơn từ 120K. Ưu đãi có hạn! 
                                        Chi tiết xem tại: bit.ly/khuyenmai
                                    </p>
                                </div>
                            </div>
                            <div class="section-pay-radio">
                                <input class="form-check-input" type="radio" name="radio2" 
                                    id="vizalo" value="option1">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mt-3">
                        <div class="main-pay-nav">
                            <div class="section-pay-text">
                                <img src="https://static.fptplay.net/static/img/share/promotion/17_08_2021/logo-shopeepay-78x7817-08-2021_15g14-47.png" 
                                    class="section-img" alt="">
                                <div style="height:58px;">
                                    <label class="fw-bold form-check-label fw-bold" for="vishopee">Ví ShopeePay</label>
                                    <p class="text-break text-sm-wrap" style="font-size:12px; padding: 0 28px 0 0;" for="vizalo">
                                        Lần đầu ưu đãi 100% tối đa 50K. Gia hạn ưu đãi 50% tối đa 30K, đơn từ 50K, nhập mã SPPGTCPEU vào ví ShopeePay. 
                                        Chi tiết xem tại: bit.ly/khuyenmai
                                    </p>
                                </div>
                            </div>
                            
                            <div class="section-pay-radio">
                                <input class="form-check-input" type="radio" name="radio2" 
                                    id="vishopee" value="option1">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mt-3">
                        <div class="main-pay-nav">
                            <div class="section-pay-text">
                                <img src="https://static.fptplay.net/static/img/share/video/21_10_2020/momo21-10-2020_17g15-59.png"
                                    class="section-img" alt="">
                                <div style="height:58px;">
                                    <label class="fw-bold form-check-label fw-bold" for="vimomo">Ví Momo</label>
                                    <p class="text-break text-sm-wrap" style="font-size: 12px; padding: 0 28px 0 0;" for="vizalo">
                                        Hoàn ngay 20K về Túi Thần Tài cho lần đầu thanh toán. Ưu đãi có hạn!
                                    </p>
                                </div>
                            </div>
                            
                            <div class="section-pay-radio">
                                <input class="form-check-input" type="radio" name="radio2" 
                                    id="vimomo" value="option1">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mt-3">
                        <div class="main-pay-nav">
                            <div class="section-pay-text">
                                <img src="https://static.fptplay.net/static/img/share/promotion/14_09_2021/ic_viettelpay_red-2x-78x7814-09-2021_15g22-33.png" 
                                    class="section-img" alt="">
                                <label class="fw-bold form-check-label fw-bold" for="viviettel">Viettel Pay</label>
                            </div>
                            <div class="section-pay-radio">
                                <input class="form-check-input" type="radio" name="radio2" 
                                    id="viviettel" value="option1">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mt-3">
                        <div class="main-pay-nav">
                            <div class="section-pay-text">
                                <img src="https://static.fptplay.net/static/img/share/promotion/16_03_2021/dcb16-03-2021_16g16-47.png"
                                    class="section-img" alt="">
                                <div style="height:58px;">
                                    <label class="fw-bold form-check-label fw-bold" for="vicard">Tài khoản điện thoại</label>
                                    <p class="text-break text-sm-wrap" style="font-size:12px; padding: 0 28px 0 0;" for="vizalo">
                                        Áp dụng cho thuê bao Viettel và MobiFone. Tặng tối đa 2,5GB data khi thanh toán thành công 
                                        gói cước (chỉ áp dụng cho thuê bao MobiFone)
                                    </p>
                                </div>
                            </div>
                            
                            <div class="section-pay-radio">
                                <input class="form-check-input" type="radio" name="radio2" 
                                    id="vicard" value="option1">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-12 mt-3">
                <div class="col-md-12 main-pay-right">
                    <h3 class="fw-bold">Thông tin thanh toán</h3>
                    <div class="row">
                        <div class="col-md-12 main-pay-select mt-3">
                            <p class="text mt-3">Chọn gói:</p>
                            <select class="form-select" aria-label="Default select example">
                                <option selected>Gói cơ bản</option>
                                <option value="1">Gói VIP</option>
                                <option value="2">Gói Premium</option>
                            </select>
                        </div>
                        <div class="col-md-12 main-pay-select mt-3">
                            <p>Tài khoản:</p>
                            <p class="fw-bold">0339892651</p>
                        </div>
                        <div class="col-md-12 main-pay-select mt-3">
                            <p>Dịch vụ:</p>
                            <p class="fw-bold">Gói dịch vụ FlixPlay</p>
                        </div>
                        <div class="col-md-12 main-pay-select mt-3">
                            <p>Gói dịch vụ:</p>
                            <p class="fw-bold">Gói cơ bản</p>
                        </div>
                        <div class="col-md-12 main-pay-select mt-3">
                            <p>Giá gói</p>
                            <p class="fw-bold">80.000 VND</p>
                        </div>
                        <div class="col-md-12 main-pay-select mt-3">
                            <p>Loại gói dịch vụ:</p>
                            <p class="fw-bold">3 Tháng</p>
                        </div>
                        <hr class="mt-2 hr"/>
                        <div class="col-md-12 mt-2 main-pay-select mt-3">
                            <p>Số tiền phải thanh toán:</p>
                            <p class="fw-bold text-color">240.000 VND</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-3 main-pay-right">
                    <h5 class="fw-bold">Nhập mã khuyến mãi</h3>
                    <div class="input-group mt-3">
                        <input type="text" class="form-control" placeholder="Mã giảm giá" aria-label="Recipient's username" aria-describedby="button-addon2">
                        <button class="btn btn-outline-orange" type="button" id="button-addon2">Áp dụng</button>
                    </div>
                    <p class="mt-3">
                        <i>
                            Hình thức tự động gia hạn khi hết hạn dịch vụ. Bạn có thể quản lý và 
                            hủy gia hạn tại mục Quản lý Thẻ Thanh Toán ở mục Tài khoản.
                        </i>
                    </p>
                    <p class="mt-3 mb-4">
                        <i>
                            Nhấn thanh toán, bạn đồng ý với <span class="text-org">chính sách</span> và <span class="text-org">điều khoản.</span>
                        </i>
                    </p>
                    <button class="btn btn-org btn-hv" type="button" data-bs-toggle="modal" data-bs-target="#thanhtoan">
                        Thanh toán
                    </button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal Thanh toán -->
    <div class="modal fade" id="thanhtoan">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="container-fluid">
                <div class="row">
                    <button class="btn ml-auto toggler-sm-icon" data-bs-dismiss="modal">
                        <box-icon class="toggler-sm-icon" name='x-circle' type='solid' color='rgba(249,249,249,0.63)' ></box-icon>
                    </button>
                    <div class="col-md-4 col-sm-12 modal-left bg-sm-none">
                        <img src="./images/code.png" class="col-sm-12 col-md-12 ml-img" style="border-radius: 12px;" alt="">
                    </div>
                   
                    <div class="col-md-7 col-sm-12 mx-4 mt-2 modal-right">
                        <button class="btn ml-auto toggler-lg-icon" data-bs-dismiss="modal">
                            <box-icon name='x-circle' type='solid' color='rgba(249,249,249,0.63)' ></box-icon>
                        </button>
                        <h5 class="fw-bold">Quét mã QR bằng ứng dụng ShopeePay để thanh toán dịch vụ FlixPlay:</h3>
                        <div class="col-md-12 mt-3" style="display: flex;">
                            <img src="https://fptplay.vn/images/payments/airpay/icon-01.svg" 
                                style="width:50px; height:50px;" alt="">
                            <div class="mx-2">
                                <p class="fw-bold mb-0">Bước 1</p>
                                <p>Mở ứng dụng ShopeePay trên điện thoại.</p>
                            </div>
                        </div>
                        <div class="col-md-12 mt-3" style="display: flex;">
                            <img src="https://fptplay.vn/images/payments/airpay/icon-02.svg" 
                                style="width:50px; height:50px;" alt="">
                            <div class="mx-2">
                                <p class="fw-bold mb-0">Bước 2</p>
                                <p>Nhấp vào biểu tượng quét mã trên trang chủ.</p>
                            </div>
                        </div>
                        <div class="col-md-12 mt-3" style="display: flex;">
                            <img src="https://fptplay.vn/images/payments/airpay/icon-03.svg" 
                                style="width:50px; height:50px;" alt="">
                            <div class="mx-2">
                                <p class="fw-bold mb-0">Bước 3</p>
                                <p>Sử dụng trình quét mã để quét mã QR.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    <!-- End modal thanh toán -->

    <?php require_once('Layout_page/Layout_footer.php');  ?>
</body>
</html>