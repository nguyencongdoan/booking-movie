<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Details Cinema</title>
    <?php require_once('Layout_page/Layout_file_top.php'); ?>
</head>
<body>
    <?php 
        require_once('Layout_page/Layout_header.php');  
        require_once('./Model/config.php');

        if(isset($_GET['id_rap'])){
            $id_rap = $_GET['id_rap'];
            $query = "SELECT * FROM rap_phim, phong_chieu WHERE rap_phim.ID_Rap = phong_chieu.ID_Rap AND rap_phim.ID_Rap = '$id_rap' ";
            $result = $conn->query($query);
            $phong_chieu = "";

            if(!$result) echo "Câu truy vấn bị lỗi";
            $row = $result->fetch_assoc();

            $ten_rap = $row['Ten_rap'];
            $dia_chi = $row['Dia_chi'];
            $sdt = $row['SDT'];
            $email = $row['Email'];
            $url_map = $row['Url_map'];
            
            $list_p = $conn->query($query);
            if($list_p->num_rows != 0) {
                $arr = array();
                $i = 0;
                while($row1 = $list_p->fetch_array()){
                    $arr[$i] = $row1['Ten_phong'];
                    $i++;
                }
                $phong_chieu = implode(", ",$arr);
            }
        }
    ?>

    <div class="container" style="max-width: 1240px;">
        <div class="row">
            <h2 class="fw-bold mt-4 text-title">Hệ thống rạp chiếu phim</h2>
            <div class="col-md-6 col-sm-12 mt-4 top-nav-left card">
                <h3 class="fw-bold card-title"><?= $ten_rap ?></h3>
                <div class="card-body">
                    <p class="card-text">
                        <?= $dia_chi ?>
                    </p>
                    <p class="card-text">
                        Số điện thoại: <?= $sdt ?>
                    </p>
                    <p class="card-text">
                        Email: <?= $email ?>
                    </p>
                    <p class="card-text">
                        Phòng chiếu: <?= $phong_chieu ?>
                    </p>
                    <a href="dat_ve.php?id_rap=<?= $id_rap; ?>" class="btn btn-buy mt-2 mb-0">
                        Đặt vé ngay
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 mt-4 top-nav-right">
                <iframe src="<?= $url_map; ?>" 
                    width="600" height="351" style="border:0; border-radius: 12px;" allowfullscreen="" loading="lazy">
                </iframe>
            </div>

            <!-- Phim đã chiếu -->
            <div class="col-md-12">
                <!-- LATEST MOVIES SECTION -->
                <div class="section">
                    <div class="">
                        <div class="section-header">
                            Phim đã chiếu
                        </div>
                        <div class="movies-slide carousel-nav-center owl-carousel">
                            <!-- MOVIE ITEM -->
                            <?php
                                $id_rap = $_GET['id_rap'];
                                $query = "SELECT DISTINCT Ten_phim,Thoi_luong,phim.ID_phim,Hinh FROM phim, suat_chieu, rap_phim WHERE rap_phim.ID_Rap = suat_chieu.ID_Rap AND suat_chieu.ID_Phim = phim.ID_phim AND rap_phim.ID_Rap = '$id_rap' ORDER BY Luot_xem DESC";
                                $result = $conn->query($query);

                                if(!$result) echo 'Câu truy vấn bị lỗi';

                                if($result->num_rows != 0){
                                    while($row = $result->fetch_array()) { ?>
                                        <a href="details.php?id_phim=<?= $row['ID_phim']; ?>" class="movie-item">
                                            <img src="./images/<?= $row['Hinh']; ?>" alt="">
                                            <div class="movie-item-content">
                                                <div class="movie-item-title mx-3">
                                                    <?= $row['Ten_phim']; ?>
                                                </div>
                                                <div class="movie-infos mx-3">
                                                    <div class="movie-info">
                                                        <i class="bx bxs-star"></i>
                                                        <span>9.5</span>
                                                    </div>
                                                    <div class="movie-info">
                                                        <i class="bx bxs-time"></i>
                                                        <span><?= $row['Thoi_luong']; ?></span>
                                                    </div>
                                                    <div class="movie-info">
                                                        <span>HD</span>
                                                    </div>
                                                    <div class="movie-info">
                                                        <span>16+</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    <?php }
                                }
                                $conn->close();
                            ?>
                            <!-- END MOVIE ITEM -->
                        </div>
                    </div>
                </div>
                <!-- END LATEST MOVIES SECTION -->
            </div>
            <!-- End Nội dung-->
        </div>
    </div>

    <?php require_once('Layout_page/Layout_footer.php');  ?>
</body>
</html>