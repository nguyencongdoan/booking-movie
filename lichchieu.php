<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lịch chiếu phim</title>
    <?php require_once('Layout_page/Layout_file_top.php'); ?>
</head>
<body>
    <?php 
        require_once('Layout_page/Layout_header.php');
        require_once('./Model/Config.php');
    ?>

    <!-- Lịch chiếu -->
    <div class="container">
        <nav class="mt-4 col-md-12 col-sm-12">
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <button class="nav-link active nav-active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-phim" 
                    type="button" role="tab" aria-controls="nav-phim" aria-selected="true">
                    Lịch chiếu theo phim
                </button>
                <span style="color:red; font-style: 40px;">|</span>
                <button class="nav-link nav-active" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-rap" 
                    type="button" role="tab" aria-controls="nav-rap" aria-selected="false">
                    Lịch chiếu theo rạp
                </button>
            </div>
        </nav>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.movie-item').click(function() {
                    var id_phim = $(this).children('.id_phim').val();
                    $.post('./Model/action_lichchieu.php', {data_id_phim: id_phim}, function(data) {
                        $('.rap_phim').html(data);
                    });
                });
            });
        </script>

        <div class="tab-content mt-4 col-sm-12 col-md-12" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-phim" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="row">
                        <!-- Tab lịch chiếu theo phim -->
                        <div class="container main-lich-phim">
                            <div class="col-md-10">
                                <!-- LATEST MOVIES SECTION -->
                                <div class="section">
                                    <div class="movies-slide carousel-nav-center owl-carousel">
                                        <!-- MOVIE ITEM -->
                                        <?php 
                                            $query = "SELECT DISTINCT Ten_phim,Thoi_luong,phim.ID_phim,Hinh FROM phim,suat_chieu WHERE phim.ID_phim = suat_chieu.ID_Phim AND ID_TL = 1 AND Tinh_trang_chieu = 1 ORDER BY Luot_xem DESC";
                                            $result = $conn->query($query);
                                            $id_phim = "";
                        
                                            if(!$result) echo 'Câu truy vấn bị lỗi';

                                            if($result->num_rows != 0){
                                                while($row = $result->fetch_array()) { ?>
                                                    <div class="movie-item">
                                                        <input type="hidden" class="id_phim" value="<?= $row['ID_phim'] ?>">
                                                        <img src="./images/<?= $row['Hinh'] ?>" class="img-sm">
                                                        <div class="movie-item-content">
                                                            <div class="movie-item-title mx-3">
                                                                <?= $row['Ten_phim'] ?>
                                                            </div>
                                                            <div class="movie-infos mx-3">
                                                                <div class="movie-info">
                                                                    <i class="bx bxs-star"></i>
                                                                    <span>9.5</span>
                                                                </div>
                                                                <div class="movie-info">
                                                                    <i class="bx bxs-time"></i>
                                                                    <span><?= $row['Thoi_luong'] ?></span>
                                                                </div>
                                                                <div class="movie-info">
                                                                    <span>HD</span>
                                                                </div>
                                                                <div class="movie-info">
                                                                    <span>16+</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }
                                            }
                                        ?>
                                        <!-- END MOVIE ITEM -->
                                    </div>
                                </div>
                                <!-- END LATEST MOVIES SECTION -->
                            </div>
                        </div>
                        <!-- END Tab lịch chiếu theo phim -->
                    </div>
                </div>
                
                <!-- Tab rạp chiếu phim -->
                <div class="tab-pane fade" id="nav-rap" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <div class="main-rap">
                        <!-- Tab lịch chiếu theo rạp -->
                        <div class="col-md-12 mt-4 mt-md-0">
                            <ul class="nav nav-pills mb-3 col-md-12 col-sm-12" id="pills-tab" role="tablist" style="justify-content:center;">
                                <li class="nav-item mt-3" role="presentation">
                                    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-hcm" 
                                        type="button" role="tab" aria-controls="pills-hcm" aria-selected="true">
                                        <box-icon name='map' animation='flashing' color='rgba(178,0,0,0.86)' ></box-icon>
                                        TP Hồ Chí Minh
                                    </button>
                                </li>
                                <li class="nav-item mt-3" role="presentation">
                                    <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-dn" 
                                        type="button" role="tab" aria-controls="pills-dn" aria-selected="false">
                                        <box-icon name='map' animation='flashing' color='rgba(178,0,0,0.86)' ></box-icon>
                                        TP Đà Nẵng
                                    </button>
                                </li>
                                <li class="nav-item mt-3" role="presentation">
                                    <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-hn" 
                                        type="button" role="tab" aria-controls="pills-hn" aria-selected="false">
                                        <box-icon name='map' animation='flashing' color='rgba(178,0,0,0.86)' ></box-icon>
                                        Hà Nội
                                    </button>
                                </li>
                            </ul>
                        </div>

                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('.rap-item').click(function() {
                                    var id_rap = $(this).children('.id_rap').val();
                                    $.post('./Model/data_rap.php', {data_id_rap: id_rap}, function(data) {
                                        $('.ds_phim').html(data);
                                    });
                                });
                            });
                        </script>

                        <div class="tab-content mt-4 mt-md-0 mx-5" id="pills-tabContent">
                            <!-- Tab TP Hồ Chí Minh -->
                            <div class="tab-pane mt-4 mt-md-0 fade show active col-sm-12" id="pills-hcm" role="tabpanel" aria-labelledby="pills-hcm-tab">
                                <div class="row">
                                    <?php
                                        $query = "SELECT * FROM rap_phim WHERE Dia_chi LIKE '%Tp. HỒ Chí Minh%' ";
                                        $result = $conn->query($query);

                                        if(!$result) echo 'Câu truy vấn bị lỗi';
                                        
                                        if($result->num_rows != 0){
                                            while($row = $result->fetch_array()) { ?>
                                                <div class="card col-md-r-2 col-sm-12 mt-4 mx-auto mx-md-3">
                                                    <div class="rap-item">
                                                        <input type="hidden" class="id_rap" value="<?= $row['ID_Rap']; ?>">
                                                        <h5 class="fw-bold card-header"><?= $row['Ten_rap']; ?></h5>
                                                        <div class="card-body">
                                                            <?= $row['Dia_chi']; ?>
                                                        </div>
                                                        <div class="card-footer">
                                                            <a href="details_cinema.php?id_rap=<?= $row['ID_Rap']; ?>" class="fw-bold">
                                                                Xem chi tiết
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }
                                        }
                                    ?>
                                </div>
                            </div>
                            <!-- End Tab TP Hồ Chí Minh -->
                        

                            <!-- Tab TP Đà Nẵng -->
                            <div class="tab-pane fade mt-4 mt-md-0 col-sm-12" id="pills-dn" role="tabpanel" aria-labelledby="pills-dn-tab">
                                <div class="row">
                                    <?php
                                        $query = "SELECT * FROM rap_phim WHERE Dia_chi LIKE '%Tp. Đà Nẵng%' ";
                                        $result = $conn->query($query);

                                        if(!$result) echo 'Câu truy vấn bị lỗi';

                                        if($result->num_rows != 0){
                                            while($row = $result->fetch_array()) { ?>
                                                <div class="card col-md-r-2 col-sm-12 mt-4 mx-auto mx-md-3">
                                                    <div class="rap-item">
                                                        <input type="hidden" class="id_rap" value="<?= $row['ID_Rap']; ?>">
                                                        <h5 class="fw-bold card-header"><?= $row['Ten_rap']; ?></h5>
                                                        <div class="card-body">
                                                            <?= $row['Dia_chi']; ?>
                                                        </div>
                                                        <div class="card-footer">
                                                            <a href="details_cinema.php?id_rap=<?= $row['ID_Rap']; ?>" class="fw-bold">
                                                                Xem chi tiết
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }
                                        }
                                    ?>
                                </div>
                            </div>
                            <!-- End Tab TP Đà Nẵng -->

                            <!-- Tab Hà Nội -->
                            <div class="tab-pane fade mt-4 mt-md-0 mt-md-0 col-sm-12" id="pills-hn" role="tabpanel" aria-labelledby="pills-hn-tab">
                                <div class="row">
                                    <?php
                                        $query = "SELECT * FROM rap_phim WHERE Dia_chi LIKE '%Tp. Hà Nội%' ";
                                        $result = $conn->query($query);

                                        if(!$result) echo 'Câu truy vấn bị lỗi';

                                        if($result->num_rows != 0){
                                            while($row = $result->fetch_array()) { ?>
                                                <div class="card col-md-r-2 col-sm-12 mt-4 mx-auto mx-md-3">
                                                    <div class="rap-item">
                                                        <input type="hidden" class="id_rap" value="<?= $row['ID_Rap']; ?>">
                                                        <h5 class="fw-bold card-header"><?= $row['Ten_rap']; ?></h5>
                                                        <div class="card-body">
                                                            <?= $row['Dia_chi']; ?>
                                                        </div>
                                                        <div class="card-footer">
                                                            <a href="details_cinema.php?id_rap=<?= $row['ID_Rap']; ?>" class="fw-bold">
                                                                Xem chi tiết
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }
                                        }
                                    ?>
                                </div>
                            </div>
                            <!-- End Tab Hà Nội -->
                            
                        </div>
                        <!-- END lịch chiếu theo rạp -->
                    </div>
                </div>
                <!-- End tab rạp chiếu phim -->
             </div>
        </div>
    </div>
    <!-- End lịch chiếu -->

    <!-- Toggler lich chiếu theo phim -->
    <div class="container mt-5 rap_phim" id="cinema" style="max-width: 1240px;"></div>
    <div class="container mt-5 phong_chieu" id="cinema" style="max-width: 1240px;"></div>
    <!-- End Toggler -->

    <!-- Toggler lịch chiếu theo rạp -->
    <div class="container ds_phim" style="max-width: 1240px;"></div>
    <!-- End -->

    <?php require_once('Layout_page/Layout_footer.php');  ?>

</body>
</html>