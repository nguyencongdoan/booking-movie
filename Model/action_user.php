<?php
    include 'config.php';

    $id_kh = "";
    $hoten = "";
    $sdt = "";
    $email = "";
    $diachi = "";
    $gioitinh = "";
    $hinh = "";
    $password = "";
    $passnew = "";
    $passconf = "";

    if(isset($_GET['id_kh'])){
        $id_kh = $_GET['id_kh'];
        $query = "SELECT * FROM khach_hang WHERE ID_KH = $id_kh";
        $result = $conn->query($query);

        if(!$result) echo 'Câu truy vấn bị lỗi';
        $row = $result->fetch_assoc();

        $hoten = $row['Ho_ten'];
        $sdt = $row['SDT'];
        $email = $row['Email'];
        $diachi = $row['Dia_chi'];
        $hinh = $row['Hinh'];

        if($row['Gioi_tinh'] == 1)
            $gioitinh = "Nam";
        else
            $gioitinh = "Nữ";
    }

    if(isset($_POST['capnhat'])){
        $id_kh = $_POST['id_kh'];
        $hoten = $_POST['hoten'];
        $sdt = $_POST['sdt'];
        $email = $_POST['email'];
        $diachi = $_POST['diachi'];
        $gioitinh = $_POST['gioitinh'];
        
        if($_FILES['image']['name'] != NULL){
            $hinh = basename($_FILES['image']['name']);
            $uploads = "./images/".$hinh;  
            $query = "UPDATE khach_hang SET Ho_ten='$hoten', Hinh='$hinh', Dia_chi='$diachi', SDT='$sdt', Gioi_tinh='$gioitinh', Email='$email' WHERE ID_KH = $id_kh";
            move_uploaded_file($_FILES['image']['tmp_name'], $uploads);
            $_SESSION['Hinh'] = $hinh;
        }
        else{
            $query = "UPDATE khach_hang SET Ho_ten='$hoten', Dia_chi='$diachi', SDT='$sdt', Gioi_tinh='$gioitinh', Email='$email' WHERE ID_KH = $id_kh";
        }
        
        if($conn->query($query) === TRUE) {
            $thongbao = "Cập nhật thông tin thành công";
        }
        else {
            $thongbao = "Cập nhật thông tin thất bại";
        }
    }

    if(isset($_POST['changePassword'])){
        $id_kh = $_POST['id_kh'];
        $password = $_POST['password'];
        $passnew = $_POST['passnew'];
        $passconf = $_POST['passconf'];
        $thongbao = "";

        $query = "SELECT * FROM khach_hang WHERE ID_KH = '$id_kh'";
        $result = $conn->query($query);

        if(!$result) echo 'Câu truy vấn bị lỗi';
        $row = $result->fetch_assoc();

        $pass = md5($password);
        if($pass == $row['Passwords']){
            if($passnew == $passconf){
                $pass = md5($passnew);
                $query = "UPDATE khach_hang SET Passwords = '$pass' WHERE ID_KH = $id_kh";
                if($conn->query($query) === TRUE){
                    $thongbao = "Cập nhật mật khẩu thành công!";
                }
                else{
                    $thongbao = "Cập nhật mật khẩu thất bại!";
                }
            }
            else{
                $thongbao = "Nhập lại mật khẩu không đúng!";
            }
        }
        else{
            $thongbao = "Mật khẩu không đúng. Vui lòng nhập lại!";
        }
    }
?>