<?php
    include 'config.php';
    if(!isset($_SESSION)) 
    { 
        session_start(); 
    }
        
    if(isset($_GET['message'])){
        $message = $_GET['message'];
        if($message == 'success'){
            $_SESSION['thongbao'] = "Đăng ký thành công !";
        }
    }

    if(isset($_POST['submit'])) {
        $email = $_POST['email'];
        $password = $_POST['password'];
        $_SESSION['email'] = $email;
        $_SESSION['password'] = $password;

        $query_kh = "SELECT * FROM khach_hang WHERE Email = '$email'";
        $query_nv = "SELECT * FROM nhan_vien WHERE Email = '$email'";
        
        $result_kh = $conn->query($query_kh);
        if(!$result_kh) echo 'Cau truy van bi loi';

        $result_nv = $conn->query($query_nv);
        if(!$result_nv) echo 'Cau truy van bi loi';

        if(empty($email) || empty($password))
        {
            $message = 'danger';
            $_SESSION['thongbao'] = "Vui lòng nhập email, password!";
            header('Location: ../login.php');
        }
        else if($result_kh->num_rows == 0 && $result_nv->num_rows == 0){
            $message = 'danger';
            $_SESSION['thongbao'] = "Tên tài khoản không tồn tại. Vui lòng kiểm tra lại!";
            header('Location: ../login.php');
        }
        else{
            if($result_kh->num_rows > 0){ // đăng nhập là khách hàng
                $row = $result_kh->fetch_assoc();
                $pass = md5($password);

                if($row['Passwords'] == $pass){
                    $_SESSION['message_login'] = "Đăng nhập thành công";
                    $_SESSION['type'] = "success";
                    $_SESSION['HoTen'] = $row['Ho_ten'];
                    $_SESSION['ID_User'] = $row['ID_KH'];
                    $_SESSION['Hinh'] = $row['Hinh'];
                    
                    header('Location: ../index.php');
                }
                else{
                    $message = 'danger';
                    $_SESSION['thongbao'] = "Mật khẩu không đúng. Vui lòng nhập lại!";
                    header('Location: ../login.php');
                }
            }
            else if($result_nv->num_rows > 0){ // đăng nhập là nhân viên
                $row = $result_nv->fetch_assoc();
                $pass = md5($password);

                if($row['Passwords'] == $pass){
                    $id_quyen = $row['ID_Quyen'];
                    $query1 = "SELECT * FROM quyen_han WHERE ID_Quyen =  $id_quyen";
                    $result1 = $conn->query($query1);
                    if(!$result1) echo 'Cau truy van bi loi';
                    $row1 = $result1->fetch_assoc();

                    $_SESSION['HoTen'] = $row['HoTen'];
                    $_SESSION['Hinh'] = $row['Hinh'];
                    $_SESSION['ID_User'] = $row['ID_NV'];
                    $_SESSION['Ten_quyen'] = $row1['Ten_quyen_han']; // sử dụng ở trang layout_header bên admin
                    $_SESSION['message_login'] = "Đăng nhập thành công";
                    $_SESSION['type'] = "success";
                    
                    header('Location: ../Admin/index.php');
                }
                else{
                    $message = 'danger';
                    $_SESSION['thongbao'] = "Mật khẩu không đúng. Vui lòng nhập lại!";
                    header('Location: ../login.php');
                }
            }    
        }
    }

    if(isset($_POST['register'])){
        header('Location: ../register.php');
    }

    if(isset($_GET['logout'])){
        session_destroy();
        $_SESSION['thongbao'] = "Bạn đã đăng xuất thành công !";
        // header('Location: ./login.php?message=success');
    }

?>