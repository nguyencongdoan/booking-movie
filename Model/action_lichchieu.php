<?php
    include 'config.php';
    $id_phim = "";
    $id_rap = "";
    $id_sc = "";

    if(isset($_POST['data_id_phim'])){
        $id_phim = $_POST['data_id_phim'];
        $query = "SELECT DISTINCT Ten_rap, rap_phim.ID_Rap, phim.ID_phim FROM phim, suat_chieu, rap_phim WHERE phim.ID_phim = suat_chieu.ID_Phim AND suat_chieu.ID_Rap = rap_phim.ID_Rap AND phim.ID_phim = '$id_phim'";
        $result = $conn->query($query);
        if(!$result) echo "Câu truy vấn bị lỗi"; 
        ?>

        <script type="text/javascript">
            $(document).ready(function() {
                $('.main-movie-tab').click(function() {
                    var id_rap = $(this).children('.id_rap').val();
                    var id_phim = $(this).children('.id_phim').val();

                    $.post('./Model/action_lichchieu.php', {data_id_rap: id_rap, data_id_phim: id_phim}, function(data) {
                        $('.rap_phim').html(data);
                    });
                });
            });
        </script>
        
        <div class="row mx-2 main-hour">
            <?php
                if($result->num_rows != 0){
                    while($row = $result->fetch_array()){ ?>
                        <div class="col-md-2 col-sm-5 main-movie-tab mt-2 mx-2 mb-2 active">
                            <input type="hidden" class="id_rap" value="<?= $row['ID_Rap'] ?>">
                            <input type="hidden" class="id_phim" value="<?= $row['ID_phim'] ?>">
                            <p class="fw-bold text-ci-p"><?= $row['Ten_rap'] ?></p>
                        </div>
                    <?php }
                }
            ?>
        </div>
    <?php } ?>

    <?php
        if(isset($_POST['data_id_rap'])){
            $id_rap = $_POST['data_id_rap'];
            $id_phim = $_POST['data_id_phim'];

            $query = "SELECT *  FROM phim, suat_chieu, rap_phim WHERE phim.ID_phim = suat_chieu.ID_Phim AND suat_chieu.ID_Rap = rap_phim.ID_Rap AND phim.ID_phim = '$id_phim' AND rap_phim.ID_Rap = '$id_rap'";
            $result = $conn->query($query);
            if(!$result) echo "Câu truy vấn bị lỗi";
            ?>
            <div class="row mx-2 mt-4 main-hour">
                <?php
                    if($result->num_rows != 0){
                        while($row = $result->fetch_array()){ ?>
                            <div class="col-md-2 col-sm-5 main-movie-tab mt-2 mx-2 mb-2 active">
                                <a href="dat_ve.php?id_rap=<?= $row['ID_Rap']; ?>&id_phim=<?= $row['ID_phim']; ?>&id_sc=<?= $row['ID_SC']; ?>">
                                    <p class="fw-bold text-ci-p"><?= $row['Gio_BD'].' ~ '.$row['Gio_KT'].' '.$row['Ngay_chieu']; ?></p>
                                </a>
                            </div>
                        <?php }
                    }
                ?>
            </div>
        <?php }
    ?>

    