<?php
    include 'config.php';

    $id_rap = $_POST['data_id_rap'];
    $query = "SELECT *  FROM phim, suat_chieu, rap_phim WHERE phim.ID_phim = suat_chieu.ID_Phim AND suat_chieu.ID_Rap = rap_phim.ID_Rap AND rap_phim.ID_Rap = '$id_rap'";
    $result = $conn->query($query);
    if(!$result) echo "Câu truy vấn bị lỗi";

    ?>
    <div class="row">
        <?php
            if($result->num_rows != 0){
                while($row = $result->fetch_array()) { ?>
                    <div class="col-md-6 col-sm-12 mx-4 mt-4 mx-md-0">
                        <div class="row">
                            <div class="col-md-5">
                                <a href="dat_ve.php?id_rap=<?= $row['ID_Rap']; ?>&id_phim=<?= $row['ID_phim']; ?>&id_sc=<?= $row['ID_SC']; ?>" class="movie-item">
                                    <img src="./images/<?= $row['Hinh'] ?>" class="img-sm">
                                    <div class="movie-item-content">
                                        <div class="movie-item-title mx-3"><?= $row['Ten_phim'] ?></div>
                                        <div class="movie-infos mx-3">
                                            <div class="movie-info">
                                                <i class="bx bxs-star"></i>
                                                <span>9.5</span>
                                            </div>
                                            <div class="movie-info">
                                                <i class="bx bxs-time"></i>
                                                <span><?= $row['Thoi_luong'] ?></span>
                                            </div>
                                            <div class="movie-info">
                                                <span>HD</span>
                                            </div>
                                            <div class="movie-info">
                                                <span>16+</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="col-md-2 col-sm-12 main-movie-tab mt-2 mx-2 mb-2 active">
                                    <a href="dat_ve.php?id_rap=<?= $row['ID_Rap']; ?>&id_phim=<?= $row['ID_phim']; ?>&id_sc=<?= $row['ID_SC']; ?>">
                                        <p class="fw-bold text-ci-p"><?= $row['Ngay_chieu'] ?></p>
                                    </a>
                                </div>
                                <div class="col-md-2 col-sm-12 main-movie-tab mt-3 mx-2 mb-2 active">
                                    <a href="dat_ve.php?id_rap=<?= $row['ID_Rap']; ?>&id_phim=<?= $row['ID_phim']; ?>&id_sc=<?= $row['ID_SC']; ?>">
                                        <p class="fw-bold text-ci-p"><?= $row['Gio_BD'].' ~ '.$row['Gio_KT']?></p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
            } 
        ?>
    </div>
<?php ?>