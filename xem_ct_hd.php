<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lịch sử giao dịch</title>
    <?php require_once('Layout_page/Layout_file_top.php'); ?>
    <style>
        input.form-control{
            background-color: #ffffff;
            border-radius: 10px;
            border: 1px solid rebeccapurple;
        }

        td{
            padding: 10px;
        }

        th{
            color: #3ec461;
        }

        tr:nth-child(even){
            background-color: #272727;
        }
    </style>
</head>
<body>
    <?php
        require_once('./Model/config.php');
        require_once('./Layout_page/Layout_header.php'); 
        require_once('./Model/action_user.php');
    ?>

    <div class="container main">
        <div class="row mt-md-5 mt-4 res-row-sm-profile">
            <h2 class="fw-bold">Lịch sử giao dịch</h2>
            <?php
                if(isset($_GET['id_kh'])){
                    $id_kh = $_GET['id_kh'];
                    $query = "SELECT * FROM hoa_don, chi_tiet_hd, phim, rap_phim, phong_chieu, suat_chieu WHERE hoa_don.ID_HD = chi_tiet_hd.ID_HD AND chi_tiet_hd.ID_SC = suat_chieu.ID_SC AND chi_tiet_hd.ID_Phim = phim.ID_phim AND chi_tiet_hd.ID_Rap =rap_phim.ID_Rap AND chi_tiet_hd.ID_Phong = phong_chieu.ID_Phong AND hoa_don.ID_KH = '$id_kh'";
                    $result = $conn->query($query);
                    if(!$result) echo "Câu truy vấn bị lỗi";
                }
            ?>
            <table class="mt-4 mx-md-2 mx-0" cellpadding="5" style="width: 100%;">
                <tbody style="color: #ffffff;">
                    <tr style="text-align: center;">
                    <th>Mã hóa đơn</th>
                    <th>Rạp phim</th>
                    <th>Phòng chiếu</th>
                    <th>Suất chiếu</th>
                    <th>Tên phim</th>
                    <th>Vị trí ghế ngồi</th>
                    <th>Ngày đặt</th>
                    <th>Phương thức thanh toán</th>
                    <th>Tổng tiền</th>
                    <th>Tình trạng</th>
                    <th></th>
                </tr>
                <?php
                    if($result->num_rows != 0){
                        while($row = $result->fetch_array()) {
                                if($row['Tinh_trang'] == 0){
                                    $tinhtrang = "Chưa xem";
                                    $color = "#f95252";
                                }
                                else{
                                    $tinhtrang = "Đã xem";
                                    $color = "lightgreen";
                                }
                            ?>
                            <tr>
                                <td>HD<?= $row['ID_HD']; ?></td>
                                <td><?= $row['Ten_rap']; ?></td>
                                <td><?= $row['Ten_phong']; ?></td>
                                <td><?= $row['Gio_BD'] .' ~ '.$row['Gio_KT']; ?></td>
                                <td><?= $row['Ten_phim']; ?></td>
                                <td><?= $row['List_ghe']; ?></td>
                                <td><?= $row['Ngay_dat']; ?></td>
                                <td><?= $row['Phuong_thuc_tt']; ?></td>
                                <td><?= $row['Tong_tien']; ?></td>
                                <td >
                                    <span style="color: <?= $color; ?>"><?= $tinhtrang; ?></span>
                                </td>
                                <td>
                                    <?php
                                        if($row['Tinh_trang'] == 0) 
                                            echo '<a href="#" style="color:lightgoldenrodyellow;">Hủy vé đặt</a>';
                                    ?>
                                </td>
                            </tr>
                        <?php }
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php require_once('Layout_page/Layout_footer.php'); ?>
</body>
</html>