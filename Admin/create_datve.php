<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Đặt vé xem phim</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-starter.css">

    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">
    
    <!-- BOX ICONS -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    
    <!-- AJAX -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>
    <script src="../js/timkiem.js"></script>
</head>

<body class="sidebar-menu-collapsed">
    <section>
        <?php 
            include '../Model/config.php';
            include './Action_admin/action_datve.php';
            require_once("./Layout_page/Layout_header.php"); 
        ?>

        <!-- main content start -->
        <div class="main-content">
            <!-- content -->
            <div class="container-fluid content-top-gap">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="dat_ve.php">Đặt vé xem phim</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Đặt vé</li>
                    </ol>
                    <h2 class="fw-bold text-center h2" style="color: rebeccapurple;">ĐẶT VÉ</h2>
                    <a href="dat_ve.php" class="btn mt-2 mb-3">Quay về trang trước</a>
                </nav>

                <?php if(isset($thongbao)){ ?>
                    <div class="col-md-10 alert alert-<?= $type; ?> mb-5" align="center">
                        <?php echo $thongbao; ?>
                    </div>
                <?php } unset($thongbao) ?>

                <div class="container">
                    <form action="" method="post">
                        <div class="row">
                            <div class="col-md-7 col-sm-12">
                                <div class="row"> 
                                    <?php 
                                        date_default_timezone_set('Asia/Ho_Chi_Minh');
                                        $date = date('Y-m-d H:i:s'); 
                                    ?>
                                    
                                    <input type="hidden" name="id_kh" value="3">
                                    <input type="hidden" name="id_nv" value="<?php if(isset($_SESSION['ID_User'])) echo $_SESSION['ID_User']; ?>">
                                    
                                    <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                        <span class="mt-2" id="sp">Rạp phim</span>
                                        <select class="form-select col-md-6 mx-2 rap_phim" name="id_rap">
                                            <?php
                                                $query = "SELECT * FROM rap_phim";
                                                $result = $conn->query($query);
                                                if(!$result) echo "Câu truy vấn bị lỗi";
                                                
                                                if($result->num_rows != 0) {
                                                    while($row = $result->fetch_array()) { ?>
                                                        <option value="<?= $row['ID_Rap']; ?>" <?php if($row['ID_Rap'] == $id_rap) echo 'selected'?>><?= $row['Ten_rap']; ?></option>
                                                <?php }
                                                }
                                            ?>
                                        </select>
                                    </div> 

                                    <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                        <span class="mt-2">Phòng chiếu</span>
                                        <select class="form-select col-md-6 mx-2 phong_chieu" name="id_phong">
                                            <?php
                                                if(isset($id_phong) && $id_phong != '') echo "<option value='$id_phong'>$tenphong</option>";
                                                else echo '<option value="" class="text-center">-------------------------------</option>';
                                            ?>
                                        </select> 
                                    </div>

                                    <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                        <span class="mt-2">Suất chiếu</span>
                                        <select class="form-select col-md-6 mx-2 suat_chieu" name="id_sc">
                                            <?php
                                                if(isset($id_sc) && $id_sc != '') echo "<option value='$id_sc'>$tensc</option>";
                                                else echo '<option value="" class="text-center">-------------------------------</option>';
                                            ?>
                                        </select> 
                                    </div>

                                    <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                        <span class="mt-2">Danh sách phim</span>
                                        <select class="form-select col-md-6 mx-2 phim" name="id_phim">
                                            <?php
                                                if(isset($id_phim) && $id_phim != '') echo "<option value='$id_phim'>$tenphim</option>";
                                                else echo '<option value="" class="text-center">-------------------------------</option>';
                                            ?>
                                        </select>
                                    </div>

                                    <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                        <span class="mt-2" style="transform: translateX(-94px);">Số lượng</span>
                                        <i class='bx bx-minus'></i>
                                        <input type="text" class="form-control col-md-1 mx-2 bx-number" min="1" max="27" name="soluong" value="<?php if(isset($soluong)) echo $soluong;?>" required>
                                        <i class='bx bx-plus'></i>
                                    </div>

                                    <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                        <span class="mt-2">Ngày đặt</span>
                                        <input type="text" class="form-control col-md-6 mx-2 disable" name="ngaydat" value="<?php echo $date; ?>" readonly>
                                    </div>

                                    <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                        <span class="mt-2">Khách hàng</span>
                                        <input type="text" class="form-control col-md-6 mx-2 disable" value="Nguyễn Văn A" readonly>
                                    </div>

                                    <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                        <span class="mt-2">Phương thức thanh toán</span>
                                        <input type="radio" class="form-check-input mx-2 mx-2" name="phuong_thuc_tt" value="1" id="offline" checked> 
                                        <label for="offline">Thanh toán tại rạp</label>
                                        <input type="radio" class="form-check-input mx-2 mx-2" name="phuong_thuc_tt" id="online" value="0">
                                        <label for="online">Thanh toán Online</label>
                                    </div>

                                    <div class="col-md-12 col-sm-12 mt-3"  style="margin-left: 220px;">
                                        <input type="submit" class="btn" name="create" value="Đặt vé">
                                    </div>
                                </div>
                            </div>

                            <script type="text/javascript">
                                $(document).ready(function() {
                                    $("input[name='ds_ghe[]'").change(function() {
                                        var max = $('.bx-number').val();
                                        var sl_check = $("input[name='ds_ghe[]']:checked").length;
                                        var sl_dis = $("input[name='ds_ghe[]']:disabled").length;
                                        number = sl_check - sl_dis;
                                        
                                        if (number > max){
                                            $(this).prop("checked", "");
                                            alert("Bạn chỉ được chọn tối đa " + max + " ghế");
                                        }
                                    });

                                    $('.phim').click(function() {
                                        var id_phim = $('.phim').val();
                                        var id_sc = $('.suat_chieu').val();
                                        var id_rap = $('.rap_phim').val();
                                        
                                        $.post('data_ghe.php', {data_id_phim: id_phim, data_id_sc: id_sc, data_id_rap: id_rap}, function(data){
                                            $('.list_ghe').html(data);
                                        });
                                    });
                                });
                            </script>

                            <div class="col-md-5 col-sm-12">
                                <h3 class="fw-bold text-center" style="color: rebeccapurple;">Chọn vị trí ghế ngồi</h3>
                                <img src="../images/screen.png" class="mx-5" style="margin-top: -50px;"> 
                                
                                <div class="list_ghe">
                                    <div class="form-check-vt" style="margin-top: -30px;">
                                        <h5 class="text-chect-h5 mx-3">A</h5>
                                        <?php
                                            for($i = 1; $i < 10; $i++) { ?>
                                                <input class="form-check-input input-check-vt" name="ds_ghe[]" id="A<?= $i ?>" type="checkbox" value="A<?= $i ?>">
                                                <label for="A<?= $i ?>" class="mx-2"></label>
                                            <?php }
                                        ?>
                                    </div>

                                    <div class="form-check-vt mt-5">
                                        <h5 class="text-chect-h5 mx-3">B</h5>
                                        <?php
                                            for($i = 1; $i < 10; $i++) { ?>
                                                <input class="form-check-input input-check-vt" name="ds_ghe[]" id="B<?= $i ?>" type="checkbox" value="B<?= $i ?>">
                                                <label for="B<?= $i ?>" class="mx-2"></label>
                                            <?php }
                                        ?>
                                    </div>

                                    <div class="form-check-vt mt-5">
                                        <h5 class="text-chect-h5 mx-3">C</h5>
                                        <?php
                                            for($i = 1; $i < 10; $i++) { ?>
                                                <input class="form-check-input input-check-vt" name="ds_ghe[]" id="C<?= $i ?>" type="checkbox" value="C<?= $i ?>">
                                                <label for="C<?= $i ?>" class="mx-2"></label>
                                            <?php }
                                        ?>
                                    </div>

                                    <div class="form-check-vt mt-5">
                                        <h5 class="text-chect-h5 mx-3">D</h5>
                                        <?php
                                            for($i = 1; $i < 10; $i++) { ?>
                                                <input class="form-check-input input-check-vt" name="ds_ghe[]" id="D<?= $i ?>" type="checkbox" value="D<?= $i ?>">
                                                <label for="D<?= $i ?>" class="mx-2"></label>
                                            <?php }
                                        ?>
                                    </div>
                                </div>
                                
                                <div class="mt-4 mx-1" style="padding-left: 35px;"> 
                                    <input class="form-check-input input-check-vt" type="checkbox">
                                    <label class="mx-2">Chưa chọn</label>
                                    <input class="form-check-input input-check-vt" type="checkbox" checked>
                                    <label class="mx-2">Đã chọn</label>
                                    <input class="form-check-input input-check-vt" type="checkbox" checked disabled>
                                    <label class="mx-2">Không thể chọn</label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- //content -->
        </div>
        <!-- main content end-->

    </section>
    
    <?php require_once("./Layout_page/Layout_footer.php"); ?>

</body>

</html>
  