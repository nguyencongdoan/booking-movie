    <?php include '../Model/action.php'; ?>
    
    <script src="https://unpkg.com/boxicons@2.0.9/dist/boxicons.js"></script>
    
    <!-- sidebar menu start -->
    <div class="sidebar-menu sticky-sidebar-menu">

    <!-- logo start -->
    <div class="logo">
        <h1><a href="index.html">Admin Home</a></h1>
    </div>

    <!-- if logo is image enable this -->
    <div class="logo-icon text-center">
        <a href="index.html" title="logo"><img src="assets/images/icon-cinema.png" alt="logo-icon"> </a>
    </div>
    <!-- //logo end -->

    <div class="sidebar-menu-inner">

    <!-- sidebar nav start -->
    <ul class="nav nav-pills nav-stacked custom-nav">
        <li class="active">
            <a href="index.php">
            <img src="assets/images/icon-cinema.png" class="img-home">
                <span> Home</span>
            </a>
        </li>
       
        <li class="menu-list active">
            <a href="#">
                <img src="../images/cinema.png" class="img-home"alt="">
                <span>Quản lý thông tin<i class="lnr lnr-chevron-right"></i></span>
            </a>
            <ul class="sub-menu-list">
                <li>
                    <a href="movie.php">Phim & tập phim</a>
                </li>
                <li>
                    <a href="cinema_admin.php">Rạp phim & phòng chiếu</a>
                </li>
                <li>
                    <a href="suatchieu_ve.php">Suất chiếu & Vé bán</a>
                </li>
            </ul>
        </li>

        <li>
            <a href="theloai.php">
                <img src="../images/dathang.png" class="img-home"class="img-con"> 
                <span>Thể loại & quyền hạn</span>
            </a>
        </li>

        <li class="menu-list active">
            <a href="#">
                <img src="../images/nhanvien.png" class="img-home"alt="">
                <span>Quản lý thông tin<i class="lnr lnr-chevron-right"></i></span>
            </a>
            <ul class="sub-menu-list">
                <li>
                    <a href="user.php">Thông tin khác hàng</a>
                </li>
                <li>
                    <a href="nhanvien.php">Thông tin nhân viên</a>
                </li>
            </ul>
        </li>
        
        <li class="menu-list active">
            <a href="#">
                <img src="../images/hoadon.png" class="img-home"alt="">
                <span>Quản lý Đặt vé & hóa đơn<i class="lnr lnr-chevron-right"></i></span>
            </a>
            <ul class="sub-menu-list">
                <li>
                    <a href="dat_ve.php">Đặt vé xem phim</a>
                </li>
                <li>
                    <a href="hoa_don.php">Quản lý hóa đơn</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- //sidebar nav end -->
    <!-- toggle button start -->
    <a class="toggle-btn">
        <i class="fa fa-angle-double-left menu-collapsed__left"><span>Collapse Sidebar</span></i>
        <i class="fa fa-angle-double-right menu-collapsed__right"></i>
    </a>
    <!-- //toggle button end -->
    </div>
    </div>
    <!-- //sidebar menu end -->

    <!-- header-starts -->
    <div class="header sticky-header">
    <!-- notification menu start -->
    <div class="menu-right">
        <div class="navbar user-panel-top">
            <div class="search-box">
                <form action="#search-results.html" method="get">
                    <input class="search-input" placeholder="Search Here..." type="search" id="search">
                    <button class="search-submit" value=""><span class="fa fa-search"></span></button>
                </form>
            </div>
            <div class="user-dropdown-details d-flex">
                <div class="profile_details_left">
                    <ul class="nofitications-dropdown">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i
                            class="fa fa-bell-o"></i><span class="badge blue">3</span></a>
                        <ul class="dropdown-menu">
                        <li>
                            <div class="notification_header">
                            <h3>You have 3 new notifications</h3>
                            </div>
                        </li>
                        <li><a href="#" class="grid">
                            <div class="user_img"><img src="assets/images/avatar1.jpg" alt=""></div>
                            <div class="notification_desc">
                                <p>Johnson purchased template</p>
                                <span>Just Now</span>
                            </div>
                            </a></li>
                        <li class="odd"><a href="#" class="grid">
                            <div class="user_img"><img src="assets/images/avatar2.jpg" alt=""></div>
                            <div class="notification_desc">
                                <p>New customer registered </p>
                                <span>1 hour ago</span>
                            </div>
                            </a></li>
                        <li><a href="#" class="grid">
                            <div class="user_img"><img src="assets/images/avatar3.jpg" alt=""></div>
                            <div class="notification_desc">
                                <p>Lorem ipsum dolor sit amet </p>
                                <span>2 hours ago</span>
                            </div>
                            </a></li>
                        <li>
                            <div class="notification_bottom">
                            <a href="#all" class="bg-primary">See all notifications</a>
                            </div>
                        </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i
                            class="fa fa-comment-o"></i><span class="badge blue">4</span></a>
                        <ul class="dropdown-menu">
                        <li>
                            <div class="notification_header">
                            <h3>You have 4 new messages</h3>
                            </div>
                        </li>
                        <li><a href="#" class="grid">
                            <div class="user_img"><img src="assets/images/avatar1.jpg" alt=""></div>
                            <div class="notification_desc">
                                <p>Johnson purchased template</p>
                                <span>Just Now</span>
                            </div>
                            </a></li>
                        <li class="odd"><a href="#" class="grid">
                            <div class="user_img"><img src="assets/images/avatar2.jpg" alt=""></div>
                            <div class="notification_desc">
                                <p>New customer registered </p>
                                <span>1 hour ago</span>
                            </div>
                            </a></li>
                        <li><a href="#" class="grid">
                            <div class="user_img"><img src="assets/images/avatar3.jpg" alt=""></div>
                            <div class="notification_desc">
                                <p>Lorem ipsum dolor sit amet </p>
                                <span>2 hours ago</span>
                            </div>
                            </a></li>
                        <li><a href="#" class="grid">
                            <div class="user_img"><img src="assets/images/avatar1.jpg" alt=""></div>
                            <div class="notification_desc">
                                <p>Johnson purchased template</p>
                                <span>Just Now</span>
                            </div>
                            </a></li>
                        <li>
                            <div class="notification_bottom">
                            <a href="#all" class="bg-primary">See all messages</a>
                            </div>
                        </li>
                        </ul>
                    </li>
                    </ul>
                </div>
                <div class="profile_details">
                    <ul>
                        <li class="dropdown profile_details_drop">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="dropdownMenu3" aria-haspopup="true"
                            aria-expanded="false">
                            <div class="profile_img">
                                <img src="../images/<?= $_SESSION['Hinh'] ?>" class="rounded-circle" alt="" />
                                <div class="user-active">
                                <span></span>
                                </div>
                            </div>
                            </a>
                            <ul class="dropdown-menu drp-mnu" aria-labelledby="dropdownMenu3">
                            <li class="user-info">
                                <h5 class="user-name mb-2"><?= $_SESSION['HoTen'] ?></h5>
                                <span class="status"><?= $_SESSION['Ten_quyen'] ?></span>
                            </li>
                            <li> <a href="#"><i class="lnr lnr-user"></i>Tài khoản</a> </li>
                            <li> <a href="#"><i class="lnr lnr-users"></i>1k Followers</a> </li>
                            <li> <a href="#"><i class="lnr lnr-cog"></i>Cài đặt</a> </li>
                            <li> <a href="#"><i class="lnr lnr-heart"></i>100 Likes</a> </li>
                            <li class="logout"> <a href="../login.php?logout"><i class="fa fa-power-off"></i> Đăng xuất</a> </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--notification menu end -->
    </div>
    <!-- //header-ends -->