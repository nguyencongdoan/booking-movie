<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Quản lý suất chiếu và vé</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-starter.css">

    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">

    <!-- AJAX -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>
</head>

<body class="sidebar-menu-collapsed">
    <section>
        <?php 
            include '../Model/config.php';
            include './Action_admin/action_suatchieu.php';
            require_once("./Layout_page/Layout_header.php"); 

            if(isset($_GET['delete_sc'])){
                $id_sc = $_GET['delete_sc'];
                $sql = "DELETE FROM suat_chieu WHERE ID_SC = '$id_sc'";
        
                if($conn->query($sql) === TRUE){
                    $_SESSION['thongbao'] = "Xóa thành công!";
                    $_SESSION['type'] = 'success';
                }
                else{
                    $_SESSION['thongbao'] = "Xóa thất bại!";
                    $_SESSION['type'] = 'danger';
                }
            }

            if(isset($_GET['delete_ve'])){
                $id_ve = $_GET['delete_ve'];
                $sql = "DELETE FROM ve_ban WHERE ID_Ve = '$id_ve'";
        
                if($conn->query($sql) === TRUE){
                    $_SESSION['thongbao'] = "Xóa thành công!";
                    $_SESSION['type'] = 'success';
                }
                else{
                    $_SESSION['thongbao'] = "Xóa thất bại!";
                    $_SESSION['type'] = 'danger';
                }
            }
        ?>

        <!-- main content start -->
        <div class="main-content">
            <!-- content -->
            <div class="container-fluid content-top-gap">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Quản lý suất chiếu & vé</li>
                    </ol>
                    <h2 class="fw-bold text-center h2" style="color: rebeccapurple;">QUẢN LÝ SUẤT CHIẾU</h2>
                    <div class="search-ajax">
                        <a href="create_suatchieu.php" class="btn mt-2 mb-3">Thêm mới</a>
                        <!-- <div class="search-box">
                            <form action="" method="post">
                                <input class="search-input" placeholder="Search Here..." type="search" id="search">
                                <button class="search-submit" type="submit" value=""><span class="fa fa-search"></span></button>
                            </form>
                        </div> -->
                    </div>
                </nav>

                <?php
                    if(isset($_GET['them']) || isset($_GET['sua'])){
                        if(isset($_GET['them']) && $_GET['them'] == 'success'){
                            $_SESSION['thongbao'] = "Thêm mới thành công!";
                        }
                        if(isset($_GET['sua']) && $_GET['sua'] == 'success'){
                            $_SESSION['thongbao'] = "Sửa thông tin thành công";
                        }
                        $_SESSION['type'] = 'success';
                    }
                ?>

                <?php if(isset($_SESSION['thongbao'])){ ?>
                    <div class="col-md-10 alert alert-<?= $_SESSION['type']; ?> mb-5" align="center">
                        <?php echo $_SESSION['thongbao']; ?>
                    </div>
                <?php } unset($_SESSION['thongbao']);?>

                <table class="table">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>ID suất chiếu</th>
                            <th class="text-start">Tên phim</th>
                            <th>Ngày chiếu</th>
                            <th>Giờ bắt đầu</th>
                            <th>Giờ kết thúc</th>
                            <th>Tình trạng</th>
                            <th class="text-start">Rạp phim</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="tbody">
                        <?php
                            if(!isset($_GET['page'])){
                                $_GET['page'] = 1;
                            }
                            $rowPerPage = 6;
                            // vị trí của mẩu tin đầu tiên trên mỗi trang
                            $offset = ($_GET['page'] - 1) * $rowPerPage;

                            $query = "SELECT * FROM suat_chieu, phim, rap_phim WHERE suat_chieu.ID_Phim = phim.ID_Phim AND suat_chieu.ID_Rap = rap_phim.ID_Rap LIMIT $offset, $rowPerPage";
                            $result = $conn->query($query);
                            if(!$result) echo "Câu truy vấn bị lỗi";
                            $i = 1;
                            if($result->num_rows != 0) {
                                while($row = $result->fetch_array()){ 
                                        if($row['Tinh_trang_chieu'] == 1) $tinhtrang = 'Phim đang chiếu';
                                        else $tinhtrang = 'Phim không còn chiếu';
                                    ?>
                                    <tr>
                                        <td><?= $i ?></td>

                                        <td>
                                            <?= $row['ID_SC'] ?>
                                        </td>

                                        <td class="text-start">
                                            <?= $row['Ten_phim'] ?>
                                        </td>

                                        <td>
                                            <?= $row['Ngay_chieu'] ?>
                                        </td>

                                        <td>
                                            <?= $row['Gio_BD'] ?>
                                        </td>

                                        <td>
                                            <?= $row['Gio_KT'] ?>
                                        </td>

                                        <td>
                                            <?= $tinhtrang ?>
                                        </td>

                                        <td class="text-start">
                                            <?= $row['Ten_rap'] ?>
                                        </td>

                                        <td>
                                            <a href="#" class="btn btn-table-details">Xem chi tiết</a>
                                            <a href="edit_suatchieu.php?id_sc=<?= $row['ID_SC'] ?>" class="btn btn-table-edit">Sửa</a>
                                            <a href="suatchieu_ve.php?delete_sc=<?= $row['ID_SC'] ?>" class="btn btn-table-delete" onclick="return confirm('Bạn có chắc muốn xóa?');">Xóa</a>
                                        </td>
                                    </tr>
                                <?php $i++;}
                            }
                            else{ ?>
                                <tr>
                                    <td colspan="6" class="text-center" style="font-size:18px; height:160px; color: rebeccapurple;"><img src="../images/thongbao.png" alt=""></td>
                                </tr>
                            <?php }
                        ?>
                    </tbody>
                </table>
                
                <?php
                    $re = $conn->query("SELECT * FROM suat_chieu, phim, rap_phim WHERE suat_chieu.ID_Phim = phim.ID_Phim AND suat_chieu.ID_Rap = rap_phim.ID_Rap");
                    $numRows = mysqli_num_rows($re);
                    $maxPage = ceil($numRows/$rowPerPage);
                ?>

                <nav class="d-flex justify-content-end p-4" aria-label="Page navigation example">
                    <ul class="pagination">
                    <li class="page-item">
                            <?php
                                if($_GET["page"] > 1){ ?>
                                    <a class="page-link" href="suatchieu_ve.php?&page=<?= $_GET['page'] - 1 ?>" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                <?php }
                            ?>
                        </li>
                        <?php 
                            for ($i=1 ; $i<=$maxPage ; $i++)
                            {
                                if($i == $_GET['page']){ ?>
                                    <li class="page-item">
                                        <a class="page-link active" href="suatchieu_ve.php?&page=<?= $i ?>"><?= $i ?></a> <!-- trang hiện tại sẽ được style -->
                                    </li>
                                <?php }
                                else { ?>
                                    <li class="page-item">
                                        <a class="page-link" href="suatchieu_ve.php?&page=<?= $i ?>"><?= $i ?></a> 
                                    </li>
                                <?php }
                            }
                        ?>
                        <li class="page-item">
                            <?php
                                if($_GET["page"] < $maxPage){ ?>
                                    <a class="page-link" href="suatchieu_ve.php?&page=<?= $_GET['page'] + 1 ?>" aria-label="Previous">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                <?php }
                            ?>
                        </li>
                    </ul>
                </nav>

                <!-- Quản lý vé bán -->
                <nav>
                    <h2 class="fw-bold text-center h2" style="color: rebeccapurple;">QUẢN LÝ VÉ BÁN</h2>
                    <div class="search-ajax">
                        <a href="create_ve.php" class="btn mt-2 mb-3">Thêm mới</a>
                        <!-- <div class="search-box">
                            <form action="" method="post">
                                <input class="search-input-ve" placeholder="Search Here..." type="search" id="search">
                                <button class="search-submit" type="submit" value=""><span class="fa fa-search"></span></button>
                            </form>
                        </div> -->
                    </div>
                </nav>

                <table class="table">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th class="text-start">Suất chiếu</th>
                            <th>Ngày bán</th>
                            <th>Giá vé</th>
                            <th>Số lượng</th>
                            <th>Nhân viên bán</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="tbody_ve">
                        <?php
                            if(!isset($_GET['page1'])){
                                $_GET['page1'] = 1;
                            }
                            $rowPerPage = 6;
                            // vị trí của mẩu tin đầu tiên trên mỗi trang
                            $offset = ($_GET['page1'] - 1) * $rowPerPage;

                            $query_v = "SELECT * FROM ve_ban, suat_chieu, nhan_vien WHERE ve_ban.ID_SC = suat_chieu.ID_SC AND ve_ban.ID_NV = nhan_vien.ID_NV LIMIT $offset, $rowPerPage";
                            $result = $conn->query($query_v);
                            if(!$result) echo "Câu truy vấn bị lỗi";
                            $i = 1;
                            if($result->num_rows != 0) {
                                while($row = $result->fetch_array()){
                                    ?>
                                    <tr>
                                        <td><?= $i ?></td>

                                        <td class="text-start">
                                            <?= $row['Ngay_chieu'].' '.$row['Gio_BD'].' ~ '.$row['Gio_KT']; ?>
                                        </td>

                                        <td>
                                            <?= $row['Ngay_ban'] ?>
                                        </td>

                                        <td>
                                            <?= number_format($row['Gia_ve'], 0, '.', '.')?> VNĐ
                                        </td>

                                        <td>
                                            <?= $row['So_luong'] ?>
                                        </td>

                                        <td>
                                            <?= $row['HoTen'] ?>
                                        </td>

                                        <td>
                                            <a href="edit_ve.php?id_ve=<?= $row['ID_Ve'] ?>" class="btn btn-table-edit">Sửa</a>
                                            <a href="suatchieu_ve.php?delete_ve=<?= $row['ID_Ve'] ?>" class="btn btn-table-delete" onclick="return confirm('Bạn có chắc muốn xóa?');">Xóa</a>
                                        </td>
                                    </tr>
                                <?php $i++;}
                            }
                            else{ ?>
                                <tr>
                                    <td colspan="6" class="text-center" style="font-size:18px; height:160px; color: rebeccapurple;"><img src="../images/thongbao.png" alt=""></td>
                                </tr>
                            <?php }
                        ?>
                    </tbody>
                </table>
                
                <?php
                    $re = $conn->query("SELECT * FROM ve_ban, suat_chieu, nhan_vien WHERE ve_ban.ID_SC = suat_chieu.ID_SC AND ve_ban.ID_NV = nhan_vien.ID_NV");
                    $numRows = mysqli_num_rows($re);
                    $maxPage = ceil($numRows/$rowPerPage);
                ?>

                <nav class="d-flex justify-content-end p-4" aria-label="Page navigation example">
                    <ul class="pagination">
                    <li class="page-item">
                            <?php
                                if($_GET["page1"] > 1){ ?>
                                    <a class="page-link" href="suatchieu_ve.php?&page1=<?= $_GET['page1'] - 1 ?>" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                <?php }
                            ?>
                        </li>
                        <?php 
                            for ($i=1 ; $i<=$maxPage ; $i++)
                            {
                                if($i == $_GET['page1']){ ?>
                                    <li class="page-item">
                                        <a class="page-link active" href="suatchieu_ve.php?&page1=<?= $i ?>"><?= $i ?></a> <!-- trang hiện tại sẽ được style -->
                                    </li>
                                <?php }
                                else { ?>
                                    <li class="page-item">
                                        <a class="page-link" href="suatchieu_ve.php?&page1=<?= $i ?>"><?= $i ?></a> 
                                    </li>
                                <?php }
                            }
                        ?>
                        <li class="page-item">
                            <?php
                                if($_GET["page1"] < $maxPage){ ?>
                                    <a class="page-link" href="suatchieu_ve.php?&page1=<?= $_GET['page1'] + 1 ?>" aria-label="Previous">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                <?php }
                                
                                $conn->close();
                            ?>
                        </li>
                    </ul>
                </nav>
                <!-- End quản lý vé bán-->

            </div>
            <!-- //content -->
        </div>
        <!-- main content end-->

    </section>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.search-input').keyup(function(){
                var key = $(this).val();
                $.post('Search/search_suatchieu.php', {key_suatchieu: key}, function(data){
                    $('.tbody').html(data);
                });
            });
            $('.search-input-ve').keyup(function(){
                var key = $(this).val();
                $.post('Search/search_suatchieu.php', {key_ve: key}, function(data){
                    $('.tbody-ve').html(data);
                });
            });
        });
    </script>
    <?php require_once("./Layout_page/Layout_footer.php"); ?>

</body>

</html>
  