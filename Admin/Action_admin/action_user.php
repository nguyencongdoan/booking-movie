<?php
    $id_kh = "";
    $hoten = "";
    $hinh = "";
    $diachi = "";
    $sdt = "";
    $gioitinh = "";
    $email = "";
    $password = "";
    $id_quyen = "";
    $thongbao = "";
    $type = "";

    if(isset($_POST['create'])){
        $hoten = $_POST['hoten'];
        $diachi = $_POST['diachi'];
        $sdt = $_POST['sdt'];
        $gioitinh = $_POST['gioitinh'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $id_quyen = $_POST['id_quyen'];

        $hinh = basename($_FILES['hinh']['name']);
        $uploads = "../images/".$hinh;  

        $query = "SELECT * FROM khach_hang WHERE Email = '$email' OR Ho_ten = '$hoten";
        $result = $conn->query($query);
        if(!$result) echo 'Cau truy van bi loi';

        // neu khong tra ve dong nao thi email do chua dang ky
        if($result->num_rows == 0) {
            $pass = md5($password);
            $query = "INSERT INTO khach_hang (Ho_ten, Hinh, Dia_chi, SDT, Gioi_tinh, So_du, Email, Passwords, ID_Quyen) VALUES ('$hoten', '$hinh', '$diachi', '$sdt', '$gioitinh', 0, '$email', '$pass', '$id_quyen')";
            move_uploaded_file($_FILES['hinh']['tmp_name'], $uploads);

            if($conn->query($query) === TRUE) {
                header('Location: user.php?them=success');
            }
            else {
                $thongbao = "Thêm thông tin thất bại!";
                $type = "danger";
            }
        }
        else{
            $thongbao = "Email hoặc họ tên đã tồn tại. Vui lòng chọn Email hoặc họ tên khác!";
            $type = "danger";
        }
    }

    if(isset($_GET['id_kh'])){
        $id_kh = $_GET['id_kh'];
        $query = "SELECT * FROM khach_hang WHERE ID_KH = '$id_kh'";
        $result = $conn->query($query);
        if(!$result) echo 'Cau truy van bi loi';
        $row = $result->fetch_assoc();

        $hoten = $row['Ho_ten'];
        $diachi = $row['Dia_chi'];
        $sdt = $row['SDT'];
        $gioitinh = $row['Gioi_tinh'];
        $email = $row['Email'];
        $password = $row['Passwords'];
        $hinh = $row['Hinh'];
        $id_quyen = $row['ID_Quyen'];
    }

    if(isset($_POST['edit'])){
        $id_kh = $_POST['id_kh'];
        $hoten = $_POST['hoten'];
        $diachi = $_POST['diachi'];
        $sdt = $_POST['sdt'];
        $gioitinh = $_POST['gioitinh'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $id_quyen = $_POST['id_quyen'];

        $hinh = basename($_FILES['hinh']['name']);
        $uploads = "../images/".$hinh;  

        // lay ra password truoc khi thay doi
        $query = "SELECT * FROM khach_hang WHERE ID_KH = '$id_kh'";
        $result = $conn->query($query);
        if(!$result) echo 'Cau truy van bi loi';
        $row = $result->fetch_assoc();
        $pass = $row['Passwords'];

        $pass_md5 = md5($password);

        if($_FILES['hinh']['name'] != NULL && $pass == $password){
            $query = "UPDATE khach_hang SET Ho_ten='$hoten', Hinh='$hinh', Dia_chi='$diachi', SDT='$sdt', Gioi_tinh='$gioitinh', ID_Quyen='$id_quyen', Email='$email' WHERE ID_KH = $id_kh";
            move_uploaded_file($_FILES['hinh']['tmp_name'], $uploads);
        }
        else if($_FILES['hinh']['name'] != NULL && $pass != $password){
            $query = "UPDATE khach_hang SET Ho_ten='$hoten', Hinh='$hinh', Dia_chi='$diachi', SDT='$sdt', Gioi_tinh='$gioitinh', ID_Quyen='$id_quyen', Email='$email', Passwords='$pass_md5' WHERE ID_KH = $id_kh";
            move_uploaded_file($_FILES['hinh']['tmp_name'], $uploads);
        }
        else if($pass != $password){
            $query = "UPDATE khach_hang SET Ho_ten='$hoten', Dia_chi='$diachi', SDT='$sdt', Gioi_tinh='$gioitinh', ID_Quyen='$id_quyen', Email='$email', Passwords='$pass_md5' WHERE ID_KH = $id_kh";
        }
        else{
            $query = "UPDATE khach_hang SET Ho_ten='$hoten', Dia_chi='$diachi', SDT='$sdt', Gioi_tinh='$gioitinh', ID_Quyen='$id_quyen', Email='$email' WHERE ID_KH = $id_kh";
        }

        // Nếu quản trị viên chuyển quyền hạn của khách hàng sang admin hoặc nhân viên
        if($id_quyen != 1){
            if($pass != $password){
                $query = "INSERT INTO nhan_vien (HoTen, Hinh, Dia_chi, SDT, Gioi_tinh, ID_Quyen, Email, Passwords) VALUES ('$hoten', '$hinh', '$diachi', '$sdt', '$gioitinh', '$id_quyen', '$email', '$pass_md5')";
                move_uploaded_file($_FILES['hinh']['tmp_name'], $uploads);
            }
            else{
                $query = "INSERT INTO nhan_vien (HoTen, Hinh, Dia_chi, SDT, Gioi_tinh, ID_Quyen, Email, Passwords) VALUES ('$hoten', '$hinh', '$diachi', '$sdt', '$gioitinh', '$id_quyen', '$email', '$pass')";
                move_uploaded_file($_FILES['hinh']['tmp_name'], $uploads);
            }

            $query_xoa_kh = "DELETE FROM khach_hang WHERE ID_KH = '$id_kh'";
        }

        if($conn->query($query) === TRUE && $conn->query($query_xoa_kh) === TRUE) {
            header('Location: user.php?sua=success');
        }
        else {
            $thongbao = "Cập nhật thông tin thất bại";
            $type = "danger";
        }
    }

?>