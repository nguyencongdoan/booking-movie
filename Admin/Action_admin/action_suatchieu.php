<?php
    $id_sc = "";
    $ngaychieu = "";
    $giobd = "";
    $giokt = "";
    $tinhtrang = "";
    $id_phim = "";
    $id_rap = "";

    if(isset($_POST['create'])){
        $id_phim = $_POST['id_phim'];
        $id_rap = $_POST['id_rap'];
        $ngaychieu = $_POST['ngaychieu'];
        $giobd = $_POST['giobd'];
        $giokt = $_POST['giokt'];
        $tinhtrang = $_POST['tinhtrang'];

        $query = "INSERT INTO suat_chieu (Ngay_chieu, Gio_BD, Gio_KT, Tinh_trang_chieu, ID_Phim, ID_Rap) VALUES ('$ngaychieu', '$giobd', '$giokt', '$tinhtrang', '$id_phim', '$id_rap')";

        if($conn->query($query) === TRUE) {
            header('Location: suatchieu_ve.php?them=success');
        }
        else {
            $thongbao = "Thêm thông tin thất bại";
            $type = "danger";
        }
    }

    if(isset($_GET['id_sc'])){
        $id_sc = $_GET['id_sc'];
        $query = "SELECT * FROM suat_chieu WHERE ID_SC = '$id_sc'";
        $result = $conn->query($query);
        if(!$result) echo "Câu truy vấn bị lỗi";
        $row = $result->fetch_assoc();

        $ngaychieu = $row['Ngay_chieu'];
        $giobd = $row['Gio_BD'];
        $giokt = $row['Gio_KT'];
        $tinhtrang = $row['Tinh_trang_chieu'];
        $id_phim = $row['ID_Phim'];
        $id_rap = $row['ID_Rap'];
    }

    if(isset($_POST['edit'])){
        $id_sc = $_POST['id_sc'];
        $id_phim = $_POST['id_phim'];
        $id_rap = $_POST['id_rap'];
        $ngaychieu = $_POST['ngaychieu'];
        $giobd = $_POST['giobd'];
        $giokt = $_POST['giokt'];
        $tinhtrang = $_POST['tinhtrang'];

        $query = "UPDATE suat_chieu SET Ngay_chieu='$ngaychieu', Gio_BD='$giobd', Gio_KT='$giokt', Tinh_trang_chieu='$tinhtrang', ID_Phim='$id_phim', ID_Rap='$id_rap' WHERE ID_SC = '$id_sc'";
    
        if($conn->query($query) === TRUE) {
            header('Location: suatchieu_ve.php?sua=success');
        }
        else {
            $thongbao = "Cập nhật thông tin thất bại";
            $type = "danger";
        }
    }
?>