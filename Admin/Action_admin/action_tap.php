<?php
    
    $id_phim = "";
    $tentap = "";
    $url_video = "";
    $thongbao = "";
    $type = "";

    if(isset($_POST['create'])){
        $id_phim = $_POST['id_phim'];
        $tentap = $_POST['tentap'];
        $url_video = basename($_FILES['video']['name']);
        $uploads = "../videos/".$url_video;

        $query = "INSERT INTO tap_phim (Ten_tap, Url_video, ID_phim) VALUES ('$tentap', '$url_video', '$id_phim')";
        move_uploaded_file($_FILES['video']['tmp_name'], $uploads);

        if($conn->query($query) === TRUE){
            header('Location: movie.php?them=success');
        }
        else{
            $thongbao = "Thêm mới thất bại";
            $type = "danger";
        }
    }

    if(isset($_GET['id_tap'])){
        $id_tap = $_GET['id_tap'];
        $query = "SELECT * FROM tap_phim WHERE ID_Tap = '$id_tap'";
        $result = $conn->query($query);
        if(!$result) echo "Câu truy vấn bị lỗi";

        $row = $result->fetch_assoc();
        $id_tap = $row['ID_Tap'];
        $tentap = $row['Ten_tap'];
        $url_video = $row['Url_video'];
        $id_phim = $row['ID_Phim'];
    }

    if(isset($_POST['edit'])){
        $id_tap = $_POST['id_tap'];
        $tentap = $_POST['tentap'];
        $id_phim = $_POST['id_phim'];

        if($_FILES['video']['name'] != NULL){
            $url_video = basename($_FILES['video']['name']);
            $uploads = "../videos/".$url_video;  
            $query = "UPDATE tap_phim SET Ten_tap='$tentap', Url_video='$url_video', ID_phim='$id_phim' WHERE ID_Tap = '$id_tap'";
            move_uploaded_file($_FILES['video']['tmp_name'], $uploads);
        }
        else{
            $query = "UPDATE tap_phim SET Ten_tap='$tentap', ID_phim='$id_phim' WHERE ID_Tap = '$id_tap'";
        }

        if($conn->query($query) === TRUE) {
            header('Location: movie.php?sua=success');
        }
        else {
            $thongbao = "Cập nhật thông tin thất bại";
            $type = "danger";
        }
    }
?>