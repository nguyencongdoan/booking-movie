<?php
    
    $id_phim = "";
    $tenphim = "";
    $thoiluong = "";
    $hinh = "";
    $tomtat = "";
    $dienvien = "";
    $daodien = "";
    $quocgia = "";  
    $nam_phat_hanh = "";
    $id_tl = "";
    $thongbao = "";
    $type = "";
    $luot_xem = "";

    if(isset($_POST['create'])){
        $tenphim = $_POST['tenphim'];
        $thoiluong = $_POST['thoiluong'];
        $tomtat = $_POST['tomtat'];
        $dienvien = $_POST['dienvien'];
        $daodien = $_POST['daodien'];
        $quocgia = $_POST['quocgia'];
        $nam_phat_hanh = $_POST['namph'];
        $id_tl = $_POST['id_tl'];

        $hinh = basename($_FILES['hinh']['name']);
        $uploads = "../images/".$hinh;  

        $query = "INSERT INTO phim (Ten_phim, Thoi_luong, Hinh, Tom_tat, Luot_xem, Dien_vien, Dao_dien, Quoc_gia, Nam_phat_hanh, ID_TL) VALUES ('$tenphim', '$thoiluong', '$hinh', '$tomtat', 0, '$dienvien', '$daodien', '$quocgia', '$nam_phat_hanh', '$id_tl')";
        move_uploaded_file($_FILES['hinh']['tmp_name'], $uploads);

        if($conn->query($query) === TRUE){
            header('Location: movie.php?them=success');
        }
        else{
            $thongbao = "Thêm mới thất bại";
            $type = "danger";
        }
    }

    if(isset($_GET['id_phim'])){
        $id_phim = $_GET['id_phim'];
        $query = "SELECT * FROM phim WHERE ID_phim = '$id_phim'";
        $result = $conn->query($query);
        $row = $result->fetch_assoc();

        $id_phim = $row['ID_phim'];
        $tenphim = $row['Ten_phim'];
        $thoiluong = $row['Thoi_luong'];
        $hinh = $row['Hinh'];
        $tomtat = $row['Tom_tat'];
        $dienvien = $row['Dien_vien'];
        $daodien = $row['Dao_dien'];
        $quocgia = $row['Quoc_gia'];
        $nam_phat_hanh = $row['Nam_phat_hanh'];
        $luot_xem = $row['Luot_xem'];
        $id_tl = $row['ID_TL'];
    }

    if(isset($_POST['edit'])){
        $id_phim = $_POST['id_phim'];
        $tenphim = $_POST['tenphim'];
        $thoiluong = $_POST['thoiluong'];
        $tomtat = $_POST['tomtat'];
        $dienvien = $_POST['dienvien'];
        $daodien = $_POST['daodien'];
        $quocgia = $_POST['quocgia'];
        $nam_phat_hanh = $_POST['namph'];
        $luot_xem = $_POST['luot_xem'];
        $id_tl = $_POST['id_tl'];

        if($_FILES['hinh']['name'] != NULL){
            $hinh = basename($_FILES['hinh']['name']);
            $uploads = "../images/".$hinh;  
            $query = "UPDATE phim SET Ten_phim='$tenphim', Thoi_luong='$thoiluong', Hinh='$hinh', Tom_tat='$tomtat', Luot_xem='$luot_xem', Dien_vien='$dienvien', Dao_dien='$daodien', Quoc_gia='$quocgia', Nam_phat_hanh='$nam_phat_hanh', ID_TL='$id_tl' WHERE ID_Phim='$id_phim'";
            move_uploaded_file($_FILES['hinh']['tmp_name'], $uploads);
        }
        else{
            $query = "UPDATE phim SET Ten_phim='$tenphim', Thoi_luong='$thoiluong', Tom_tat='$tomtat', Luot_xem='$luot_xem', Dien_vien='$dienvien', Dao_dien='$daodien', Quoc_gia='$quocgia', Nam_phat_hanh='$nam_phat_hanh', ID_TL='$id_tl' WHERE ID_Phim='$id_phim'";
        }

        if($conn->query($query) === TRUE) {
            header('Location: movie.php?sua=success');
        }
        else {
            $thongbao = "Cập nhật thông tin thất bại";
            $type = "danger";
        }
    }
?>