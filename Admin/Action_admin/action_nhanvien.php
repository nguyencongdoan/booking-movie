<?php
    $id_nv = "";
    $hoten = "";
    $hinh = "";
    $diachi = "";
    $sdt = "";
    $gioitinh = "";
    $id_quyen = "";
    $thongbao = "";
    $type = "";
    $email = ""; 
    $password = "";

    if(isset($_POST['create'])){
        $hoten = $_POST['hoten'];
        $diachi = $_POST['diachi'];
        $sdt = $_POST['sdt'];
        $gioitinh = $_POST['gioitinh'];
        $id_quyen = $_POST['id_quyen'];
        $email = $_POST['email'];
        $password = $_POST['password'];

        $hinh = basename($_FILES['hinh']['name']);
        $uploads = "../images/".$hinh;  

        $query = "SELECT * FROM nhan_vien WHERE HoTen = '$hoten' OR Email = '$email'";
        $result = $conn->query($query);
        if(!$result) echo 'Cau truy van bi loi';

        // neu khong tra ve dong nao thi email do chua dang ky
        if($result->num_rows == 0) {
            $pass = md5($password);
            $query = "INSERT INTO nhan_vien (HoTen, Hinh, Dia_chi, SDT, Gioi_tinh, ID_Quyen, Email, Passwords) VALUES ('$hoten', '$hinh', '$diachi', '$sdt', '$gioitinh', '$id_quyen', '$email', '$pass')";
            move_uploaded_file($_FILES['hinh']['tmp_name'], $uploads);

            if($conn->query($query) === TRUE) {
                header('Location: nhanvien.php?them=success');
            }
            else {
                $thongbao = "Thêm thông tin thất bại!";
                $type = "danger";
            }
        }
        else{
            $thongbao = "Email hoặc họ tên đã tồn tại. Vui lòng chọn Email hoặc họ tên khác!";
            $type = "danger";
        }
    }

    if(isset($_GET['id_nv'])){
        $id_nv = $_GET['id_nv'];
        $query = "SELECT * FROM nhan_vien WHERE ID_NV = '$id_nv'";
        $result = $conn->query($query);
        if(!$result) echo 'Cau truy van bi loi';
        $row = $result->fetch_assoc();

        $hoten = $row['HoTen'];
        $diachi = $row['Dia_chi'];
        $sdt = $row['SDT'];
        $gioitinh = $row['Gioi_tinh'];
        $hinh = $row['Hinh'];
        $id_quyen = $row['ID_Quyen'];
        $email = $row['Email'];
        $password = $row['Passwords'];
    }

    if(isset($_POST['edit'])){
        $id_nv = $_POST['id_nv'];
        $hoten = $_POST['hoten'];
        $diachi = $_POST['diachi'];
        $sdt = $_POST['sdt'];
        $gioitinh = $_POST['gioitinh'];
        $id_quyen = $_POST['id_quyen'];
        $email = $_POST['email'];
        $password = $_POST['password'];

        $hinh = basename($_FILES['hinh']['name']);
        $uploads = "../images/".$hinh;  

        $pass_md5 = md5($password); // password nhận từ trang edit_nhanvien

        // lay ra password truoc khi thay doi
        $query = "SELECT * FROM nhan_vien WHERE ID_NV = '$id_nv'";
        $result = $conn->query($query);
        if(!$result) echo 'Cau truy van bi loi';
        $row = $result->fetch_assoc();
        $pass = $row['Passwords'];

        // khi admin chuyển quyền hạn sang user thì sẽ xóa nhân viên đó khỏi bảng nhan_vien và thêm user đó vào bảng khách hàng
        if($id_quyen == 1){
            $query = "DELETE FROM nhan_vien WHERE ID_NV = '$id_nv'";

            // thêm nhân viên vào bảng khach_hang
            if($pass != $password){ // nếu quản trị viên thay đổi mật khẩu trước khi chuyến sang user
                $query_insert_kh = "INSERT INTO khach_hang (Ho_ten, Hinh, Dia_chi, SDT, Gioi_tinh, So_du, Email, Passwords, ID_Quyen) VALUES ('$hoten', '$hinh', '$diachi', '$sdt', '$gioitinh', 0, '$email', '$pass_md5', '$id_quyen')";
                move_uploaded_file($_FILES['hinh']['tmp_name'], $uploads);
            }
            else{
                $query_insert_kh = "INSERT INTO khach_hang (Ho_ten, Hinh, Dia_chi, SDT, Gioi_tinh, So_du, Email, Passwords, ID_Quyen) VALUES ('$hoten', '$hinh', '$diachi', '$sdt', '$gioitinh', 0, '$email', '$password', '$id_quyen')";
                move_uploaded_file($_FILES['hinh']['tmp_name'], $uploads);

            }
           
            $conn->query($query_insert_kh);
        }
        else{ // ngược lại cập nhật lại thông tin cho nhân viên
            
            // kiêm tra nếu quản trị viên chỉ thay đổi hình nhưng không thay đổi password
            if($_FILES['hinh']['name'] != NULL && $pass == $password){
                $query = "UPDATE nhan_vien SET HoTen='$hoten', Hinh='$hinh', Dia_chi='$diachi', SDT='$sdt', Gioi_tinh='$gioitinh', ID_Quyen='$id_quyen', Email='$email' WHERE ID_NV = '$id_nv'";
                move_uploaded_file($_FILES['hinh']['tmp_name'], $uploads);
            }
            else if ($_FILES['hinh']['name'] != NULL && $pass != $password){ // Kiểm tra nếu quản trị viên thay đổi hình và password
                $query = "UPDATE nhan_vien SET HoTen='$hoten', Hinh='$hinh', Dia_chi='$diachi', SDT='$sdt', Gioi_tinh='$gioitinh', ID_Quyen='$id_quyen', Email='$email', Passwords='$pass_md5' WHERE ID_NV = '$id_nv'";
                move_uploaded_file($_FILES['hinh']['tmp_name'], $uploads);
            }
            else if($pass != $password){ // kiêm tra nếu quản trị viên chỉ thay đổi password
                $query = "UPDATE nhan_vien SET HoTen='$hoten', Dia_chi='$diachi', SDT='$sdt', Gioi_tinh='$gioitinh', ID_Quyen='$id_quyen', Email='$email', Passwords='$pass_md5' WHERE ID_NV = '$id_nv'";
            }
            else{
                $query = "UPDATE nhan_vien SET HoTen='$hoten', Dia_chi='$diachi', SDT='$sdt', Gioi_tinh='$gioitinh', ID_Quyen='$id_quyen', Email='$email' WHERE ID_NV = '$id_nv'";
            }
        }

        if($conn->query($query) === TRUE) {
            header('Location: nhanvien.php?sua=success');
        }
        else {
            $thongbao = "Cập nhật thông tin thất bại";
            $type = "danger";
        }
    }

?>