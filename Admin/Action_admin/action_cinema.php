<?php
    $id_rap = "";
    $tenrap = "";
    $diachi = "";
    $email = "";
    $sdt = "";
    $url_map = "";

    if(isset($_POST['create'])){
        $tenrap = $_POST['tenrap'];
        $diachi = $_POST['diachi'];
        $email = $_POST['email'];
        $sdt = $_POST['sdt'];
        $url_map = $_POST['url_map'];

        $query = "INSERT INTO rap_phim (Ten_Rap, Dia_chi, Email, SDT, Url_map) VALUES ('$tenrap', '$diachi', '$email', '$sdt', '$url_map')";
        if($conn->query($query) === TRUE){
            header('Location: cinema_admin.php?them=success');
        }
        else{
            $thongbao = "Thêm mới thất bại";
            $type = "danger";
        }
    }

    if(isset($_GET['id_rap'])){
        $id_rap = $_GET['id_rap'];
        $query = "SELECT * FROM rap_phim WHERE ID_Rap = '$id_rap'";
        $result = $conn->query($query);
        if(!$result) echo "Câu truy vấn bị lỗi";

        $row = $result->fetch_assoc();
        $id_rap = $row['ID_Rap'];
        $tenrap = $row['Ten_rap'];
        $diachi = $row['Dia_chi'];
        $email = $row['Email'];
        $sdt = $row['SDT'];
        $url_map = $row['Url_map'];
    }

    if(isset($_POST['edit'])){
        $id_rap = $_POST['id_rap'];
        $tenrap = $_POST['tenrap'];
        $diachi = $_POST['diachi'];
        $email = $_POST['email'];
        $sdt = $_POST['sdt'];
        $url_map = $_POST['url_map'];

        $query = "UPDATE rap_phim SET Ten_rap='$tenrap', Dia_chi='$diachi', Email='$email', SDT='$sdt', Url_map='$url_map' WHERE ID_Rap = '$id_rap'";

        if($conn->query($query) === TRUE) {
            header('Location: cinema_admin.php?sua=success');
        }
        else {
            $thongbao = "Cập nhật thông tin thất bại";
            $type = "danger";
        }
    }
?>