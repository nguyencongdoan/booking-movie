<?php
    $id_dv = "";
    $ngaydat = "";
    $soluong = 1;
    $id_kh = "";
    $id_phim = "";
    $id_rap = "";
    $id_sc = "";
    $tenrap = "";
    $tensc = "";
    $tenphim = "";
    $id_nv = "";
    $id_hd = "";
    $phuong_thuc_tt = "";
    $id_phong = "";

    if(isset($_POST['create'])){
        $ngaydat = $_POST['ngaydat'];
        $soluong = $_POST['soluong'];
        $id_kh = $_POST['id_kh'];
        $id_phim = $_POST['id_phim'];
        $id_rap = $_POST['id_rap'];
        $id_sc = $_POST['id_sc'];   
        $id_nv = $_POST['id_nv'];   
        $phuong_thuc_tt = $_POST['phuong_thuc_tt'];
        $id_phong = $_POST['id_phong'];

        if(isset($_POST['ds_ghe'])){
            $ds_ghe = $_POST['ds_ghe'];
            $ds = implode(', ', $ds_ghe);
        } 
        
        // Kiểm tra nếu chưa chọn thông tin suất chiếu hoặc thông tin rạp phim mà bấm nút đặt vé thì thông báo
        if(empty($id_sc) || empty($id_phim))
        {
            $thongbao = "Vui lòng chọn đầy đủ thông tin trước khi đặt vé!";
            $type = "danger";
        } 
        else if(empty($ds_ghe)){ // Kiểm tra nếu chưa chọn vị trí ngồi mà đặt vé thì thông báo
            $thongbao = "Vui lòng chọn vị trí ghế ngồi trước khi đặt vé!";
            $type = "danger";
        }
        else{
            // xử lý để lấy ra tên rạp phim
            if(isset($id_rap)){
                $query = "SELECT * FROM rap_phim WHERE ID_Rap = '$id_rap'";
                $result = $conn->query($query);
                if(!$result) echo "Câu truy vấn bị lỗi";
                $row = $result->fetch_assoc();
                $tenrap = $row['Ten_rap'];
            }

            // xử lý để lấy ra thông tin suất chiếu
            if(isset($id_sc)){
                $query = "SELECT * FROM suat_chieu WHERE ID_SC = '$id_sc'";
                $result = $conn->query($query);
                if(!$result) echo "Câu truy vấn bị lỗi";
                $row = $result->fetch_assoc();
                $tensc = $row['Gio_BD'] .' ~ '.$row['Gio_KT'].' '.$row['Ngay_chieu'];
            }

            // xử lý để lấy ra tên phim
            if(isset($id_phim)){
                $query = "SELECT * FROM phim WHERE ID_phim = '$id_phim'";
                $result = $conn->query($query);
                if(!$result) echo "Câu truy vấn bị lỗi";
                $row = $result->fetch_assoc();
                $tenphim = $row['Ten_phim'];
            }

            // xử lý để lấy ra tên phòng chiếu
            if(isset($id_phim)){
                $query = "SELECT * FROM phong_chieu WHERE ID_Phong = '$id_phong'";
                $result = $conn->query($query);
                if(!$result) echo "Câu truy vấn bị lỗi";
                $row = $result->fetch_assoc();
                $tenphim = $row['Ten_phong'];
            }
            
            // Kiểm tra nếu số lượng ghế chọn nhỏ hơn số lượng đặt mua hoặc số lượng ghế chọn lớn hơn số lượng đặt mua thì thông báo
            if(count($ds_ghe) < $soluong || count($ds_ghe) > $soluong){
                $thongbao = "Vui lòng chọn đủ số lượng ghế!";
                $type = "danger";
            }
            else{ // ngược lại thì tiến hành đặt vé
                $query = "INSERT INTO dat_ve (So_luong, Ngay_dat, List_ghe, ID_KH, ID_Phim, ID_SC, ID_Rap, ID_Phong) VALUES ('$soluong', '$ngaydat', '$ds', '$id_kh', '$id_phim', '$id_sc', '$id_rap', '$id_phong')";

                if($conn->query($query) === TRUE) {
                    $max_id_dv = "SELECT MAX(ID_Dat) FROM dat_ve";
                    $result = $conn->query($max_id_dv);
                    if(!$result) echo "Câu truy vấn bị lỗi";
                    $row = $result->fetch_assoc();
                    $id_dv = $row['MAX(ID_Dat)'];

                    if($phuong_thuc_tt == '1') $phuong_thuc_tt = "Thanh toán tại rạp";
                    else $phuong_thuc_tt = "Thanh toán Online";
                    
                    $query_hd = "INSERT INTO hoa_don (ID_KH, ID_NV, ID_Dat, Phuong_thuc_tt, Tinh_trang) VALUES ('$id_kh', '$id_nv', '$id_dv', '$phuong_thuc_tt', 0)";
                   
                    if($conn->query($query_hd) === TRUE) {
                        $max_id_hd = "SELECT MAX(ID_HD) FROM hoa_don";
                        $result = $conn->query($max_id_hd);
                        if(!$result) echo "Câu truy vấn bị lỗi";
                        $row = $result->fetch_assoc();
                        $id_hd = $row['MAX(ID_HD)'];
                        
                        $total = $soluong * 45000;
                        $query_cthd = "INSERT INTO chi_tiet_hd (ID_HD, Ngay_dat, So_luong, List_ghe, Tong_tien, ID_SC, ID_Phim, ID_Rap, ID_Phong) VALUES ('$id_hd', '$ngaydat', '$soluong', '$ds', '$total', '$id_sc', '$id_phim', '$id_rap', '$id_phong')";
                        
                        if($conn->query($query_cthd) === TRUE) {
                            header('Location: dat_ve.php?them=success');
                        }
                        else {
                            $thongbao = "Thêm thông tin chi tiết hóa đơn thất bại!";
                            $type = "danger";
                        }
                    }
                    else {
                        $thongbao = "Thêm thông tin hóa đơn thất bại!";
                        $type = "danger";
                    }
                }
                else {
                    $thongbao = "Thêm thông tin thất bại!";
                    $type = "danger";
                }
            }
        }
    }

    if(isset($_GET['id_dat'])){
        $id_dv = $_GET['id_dat'];
        $query = "SELECT * FROM dat_ve, hoa_don WHERE dat_ve.ID_Dat = hoa_don.ID_Dat AND dat_ve.ID_Dat = '$id_dv'";
        $result = $conn->query($query);
        if(!$result) echo "Câu truy vấn bị lỗi";

        $row = $result->fetch_assoc();

        $id_rap = $row['ID_Rap'];
        $id_sc = $row['ID_SC'];
        $id_phim = $row['ID_Phim'];
        $soluong = $row['So_luong'];
        $ngaydat = $row['Ngay_dat'];
        $list_ghe = $row['List_ghe'];
        $id_kh = $row['ID_KH'];
        $id_hd = $row['ID_HD'];
        $id_phong = $row['ID_Phong'];
        $phuong_thuc_tt = $row['Phuong_thuc_tt'];

        // xử lý để lấy ra tên rạp phim
        if(isset($id_rap)){
            $query = "SELECT * FROM rap_phim WHERE ID_Rap = '$id_rap'";
            $result = $conn->query($query);
            if(!$result) echo "Câu truy vấn bị lỗi";
            $row = $result->fetch_assoc();
            $tenrap = $row['Ten_rap'];
        }

        // lấy thời gian suất chiếu
        if(isset($id_sc)){
            $query = "SELECT * FROM suat_chieu WHERE ID_SC = '$id_sc'";
            $result = $conn->query($query);
            if(!$result) echo "Câu truy vấn bị lỗi";
            $row = $result->fetch_assoc();
            $tensc = $row['Gio_BD'] .' ~ '.$row['Gio_KT'].' '.$row['Ngay_chieu'];
        }
        
        // lấy tên phim
        if(isset($id_phim)){
            $query = "SELECT * FROM phim WHERE ID_phim = '$id_phim'";
            $result = $conn->query($query);
            if(!$result) echo "Câu truy vấn bị lỗi";
            $row = $result->fetch_assoc();
            $tenphim = $row['Ten_phim'];
            $hinh = $row['Hinh'];
        }

        // lấy tên phòng
        if(isset($id_phong)){
            $query = "SELECT * FROM phong_chieu WHERE ID_phong = '$id_phong'";
            $result = $conn->query($query);
            if(!$result) echo "Câu truy vấn bị lỗi";
            $row = $result->fetch_assoc();
            $tenphong = $row['Ten_phong'];
        }

        // xử lý để lấy ra danh sách các dãy ghế đã chọn của bộ phim A thuộc rạp chiếu phim A và xuất chiếu của A 
        $query = "SELECT * FROM dat_ve, phim, suat_chieu, rap_phim WHERE dat_ve.ID_Phim = phim.ID_Phim AND dat_ve.ID_SC = suat_chieu.ID_SC AND dat_ve.ID_Rap = rap_phim.ID_Rap AND dat_ve.ID_Phim = '$id_phim' AND dat_ve.ID_Rap = '$id_rap' AND dat_ve.ID_SC = '$id_sc'";
        $result = $conn->query($query);
        
        if(!$result) echo "Câu truy vấn bị lỗi";
        $i = 0;
        
        if($result->num_rows != 0){
            while($row = $result->fetch_array()){
                $ds_ghe[$i] = $row['List_ghe'];
                $i++;
            }
        }

        $ds = implode(", ", $ds_ghe);
        $list = explode(", ", $ds); // danh sách ghế đã chọn của rạp 
        // end
    }

    if(isset($_POST['edit'])){
        $id_dv = $_POST['id_dat'];
        $ngaydat = $_POST['ngaydat'];
        $soluong = $_POST['soluong'];
        $id_kh = $_POST['id_kh'];
        $id_phim = $_POST['id_phim'];
        $id_rap = $_POST['id_rap'];
        $id_sc = $_POST['id_sc'];   
        $id_nv = $_POST['id_nv'];
        $phuong_thuc_tt = $_POST['phuong_thuc_tt'];
        $id_hd = $_POST['id_hd'];
        $id_phong = $_POST['id_phong'];
        $ds_ghe = $_POST['ds_ghe'];
        $ds = implode(', ', $ds_ghe);

        if(empty($ds_ghe)){ // Kiểm tra nếu chưa chọn vị trí ngồi mà đặt vé thì thông báo
            $thongbao = "Vui lòng chọn vị trí ghế ngồi!";
            $type = "danger";
        }
        else{
            // Kiểm tra nếu số lượng ghế chọn nhỏ hơn số lượng đặt mua hoặc số lượng ghế chọn lớn hơn số lượng đặt mua thì thông báo
            if(count($ds_ghe) < $soluong || count($ds_ghe) > $soluong){
                $thongbao = "Vui lòng chọn đủ số lượng ghế!";
                $type = "danger";
            }
            else{
                $query = "UPDATE dat_ve SET So_luong = '$soluong', Ngay_dat = '$ngaydat', List_ghe = '$ds', ID_KH = '$id_kh', ID_Phim = '$id_phim', ID_SC = '$id_sc', ID_Rap = '$id_rap', ID_Phong = '$id_phong' WHERE dat_ve.ID_Dat = '$id_dv'";
            
                if($conn->query($query) === TRUE){

                    if($phuong_thuc_tt == 1) $phuong_thuc_tt = "Thanh toán tại rạp";
                    else $phuong_thuc_tt = "Thanh toán Online";

                    $query_hd = "UPDATE hoa_don SET ID_KH = '$id_kh', ID_NV = '$id_nv', ID_Dat = '$id_dv', Phuong_thuc_tt = '$phuong_thuc_tt', Tinh_trang = 0 WHERE hoa_don.ID_HD = '$id_hd'";

                    if($conn->query($query_hd) === TRUE){
                        $total = $soluong * 45000;
                        $query_cthd = "UPDATE chi_tiet_hd SET Ngay_dat = '$ngaydat', So_luong = '$soluong', List_ghe = '$list_ghe', Tong_tien = '$total', ID_SC = '$id_sc', ID_Phim = '$id_phim', ID_Rap = '$id_rap', ID_Phong = '$id_phong' WHERE ID_HD = '$id_hd'";
                        
                        if($conn->query($query_cthd) === TRUE) {
                            header('Location: dat_ve.php?sua=success');
                        }
                        else {
                            $thongbao = "Chỉnh sửa thông tin chi tiết hoá đơn thất bại!";
                            $type = "danger";
                        }
                    }
                    else {
                        $thongbao = "Chỉnh sửa thông tin hoá đơn thất bại!";
                        $type = "danger";
                    }
                }
                else {
                    $thongbao = "Chỉnh sửa thông tin thất bại!";
                    $type = "danger";
                }
            }
        }
    }

?>