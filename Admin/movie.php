<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Quản lý phim</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-starter.css">

    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">

    <!-- AJAX -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>
</head>

<body class="sidebar-menu-collapsed">
    <section>
        <?php 
            include '../Model/config.php';
            include './Action_admin/action_movie.php';
            require_once("./Layout_page/Layout_header.php"); 

            if(isset($_GET['delete'])){
                $id_phim = $_GET['delete'];
        
                $sql = "DELETE FROM phim WHERE ID_phim = '$id_phim' ";
                if($conn->query($sql) === TRUE){
                    $_SESSION['thongbao'] = "Xóa thành công!";
                    $_SESSION['type'] = 'success';
                }
                else{
                    $_SESSION['thongbao'] = "Xóa thất bại!";
                    $_SESSION['type'] = 'danger';
                }
            }

            if(isset($_GET['delete_tap'])){
                $id_tap = $_GET['delete_tap'];
                $sql = "DELETE FROM tap_phim WHERE ID_Tap = '$id_tap'";
        
                if($conn->query($sql) === TRUE){
                    $_SESSION['thongbao'] = "Xóa thành công!";
                    $_SESSION['type'] = 'success';
                }
                else{
                    $_SESSION['thongbao'] = "Xóa thất bại!";
                    $_SESSION['type'] = 'danger';
                }
            }
        ?>

        <!-- main content start -->
        <div class="main-content">
            <!-- content -->
            <div class="container-fluid content-top-gap">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Quản lý phim & tập phim</li>
                    </ol>
                    <h2 class="fw-bold text-center h2" style="color: rebeccapurple;">QUẢN LÝ PHIM</h2>
                    <div class="search-ajax">
                        <a href="create_movie.php" class="btn mt-2 mb-3">Thêm mới</a>
                        <div class="search-box">
                            <form action="" method="post">
                                <input class="search-input" placeholder="Search Here..." type="search" id="search">
                                <button class="search-submit" type="submit" value=""><span class="fa fa-search"></span></button>
                            </form>
                        </div>
                    </div>
                </nav>

                <?php
                    if(isset($_GET['them']) || isset($_GET['sua'])){
                        if(isset($_GET['them']) && $_GET['them'] == 'success'){
                            $_SESSION['thongbao'] = "Thêm mới thành công!";
                        }
                        if(isset($_GET['sua']) && $_GET['sua'] == 'success'){
                            $_SESSION['thongbao'] = "Sửa thông tin thành công";
                        }
                        $_SESSION['type'] = 'success';
                    }
                ?>

                <?php if(isset($_SESSION['thongbao'])){ ?>
                    <div class="col-md-10 alert alert-<?= $_SESSION['type']; ?> mb-5" align="center">
                        <?php echo $_SESSION['thongbao']; ?>
                    </div>
                <?php } unset($_SESSION['thongbao']);?>
                
                <table class="table">
                    <thead>
                        <tr>
                            <th width="170px" class="text-start">Tên phim</th>
                            <th width="140px">Thời lượng</th>
                            <th width="140px">Lượt xem</th>
                            <th width="400px">Tóm tắt</th>
                            <th>Quốc gia</th>
                            <th>Thể loại</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="tbody">
                        <?php
                            if(!isset($_GET['page'])){
                                $_GET['page'] = 1;
                            }
                            $rowPerPage = 6;
                            // vị trí của mẩu tin đầu tiên trên mỗi trang
                            $offset = ($_GET['page'] - 1) * $rowPerPage;

                            $query = "SELECT * FROM phim, the_loai WHERE phim.ID_TL = the_loai.ID_TL LIMIT $offset, $rowPerPage";
                            $result = $conn->query($query);
                            if(!$result) echo "Câu truy vấn bị lỗi";

                            if($result->num_rows != 0) {
                                while($row = $result->fetch_array()){ ?>
                                    <tr>
                                        <td>
                                            <?= $row['Ten_phim'] ?>
                                        </td>

                                        <td>
                                            <?= $row['Thoi_luong'] ?>
                                        </td>

                                        <td>
                                            <?= $row['Luot_xem'] ?>
                                        </td>

                                        <td class="text-justify text-table">
                                            <?= $row['Tom_tat'] ?>
                                        </td>

                                        <td>
                                            <?= $row['Quoc_gia'] ?>
                                        </td>

                                        <td>
                                            <?= $row['Ten_TL'] ?>
                                        </td>

                                        <td>
                                            <a href="Action_admin/action_movie.php" class="btn btn-table-details">Xem chi tiết</a>
                                            <a href="edit_movie.php?id_phim=<?= $row['ID_phim']; ?>" class="btn btn-table-edit">Sửa</a>
                                            <a href="movie.php?delete=<?= $row['ID_phim']; ?>" class="btn btn-table-delete" onclick="return confirm('Bạn có chắc muốn xóa?');">Xóa</a>
                                        </td>
                                    </tr>
                                <?php }
                            }
                            else{ ?>
                                <tr>
                                    <td colspan="6" class="text-center" style="font-size:18px; height:160px; color: rebeccapurple;">
                                        <img src="../images/thongbao.png" class="img-search">
                                    </td>
                                </tr>
                            <?php }
                        ?>
                    </tbody>
                </table>
                
                <?php
                    $re = $conn->query("SELECT * FROM phim, the_loai WHERE phim.ID_TL = the_loai.ID_TL");
                    $numRows = mysqli_num_rows($re);
                    $maxPage = ceil($numRows/$rowPerPage);
                ?>

                <nav class="d-flex justify-content-end p-4" aria-label="Page navigation example">
                    <ul class="pagination">
                    <li class="page-item">
                            <?php
                                if($_GET["page"] > 1){ ?>
                                    <a class="page-link" href="movie.php?&page=<?= $_GET['page'] - 1 ?>" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                <?php }
                            ?>
                        </li>
                        <?php 
                            for ($i=1 ; $i<=$maxPage ; $i++)
                            {
                                if($i == $_GET['page']){ ?>
                                    <li class="page-item">
                                        <a class="page-link active" href="movie.php?&page=<?= $i ?>"><?= $i ?></a> <!-- trang hiện tại sẽ được style -->
                                    </li>
                                <?php }
                                else { ?>
                                    <li class="page-item">
                                        <a class="page-link" href="movie.php?&page=<?= $i ?>"><?= $i ?></a> 
                                    </li>
                                <?php }
                            }
                        ?>
                        <li class="page-item">
                            <?php
                                if($_GET["page"] < $maxPage){ ?>
                                    <a class="page-link" href="movie.php?&page=<?= $_GET['page'] + 1 ?>" aria-label="Previous">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                <?php }
                            ?>
                        </li>
                    </ul>
                </nav>

                <!-- Quản lý tập phim -->
                <nav aria-label="breadcrumb">
                    <h2 class="fw-bold text-center h2" style="color: rebeccapurple;">QUẢN LÝ TẬP PHIM</h2>
                    <div class="search-ajax">
                        <a href="create_tap.php" class="btn mt-2 mb-3">Thêm mới</a>
                        <div class="search-box">
                            <form action="" method="post">
                                <input class="search-input-tap" placeholder="Search Here..." type="search" id="search">
                                <button class="search-submit" value=""><span class="fa fa-search"></span></button>
                            </form>
                        </div>
                   </div>
                </nav>
                
                <table class="table">
                    <thead>
                        <tr>
                            <th width="170px">Tên phim</th>
                            <th width="140px">Tên tập</th>
                            <th width="500px">Url</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="tbody-tap">
                        <?php
                            if(!isset($_GET['page1'])){
                                $_GET['page1'] = 1;
                            }
                            $rowPerPage = 6;
                            // vị trí của mẩu tin đầu tiên trên mỗi trang
                            $offset = ($_GET['page1'] - 1) * $rowPerPage;

                            $query = "SELECT * FROM tap_phim, phim WHERE tap_phim.ID_Phim = phim.ID_Phim LIMIT $offset, $rowPerPage";
                            $result = $conn->query($query);
                            if(!$result) echo "Câu truy vấn bị lỗi";

                            if($result->num_rows != 0) {
                                while($row = $result->fetch_array()){ ?>
                                    <tr>
                                        <td>
                                            <?= $row['Ten_phim'] ?>
                                        </td>

                                        <td>
                                            <?= $row['Ten_tap'] ?>
                                        </td>

                                        <td class="text-start">
                                            <?= $row['Url_video'] ?>
                                        </td>

                                        <td>
                                            <a href="#" class="btn btn-table-details">Xem chi tiết</a>
                                            <a href="edit_tap.php?id_tap=<?= $row['ID_Tap'] ?>" class="btn btn-table-edit">Sửa</a>
                                            <a href="movie.php?delete_tap=<?= $row['ID_Tap'] ?>" onclick="return confirm('Bạn có chắc muốn xóa?');" class="btn btn-table-delete">Xóa</a>
                                        </td>
                                    </tr>
                                <?php }
                            }
                            else{ ?>
                                <tr>
                                    <td colspan="6" class="text-center" style="font-size:18px; height:160px; color: rebeccapurple;">
                                        <img src="../images/thongbao.png" class="img-search">
                                    </td>
                                </tr>
                            <?php }
                        ?>
                    </tbody>
                </table>
                
                <?php
                    $re = $conn->query("SELECT * FROM tap_phim, phim WHERE tap_phim.ID_Phim = phim.ID_Phim");
                    $numRows = mysqli_num_rows($re);
                    $maxPage = ceil($numRows/$rowPerPage);
                ?>

                <nav class="d-flex justify-content-end p-4" aria-label="Page navigation example">
                    <ul class="pagination">
                    <li class="page-item">
                            <?php
                                if($_GET["page1"] > 1){ ?>
                                    <a class="page-link" href="movie.php?&page=<?= $_GET['page1'] - 1 ?>" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                <?php }
                            ?>
                        </li>
                        <?php 
                            for ($i=1 ; $i<=$maxPage ; $i++)
                            {
                                if($i == $_GET['page1']){ ?>
                                    <li class="page-item">
                                        <a class="page-link active" href="movie.php?&page1=<?= $i ?>"><?= $i ?></a> <!-- trang hiện tại sẽ được style -->
                                    </li>
                                <?php }
                                else { ?>
                                    <li class="page-item">
                                        <a class="page-link" href="movie.php?&page1=<?= $i ?>"><?= $i ?></a> 
                                    </li>
                                <?php }
                            }
                        ?>
                        <li class="page-item">
                            <?php
                                if($_GET["page1"] < $maxPage){ ?>
                                    <a class="page-link" href="movie.php?&page1=<?= $_GET['page1'] + 1 ?>" aria-label="Previous">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                <?php }
                                
                                $conn->close();
                            ?>
                        </li>
                    </ul>
                </nav>
                <!-- End quản lý tập phim -->
            </div>
            <!-- //content -->
        </div>
        <!-- main content end-->

    </section>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.search-input').keyup(function(){
                var key = $(this).val();
                $.post('Search/search_movie.php', {key_phim: key}, function(data){
                    $('.tbody').html(data);
                });
            });
            $('.search-input-tap').keyup(function(){
                var key = $(this).val();
                $.post('Search/search_movie.php', {key_tap: key}, function(data){
                    $('.tbody-tap').html(data);
                });
            });
        });
    </script>

    <?php require_once("./Layout_page/Layout_footer.php"); ?>

</body>

</html>
  