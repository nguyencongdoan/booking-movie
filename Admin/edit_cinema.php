<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Chỉnh sửa thông tin rạp phim </title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-starter.css">

    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">
    <!-- BOX ICONS -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
</head>

<body class="sidebar-menu-collapsed">
    <section>
        <?php 
            include '../Model/config.php';
            include './Action_admin/action_cinema.php';
            require_once("./Layout_page/Layout_header.php"); 
        ?>

        <!-- main content start -->
        <div class="main-content">
            <!-- content -->
            <div class="container-fluid content-top-gap">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="cinema_admin.php">Quản lý rạp phim & phòng chiếu </a></li>
                        <li class="breadcrumb-item active" aria-current="page">Chỉnh sửa thông tin rạp phim </li>
                    </ol>
                    <h2 class="fw-bold text-center h2" style="color: rebeccapurple;">CHỈNH SỬA THÔNG TIN RẠP PHIM</h2>
                    <a href="cinema_admin.php" class="btn mt-2 mb-3">Quay về trang trước</a>
                </nav>

                <?php if(isset($thongbao)){ ?>
                    <div class="col-md-10 alert alert-<?= $type; ?> mb-5" align="center">
                        <?php echo $thongbao; ?>
                    </div>
                <?php } unset($thongbao) ?>

                <div class="container">
                    <div class="row">
                        <form action="" method="post">
                            <div class="row">
                                <input type="hidden" name="id_rap" value="<?= $id_rap; ?>" />
                                <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                    <span class="mt-2">Tên Rạp</span>
                                    <input type="text" class="form-control col-md-4 mx-2" name="tenrap" value="<?php if(isset($tenrap)) echo $tenrap; ?>" required>
                                </div>

                                <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                    <span class="mt-2">Địa chỉ</span>
                                    <textarea name="diachi" cols="50" rows="10" class="form-control col-md-4 mx-2"><?php if(isset($diachi)) echo $diachi; ?></textarea>
                                </div>

                                <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                    <span class="mt-2">Email</span>
                                    <input type="text" class="form-control col-md-4 mx-2" name="email" value="<?php if(isset($email)) echo $email; ?>" required>
                                </div>

                                <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                    <span class="mt-2">Số điện thoại</span>
                                    <input type="text" class="form-control col-md-4 mx-2" name="sdt" value="<?php if(isset($sdt)) echo $sdt; ?>" required>
                                </div>

                                <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                    <span class="mt-2">Url map</span>
                                    <input type="text" class="form-control col-md-4 mx-2" name="url_map" value="<?php if(isset($url_map)) echo $url_map; ?>" required>
                                </div>

                                <div class="col-md-12 col-sm-12 mt-3"  style="margin-left: 435px;">
                                    <input type="submit" class="btn" name="edit" value="Cập nhật">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- //content -->
        </div>
        <!-- main content end-->

    </section>
    
    <?php require_once("./Layout_page/Layout_footer.php"); ?>

</body>

</html>
  