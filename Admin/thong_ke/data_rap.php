<?php
    header('Content-Type: application/json');
    require_once("database.php");

    $data = array();
    $query = "SELECT Ten_rap, SUM(Tong_tien) as Total FROM chi_tiet_hd, rap_phim WHERE chi_tiet_hd.ID_Rap = rap_phim.ID_Rap GROUP BY Ten_rap";
    $result = $conn->query($query);
    if(!$result) echo "Câu truy vấn bị lỗi";

    if($result->num_rows != 0){
        $result->fetch_array();
    }

    foreach($result as $row)
    {
        $data[] = $row;
    }

    echo json_encode($data);
?>