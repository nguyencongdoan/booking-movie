<?php
    header('Content-Type: application/json');
    require_once("database.php");

    $data = array();
    $query = "SELECT phim.Ten_phim, SUM(Tong_tien) as Total FROM chi_tiet_hd, phim WHERE chi_tiet_hd.ID_Phim = phim.ID_phim GROUP BY Ten_phim";
    $result = $conn->query($query);
    if(!$result) echo "Câu truy vấn bị lỗi";

    if($result->num_rows != 0){
        $result->fetch_array();
    }

    foreach($result as $row)
    {
        $data[] = $row;
    }

    echo json_encode($data);
?>