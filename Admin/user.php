<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Quản lý khách hàng</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-starter.css">

    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">

    <!-- AJAX -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>
</head>

<body class="sidebar-menu-collapsed">
    <section>
        <?php 
            include '../Model/config.php';
            include './Action_admin/action_user.php';
            require_once("./Layout_page/Layout_header.php"); 

            if(isset($_GET['delete'])){
                $id_kh = $_GET['delete'];
        
                $sql = "DELETE FROM khach_hang WHERE ID_KH = '$id_kh' ";
                if($conn->query($sql) === TRUE){
                    $_SESSION['thongbao'] = "Xóa thành công!";
                    $_SESSION['type'] = 'success';
                }
                else{
                    $_SESSION['thongbao'] = "Xóa thất bại!";
                    $_SESSION['type'] = 'danger';
                }
            }
        ?>

        <!-- main content start -->
        <div class="main-content">
            <!-- content -->
            <div class="container-fluid content-top-gap">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Quản lý khách hàng</li>
                    </ol>
                    <h2 class="fw-bold text-center h2" style="color: rebeccapurple;">QUẢN LÝ KHÁCH HÀNG</h2>
                    <div class="search-ajax mt-5">
                        <a href="create_user.php" class="btn mt-2 mb-3">Thêm mới</a>
                        <div class="search-box">
                            <form action="" method="post">
                                <input class="search-input" placeholder="Search Here..." type="search" id="search">
                                <button class="search-submit" type="submit" value=""><span class="fa fa-search"></span></button>
                            </form>
                        </div>
                    </div>
                </nav>

                <?php
                    if(isset($_GET['them']) || isset($_GET['sua'])){
                        if(isset($_GET['them']) && $_GET['them'] == 'success'){
                            $_SESSION['thongbao'] = "Thêm mới thành công!";
                        }
                        if(isset($_GET['sua']) && $_GET['sua'] == 'success'){
                            $_SESSION['thongbao'] = "Sửa thông tin thành công";
                        }
                        $_SESSION['type'] = 'success';
                    }
                ?>

                <?php if(isset($_SESSION['thongbao'])){ ?>
                    <div class="col-md-10 alert alert-<?= $_SESSION['type']; ?> mb-5" align="center">
                        <?php echo $_SESSION['thongbao']; ?>
                    </div>
                <?php } unset($_SESSION['thongbao']);?>

                <table class="table">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Họ tên</th>
                            <th class="text-start">Địa chỉ</th>
                            <th>Số điện thoại</th>
                            <th>Giới tính</th>
                            <th>Email</th>
                            <th>Password</th>
                            <th>Quyền hạn</th>
                            <th width="255px"></th>
                        </tr>
                    </thead>
                    <tbody class="tbody">
                        <?php
                            if(!isset($_GET['page'])){
                                $_GET['page'] = 1;
                            }
                            $rowPerPage = 6;
                            // vị trí của mẩu tin đầu tiên trên mỗi trang
                            $offset = ($_GET['page'] - 1) * $rowPerPage;

                            $query = "SELECT * FROM khach_hang, quyen_han WHERE khach_hang.ID_Quyen = quyen_han.ID_Quyen LIMIT $offset, $rowPerPage";
                            $result = $conn->query($query);
                            if(!$result) echo "Câu truy vấn bị lỗi";
                            $i = 1;
                            if($result->num_rows != 0) {
                                while($row = $result->fetch_array()){ 
                                        if($row['Gioi_tinh'] == 1) $gioitinh = 'Nam';
                                        else $gioitinh = 'Nữ';
                                    ?>
                                    <tr>
                                        <td><?= $i ?></td>

                                        <td class="text-start">
                                            <?= $row['Ho_ten'] ?>
                                        </td>

                                        <td class="text-start" width="290px">
                                            <?= $row['Dia_chi'] ?>
                                        </td>

                                        <td>
                                            <?= $row['SDT'] ?>
                                        </td>

                                        <td>
                                            <?= $gioitinh ?>
                                        </td>

                                        <td class="text-start">
                                            <?= $row['Email'] ?>
                                        </td>

                                        <td>********</td>

                                        <td>
                                            <?= $row['Ten_quyen_han'] ?>
                                        </td>

                                        <td>
                                            <a href="#" class="btn btn-table-details">Xem chi tiết</a>
                                            <a href="edit_user.php?id_kh=<?= $row['ID_KH'] ?>" class="btn btn-table-edit">Sửa</a>
                                            <a href="user.php?delete=<?= $row['ID_KH'] ?>" onclick="return confirm('Bạn có chắc muốn xóa?');" class="btn btn-table-delete">Xóa</a>
                                        </td>
                                    </tr>
                                <?php $i++;}
                            }
                            else{ ?>
                                <tr>
                                    <td colspan="9" class="text-center" style="font-size:18px; height:160px; color: rebeccapurple;">
                                        <img src="../images/thongbao.png" class="img-search">
                                    </td>
                                </tr>
                            <?php }
                        ?>
                    </tbody>
                </table>
                
                <?php
                    $re = $conn->query("SELECT * FROM khach_hang, quyen_han WHERE khach_hang.ID_Quyen = quyen_han.ID_Quyen");
                    $numRows = mysqli_num_rows($re);
                    $maxPage = ceil($numRows/$rowPerPage);
                ?>

                <nav class="d-flex justify-content-end p-4" aria-label="Page navigation example">
                    <ul class="pagination">
                    <li class="page-item">
                            <?php
                                if($_GET["page"] > 1){ ?>
                                    <a class="page-link" href="user.php?&page=<?= $_GET['page'] - 1 ?>" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                <?php }
                            ?>
                        </li>
                        <?php 
                            for ($i=1 ; $i<=$maxPage ; $i++)
                            {
                                if($i == $_GET['page']){ ?>
                                    <li class="page-item">
                                        <a class="page-link active" href="user.php?&page=<?= $i ?>"><?= $i ?></a> <!-- trang hiện tại sẽ được style -->
                                    </li>
                                <?php }
                                else { ?>
                                    <li class="page-item">
                                        <a class="page-link" href="user.php?&page=<?= $i ?>"><?= $i ?></a> 
                                    </li>
                                <?php }
                            }
                        ?>
                        <li class="page-item">
                            <?php
                                if($_GET["page"] < $maxPage){ ?>
                                    <a class="page-link" href="user.php?&page=<?= $_GET['page'] + 1 ?>" aria-label="Previous">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                <?php }
                                $conn->close();
                            ?>
                        </li>
                    </ul>
                </nav>

            </div>
            <!-- //content -->
        </div>
        <!-- main content end-->

    </section>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('.search-input').keyup(function(){
                var key = $(this).val();
                $.post('Search/search_user.php', {key_user: key}, function(data){
                    $('.tbody').html(data);
                });
            });
        });
    </script>

    <?php require_once("./Layout_page/Layout_footer.php"); ?>

</body>

</html>
  