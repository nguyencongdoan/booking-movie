<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Quản lý hoá đơn</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-starter.css">

    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">
</head>

<body class="sidebar-menu-collapsed">
    <section>
        <?php 
            include './thong_ke/database.php';
            require_once("./Layout_page/Layout_header.php"); 

            if(isset($_GET['xem_ct'])){
                $id_hd = $_GET['xem_ct'];
                $query = "SELECT * FROM hoa_don, chi_tiet_hd, nhan_vien, khach_hang, rap_phim, phong_chieu, suat_chieu, phim 
                WHERE hoa_don.ID_HD = chi_tiet_hd.ID_HD AND hoa_don.ID_NV = nhan_vien.ID_NV AND hoa_don.ID_KH = khach_hang.ID_KH
                AND chi_tiet_hd.ID_SC = suat_chieu.ID_SC AND chi_tiet_hd.ID_Phim = phim.ID_phim AND chi_tiet_hd.ID_Rap = rap_phim.ID_Rap
                AND chi_tiet_hd.ID_Phong = Phong_chieu.ID_Phong AND hoa_don.ID_HD = '$id_hd'";
                $result = $conn->query($query);
                if(!$result) echo "Câu truy vấn bị lỗi";
        
                $row = $result->fetch_assoc();
                $tenrap = $row['Ten_rap'];
                $tenphim = $row['Ten_phim'];
                $tenphong = $row['Ten_phong'];
                $tensc = $row['Gio_BD'] .' ~ '.$row['Gio_KT'].' '.$row['Ngay_chieu'];
                $soluong = $row['So_luong'];
                $tinhtrang = $row['Tinh_trang'];
                $phuong_thuc_tt = $row['Phuong_thuc_tt'];
                $tinhtrang = $row['Tinh_trang'];
                $total = $row['Tong_tien'];
                $hinh = $row['Hinh'];
                $ds = $row['List_ghe'];
                $ngaydat = $row['Ngay_dat'];
                $khachhang = $row['Ho_ten'];
                $nhanvien = $row['HoTen'];
        
                if($tinhtrang == 0) $tinhtrang = "Chưa xem";
                else $tinhtrang = "Đã xem";
            }
        ?>

        <!-- main content start -->
        <div class="main-content">
            <!-- content -->
            <div class="container-fluid content-top-gap">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="hoa_don.php">Quản lý hóa đơn</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Xem chi tiết</li>
                    </ol>
                    <h2 class="fw-bold text-center h2" style="color: rebeccapurple;">XEM CHI TIẾT</h2>
                    <a href="hoa_don.php" class="btn mt-2 mb-3">Quay về trang trước</a>
                </nav>

                <div class="container mt-5">
                    <div class="row">
                        <div class="col-md-3">
                            <img src="../images/<?= $hinh ?>" style="border-radius: 12px;">
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <h5 class="h5-xemct col-md-6 col-sm-6">Mã hóa đơn: <span>HD<?= $id_hd ?> </span></h5> 
                                <h5 class="h5-xemct col-md-6 col-sm-6">Tên phim: <span><?= $tenphim ?></span></h5> 
                                <h5 class="h5-xemct col-md-6 col-sm-6">Rạp phim: <span><?= $tenrap ?></span></h5> 
                                <h5 class="h5-xemct col-md-6 col-sm-6">Phòng chiếu: <span><?= $tenphong ?></span></h5> 
                                <h5 class="h5-xemct col-md-6 col-sm-6">Suất chiếu: <span><?= $tensc ?></span></h5> 
                                <h5 class="h5-xemct col-md-6 col-sm-6">Vị trí ngồi: <span><?= $ds ?></span></h5> 
                                <h5 class="h5-xemct col-md-6 col-sm-6">Số lượng: <span><?= $soluong ?></span></h5> 
                                <h5 class="h5-xemct col-md-6 col-sm-6">Tổng tiền: <span><?= $total ?> VND</span></h5> 
                                <h5 class="h5-xemct col-md-6 col-sm-6">Phương thức thanh toán: <span><?= $phuong_thuc_tt ?></span></h5> 
                                <h5 class="h5-xemct col-md-6 col-sm-6">Ngày đặt: <span><?= $ngaydat ?></span></h5> 
                                <h5 class="h5-xemct col-md-6 col-sm-6">Tình trạng: <span><?= $tinhtrang ?></span></h5> 
                                <h5 class="h5-xemct col-md-6 col-sm-6">Nhân viên: <span><?= $nhanvien ?></span></h5> 
                                <h5 class="h5-xemct col-md-6 col-sm-6">Khách hàng: <span><?= $khachhang ?></span></h5> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //content -->
        </div>
        <!-- main content end-->

    </section>
    
    <?php require_once("./Layout_page/Layout_footer.php"); ?>

</body>

</html>
  