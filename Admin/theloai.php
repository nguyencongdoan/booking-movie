<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Quản lý thể loại & quyền hạn</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-starter.css">

    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">

    <!-- AJAX -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>
    
</head>

<body class="sidebar-menu-collapsed">
    <section>
        <?php 
            include '../Model/config.php';
            require_once("./Layout_page/Layout_header.php"); 

            $id_tl = "";
            $ten_tl = "";

            if(isset($_POST['create_tl'])){
                $ten_tl = $_POST['ten_tl'];
                $query = "INSERT INTO the_loai(Ten_TL) VALUES ('$ten_tl')";
                if($conn->query($query) === TRUE){
                    $_SESSION['thongbao'] = "Thêm mới thành công!";
                    $_SESSION['type'] = "success";
                }
                else{
                    $_SESSION['thongbao'] = "Thêm mới thất bại!";
                    $_SESSION['type'] = "danger";
                }
            }

            if(isset($_GET['id_tl'])){
                $id_tl = $_GET['id_tl'];
                $query = "SELECT * FROM the_loai WHERE ID_TL = '$id_tl'";
                $result = $conn->query($query);
                if(!$result) echo "Câu truy vấn bị lỗi";
                $row = $result->fetch_assoc();
                $ten_tl = $row['Ten_TL'];
            }

            if(isset($_POST['edit_tl'])){
                $ten_tl = $_POST['ten_tl'];
                $query = "UPDATE the_loai SET Ten_TL = '$ten_tl' WHERE ID_TL = '$id_tl'";
                if($conn->query($query) === TRUE){
                    $_SESSION['thongbao'] = "Cập nhật thông tin thành công!";
                    $_SESSION['type'] = "success";
                }
                else{
                    $_SESSION['thongbao'] = "Cập nhật thông tin thất bại!";
                    $_SESSION['type'] = "danger";
                }
            }

            if(isset($_GET['delete_tl'])){
                $id_tl = $_GET['delete_tl'];
                $query = "DELETE FROM the_loai WHERE ID_TL = '$id_tl'";
                if($conn->query($query) === TRUE){
                    $_SESSION['thongbao'] = "Xóa thành công!";
                    $_SESSION['type'] = "success";
                }
                else{
                    $_SESSION['thongbao'] = "Xóa thất bại!";
                    $_SESSION['type'] = "danger";
                }
            }
        ?>

         <!-- xử lý thêm / sửa / xóa quyền hạn -->
         <?php
            $id_quyen = "";
            $ten_quyen = "";

            if(isset($_POST['create_quyen'])){
                $ten_quyen = $_POST['ten_quyen'];
                $query = "INSERT INTO quyen_han (Ten_quyen_han) VALUES ('$ten_quyen')";
                if($conn->query($query) === TRUE){
                    $_SESSION['thongbao'] = "Thêm mới thành công!";
                    $_SESSION['type'] = "success";
                }
                else{
                    $_SESSION['thongbao'] = "Thêm mới thất bại!";
                    $_SESSION['type'] = "danger";
                }
            }

            if(isset($_GET['id_quyen'])){
                $id_quyen = $_GET['id_quyen'];
                $query = "SELECT * FROM quyen_han WHERE ID_Quyen = '$id_quyen'";
                $result = $conn->query($query);
                if(!$result) echo "Câu truy vấn bị lỗi";
                $row = $result->fetch_assoc();
                $ten_quyen = $row['Ten_quyen_han'];
            }

            if(isset($_POST['edit_quyen'])){
                $ten_quyen = $_POST['ten_quyen'];
                $query = "UPDATE quyen_han SET Ten_quyen_han = '$ten_quyen' WHERE ID_Quyen = '$id_quyen'";
                if($conn->query($query) === TRUE){
                    $_SESSION['thongbao'] = "Cập nhật thông tin thành công!";
                    $_SESSION['type'] = "success";
                }
                else{
                    $_SESSION['thongbao'] = "Cập nhật thông tin thất bại!";
                    $_SESSION['type'] = "danger";
                }
            }

            if(isset($_GET['delete_quyen'])){
                $id_quyen = $_GET['delete_quyen'];
                $query = "DELETE FROM quyen_han WHERE ID_Quyen = '$id_quyen'";
                if($conn->query($query) === TRUE){
                    $_SESSION['thongbao'] = "Xóa thành công!";
                    $_SESSION['type'] = "success";
                }
                else{
                    $_SESSION['thongbao'] = "Xóa thất bại!";
                    $_SESSION['type'] = "danger";
                }
            }
        ?>
        <!-- xử lý thêm / sửa / xóa quyền hạn  -->

        <!-- main content start -->
        <div class="main-content">
            <!-- content -->
            <div class="container-fluid content-top-gap">
                <div class="row">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb my-breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Quản lý thể loại & quyền hạn</li>
                        </ol>
                    </nav>

                    <div class="container">
                        <div class="row">
                            <form action="" method="post">
                                <div class="row">
                                   <?php
                                        if(isset($_GET['them_tl']) || isset($_GET['id_tl'])){ ?>
                                            <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                                <input type="hidden" name="id_tl" value="<?php if(isset($id_tl)) echo $id_tl; ?>">
                                                <span class="mt-2">Tên thể loại</span>
                                                <input type="text" class="form-control col-md-4 mx-2" name="ten_tl" value="<?php if(isset($ten_tl)) echo $ten_tl; ?>" required>
                                                <?php
                                                    if(isset($_GET['id_tl'])) echo '<input type="submit" class="btn" name="edit_tl" value="Cập nhật">';
                                                    else echo '<input type="submit" class="btn" name="create_tl" value="Thêm mới">'
                                                ?>
                                            </div>
                                        <?php }
                                        else if(isset($_GET['them_quyen']) || isset($_GET['id_quyen'])){ ?>
                                            <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                                <input type="hidden" name="id_quyen" value="<?php if(isset($id_quyen)) echo $id_quyen; ?>">
                                                <span class="mt-2">Tên quyền hạn</span>
                                                <input type="text" class="form-control col-md-4 mx-2" name="ten_quyen" value="<?php if(isset($ten_quyen)) echo $ten_quyen; ?>" required>
                                                <?php
                                                    if(isset($_GET['id_quyen'])) echo '<input type="submit" class="btn" name="edit_quyen" value="Cập nhật">';
                                                    else echo '<input type="submit" class="btn" name="create_quyen" value="Thêm mới">'
                                                ?>
                                            </div>
                                        <?php }
                                   ?>
                                </div>
                            </form>
                        </div>
                    </div>

                    <?php if(isset($_SESSION['thongbao'])){ ?>
                        <div class="col-md-10 alert alert-<?= $_SESSION['type']; ?> mt-5" align="center">
                            <?php echo $_SESSION['thongbao']; ?>
                        </div>
                    <?php } unset($_SESSION['thongbao']);?>

                    <div class="col-md-6 mt-4">
                        <nav aria-label="breadcrumb">
                            <h2 class="fw-bold text-center h2" style="color: rebeccapurple;">QUẢN LÝ THỂ LOẠI</h2>
                            <div class="search-ajax mt-5">
                                <a href="theloai.php?them_tl" class="btn mt-2 mb-3">Thêm mới</a>
                                <div class="search-box">
                                    <form action="" method="post">
                                        <input class="search-input" placeholder="Search Here..." type="search" id="search">
                                        <button class="search-submit" type="submit" value=""><span class="fa fa-search"></span></button>
                                    </form>
                                </div>
                            </div>
                        </nav>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID thể loại</th>
                                    <th class="text-start">Tên thể loại</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="tbody">
                                <?php
                                    if(!isset($_GET['page'])){
                                        $_GET['page'] = 1;
                                    }
                                    $rowPerPage = 6;
                                    // vị trí của mẩu tin đầu tiên trên mỗi trang
                                    $offset = ($_GET['page'] - 1) * $rowPerPage;

                                    $query = "SELECT * FROM the_loai LIMIT $offset, $rowPerPage";
                                    $result = $conn->query($query);
                                    if(!$result) echo "Câu truy vấn bị lỗi";

                                    if($result->num_rows != 0) {
                                        while($row = $result->fetch_array()){ ?>
                                            <tr>

                                                <td class="text-center">
                                                    <?= $row['ID_TL'] ?>
                                                </td>

                                                <td class="text-start">
                                                    <?= $row['Ten_TL'] ?>
                                                </td>

                                                <td>
                                                    <a href="theloai.php?id_tl=<?= $row['ID_TL']; ?>" class="btn btn-table-edit">Sửa</a>
                                                    <a href="theloai.php?delete_tl=<?= $row['ID_TL']; ?>" class="btn btn-table-delete" onclick="return confirm('Bạn có chắc muốn xóa?');">Xóa</a>
                                                </td>
                                            </tr>
                                        <?php }
                                    }
                                    else{ ?>
                                        <tr>
                                            <td colspan="6" class="text-center" style="font-size:18px; height:160px; color: rebeccapurple;"><img src="../images/thongbao.png" alt=""></td>
                                        </tr>
                                    <?php }
                                ?>
                            </tbody>
                        </table>
                        
                        <?php
                            $re = $conn->query("SELECT * FROM the_loai");
                            $numRows = mysqli_num_rows($re);
                            $maxPage = ceil($numRows/$rowPerPage);
                        ?>

                        <nav class="d-flex justify-content-end p-4" aria-label="Page navigation example">
                            <ul class="pagination">
                            <li class="page-item">
                                    <?php
                                        if($_GET["page"] > 1){ ?>
                                            <a class="page-link" href="theloai.php?&page=<?= $_GET['page'] - 1 ?>" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                            </a>
                                        <?php }
                                    ?>
                                </li>
                                <?php 
                                    for ($i=1 ; $i<=$maxPage ; $i++)
                                    {
                                        if($i == $_GET['page']){ ?>
                                            <li class="page-item">
                                                <a class="page-link active" href="theloai.php?&page=<?= $i ?>"><?= $i ?></a> <!-- trang hiện tại sẽ được style -->
                                            </li>
                                        <?php }
                                        else { ?>
                                            <li class="page-item">
                                                <a class="page-link" href="theloai.php?&page=<?= $i ?>"><?= $i ?></a> 
                                            </li>
                                        <?php }
                                    }
                                ?>
                                <li class="page-item">
                                    <?php
                                        if($_GET["page"] < $maxPage){ ?>
                                            <a class="page-link" href="theloai.php?&page=<?= $_GET['page'] + 1 ?>" aria-label="Previous">
                                                <span aria-hidden="true">&raquo;</span>
                                            </a>
                                        <?php }
                                    ?>
                                </li>
                            </ul>
                        </nav>
                    </div>

                    <div class="col-md-6 mt-4">
                        <!-- Quản lý quyền hạn -->    
                        <nav aria-label="breadcrumb">
                            <h2 class="fw-bold text-center h2" style="color: rebeccapurple;">QUẢN LÝ QUYỀN HẠN</h2>
                            <div class="search-ajax mt-5">
                                <a href="theloai.php?them_quyen" class="btn mt-2 mb-3">Thêm mới</a>
                                <div class="search-box">
                                    <form action="" method="post">
                                        <input class="search-input-qh" placeholder="Search Here..." type="search" id="search">
                                        <button class="search-submit" type="submit" value=""><span class="fa fa-search"></span></button>
                                    </form>
                                </div>
                            </div>
                        </nav>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID quyền</th>
                                    <th class="text-start">Tên quyền hạn</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="tbody-qh">
                                <?php
                                    if(!isset($_GET['page1'])){
                                        $_GET['page1'] = 1;
                                    }
                                    $rowPerPage = 6;
                                    // vị trí của mẩu tin đầu tiên trên mỗi trang
                                    $offset = ($_GET['page1'] - 1) * $rowPerPage;

                                    $query = "SELECT * FROM quyen_han LIMIT $offset, $rowPerPage";
                                    $result = $conn->query($query);
                                    if(!$result) echo "Câu truy vấn bị lỗi";

                                    if($result->num_rows != 0) {
                                        while($row = $result->fetch_array()){ ?>
                                            <tr>

                                                <td class="text-center">
                                                    <?= $row['ID_Quyen'] ?>
                                                </td>

                                                <td class="text-start">
                                                    <?= $row['Ten_quyen_han'] ?>
                                                </td>

                                                <td>
                                                <a href="theloai.php?id_quyen=<?= $row['ID_Quyen']; ?>" class="btn btn-table-edit">Sửa</a>
                                                    <a href="theloai.php?delete_quyen=<?= $row['ID_Quyen']; ?>" class="btn btn-table-delete" onclick="return confirm('Bạn có chắc muốn xóa?');">Xóa</a>
                                                </td>
                                            </tr>
                                        <?php }
                                    }
                                    else{ ?>
                                        <tr>
                                            <td colspan="6" class="text-center" style="font-size:18px; height:160px; color: rebeccapurple;"><img src="../images/thongbao.png" alt=""></td>
                                        </tr>
                                    <?php }
                                ?>
                            </tbody>
                        </table>
                        
                        <?php
                            $re = $conn->query("SELECT * FROM quyen_han");
                            $numRows = mysqli_num_rows($re);
                            $maxPage = ceil($numRows/$rowPerPage);
                        ?>

                        <nav class="d-flex justify-content-end p-4" aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item">
                                    <?php
                                        if($_GET["page1"] > 1){ ?>
                                            <a class="page-link" href="theloai.php?&page1=<?= $_GET['page1'] - 1 ?>" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                            </a>
                                        <?php }
                                    ?>
                                </li>
                                <?php 
                                    for ($i=1 ; $i<=$maxPage ; $i++)
                                    {
                                        if($i == $_GET['page1']){ ?>
                                            <li class="page-item">
                                                <a class="page-link active" href="theloai.php?&page1=<?= $i ?>"><?= $i ?></a> <!-- trang hiện tại sẽ được style -->
                                            </li>
                                        <?php }
                                        else { ?>
                                            <li class="page-item">
                                                <a class="page-link" href="theloai.php?&page1=<?= $i ?>"><?= $i ?></a> 
                                            </li>
                                        <?php }
                                    }
                                ?>
                                <li class="page-item">
                                    <?php
                                        if($_GET["page1"] < $maxPage){ ?>
                                            <a class="page-link" href="theloai.php?&page1=<?= $_GET['page1'] + 1 ?>" aria-label="Previous">
                                                <span aria-hidden="true">&raquo;</span>
                                            </a>
                                        <?php }
                                        
                                        $conn->close();
                                    ?>
                                </li>
                            </ul>
                        </nav>
                        <!-- End quản lý phòng chiếu -->
                    </div>
                </div>
            </div>
            <!-- //content -->
        </div>
        <!-- main content end-->

    </section>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.search-input').keyup(function(){
                var key = $(this).val();
                $.post('Search/search_theloai.php', {key_theloai: key}, function(data){
                    $('.tbody').html(data);
                });
                // alert(key);
            });
            $('.search-input-qh').keyup(function(){
                var key = $(this).val();
                $.post('Search/search_theloai.php', {key_qh: key}, function(data){
                    $('.tbody-qh').html(data);
                });
            });
        });
    </script>

    <?php require_once("./Layout_page/Layout_footer.php"); ?>

</body>

</html>
  