<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Admin</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <!-- BOX ICONS -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>

    <link rel="stylesheet" href="../css/toast.css">
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-starter.css">
    
    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>

</head>

<body class="sidebar-menu-collapsed">
    <section>
        <?php 
            require_once("./Layout_page/Layout_header.php"); 
            
            if(isset($_SESSION['message_login']))
            { ?>
                <div id="toast" style="top: 70px;">
                    <div class="toast toast--<?= $_SESSION['type'] ?>" style="animation: 1s ease 0s 1 normal none running slideInLeft, 1s linear 5s 1 normal forwards running fadeOut; z-index: 1000;">
                        <div class="toast__icon">
                            <i class='bx bxs-check-circle' ></i>
                        </div>
                        <div class="toast__body">
                            <h3 class="toast__title">Thành công!</h3>
                            <p class="toast__msg"><?= $_SESSION['message_login']; ?></p>
                        </div>
                        <div class="toast__close">
                            <i class='bx bx-x'></i>
                        </div>
                    </div>
                </div>
            <?php } unset($_SESSION['message_login']);
        ?>

        <!-- main content start -->
        <div class="main-content">
            <!-- content -->
            <div class="container-fluid content-top-gap">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    </ol>
                </nav>
                <div class="welcome-msg pt-3 pb-4">
                    <h1>Hi <span class="text-primary"><?= $_SESSION['HoTen'] ?></span>, Chào mừng đã quay lại</h1>
                </div>

                <!-- statistics data -->
                <div class="statistics">
                <div class="row">
                    <div class="col-xl-6 pr-xl-2">
                    <div class="row">
                        <div class="col-sm-6 pr-sm-2 statistics-grid">
                        <div class="card card_border border-primary-top p-4">
                            <i class="lnr lnr-users"> </i>
                            <h3 class="text-primary number">29.75 M</h3>
                            <p class="stat-text">Tổng khách hàng</p>
                        </div>
                        </div>
                        <div class="col-sm-6 pl-sm-2 statistics-grid">
                        <div class="card card_border border-primary-top p-4">
                            <i class="lnr lnr-eye"> </i>
                            <h3 class="text-secondary number">51.25 K</h3>
                            <p class="stat-text">Tổng số lượt xem</p>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-xl-6 pl-xl-2">
                    <div class="row">
                        <div class="col-sm-6 pr-sm-2 statistics-grid">
                        <div class="card card_border border-primary-top p-4">
                            <i class="lnr lnr-cloud-download"> </i>
                            <h3 class="text-success number">166.89 M</h3>
                            <p class="stat-text">Số lượt tải xuống</p>
                        </div>
                        </div>
                        <div class="col-sm-6 pl-sm-2 statistics-grid">
                        <div class="card card_border border-primary-top p-4">
                            <i class="lnr lnr-cart"> </i>
                            <h3 class="text-danger number">1,250k</h3>
                            <p class="stat-text">Doanh thu</p>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <!-- //statistics data -->

                <!-- charts -->
                <div class="chart">
                    <div class="row">
                        <div class="col-lg-6 pr-lg-2 chart-grid">
                            <div class="card text-center card_border">
                                <div class="card-header chart-grid__header">
                                    Thống kê doanh thu theo phim
                                </div>
                                <div class="card-body">
                                    <!-- bar chart -->
                                    <div id="container">
                                        <canvas id="barchart"></canvas>
                                    </div>
                                    <!-- //bar chart -->
                                </div>
                                <div class="card-footer text-muted chart-grid__footer">
                                    Cập nhật sau 2 giờ
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 pl-lg-2 chart-grid">
                            <div class="card text-center card_border">
                                <div class="card-header chart-grid__header">
                                    Thống kê doanh thu theo rạp
                                </div>
                                <div class="card-body">
                                    <!-- pie chart -->
                                    <div id="container">
                                        <canvas id="piechart"></canvas>
                                    </div>
                                    <!-- pie chart -->
                                </div>
                                <div class="card-footer text-muted chart-grid__footer">
                                    Cập nhật sau 2 giờ
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-lg-6 pl-lg-2 chart-grid">
                            <div class="card text-center card_border">
                                <div class="card-header chart-grid__header">
                                    Line Chart
                                </div>
                                <div class="card-body">
                                    <div id="container">
                                        <canvas id="linechart"></canvas>
                                    </div>
                                </div>
                                <div class="card-footer text-muted chart-grid__footer">
                                    Cập nhật sau 2 giờ
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
                <!-- //charts -->
            </div>
            <!-- //content -->
        </div>
        <!-- main content end-->
    </section>
    
    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/jquery-1.10.2.min.js"></script>

    <!-- chart js -->
    <script src="assets/js/Chart.min.js"></script>
    <?php require_once("./Layout_page/Layout_footer.php"); ?>
    <script src="../toast/toast.js"></script>

    <script>
        $(document).ready(function(){
            $("#toast").click(function(){
                $(this).hide();
            });
        });
    </script>

</body>

</html>
  