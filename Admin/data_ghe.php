<?php
    include '../Model/config.php';

    $id_phim = $_POST['data_id_phim'];
    $id_sc = $_POST['data_id_sc'];
    $id_rap = $_POST['data_id_rap'];
    $ds_ghe = array();

    $query = "SELECT * FROM dat_ve, phim, suat_chieu, rap_phim WHERE dat_ve.ID_Phim = phim.ID_Phim AND dat_ve.ID_SC = suat_chieu.ID_SC AND dat_ve.ID_Rap = rap_phim.ID_Rap AND dat_ve.ID_Phim = '$id_phim' AND dat_ve.ID_Rap = '$id_rap' AND dat_ve.ID_SC = '$id_sc'";
    $result = $conn->query($query);
    if(!$result) echo "Câu truy vấn bị lỗi";
    $i = 0;
    if($result->num_rows != 0){
        while($row = $result->fetch_array()){
            $ds_ghe[$i] = $row['List_ghe'];
            $i++;
        }
    }

    $ds = implode(", ", $ds_ghe);
    $list = explode(", ", $ds);
?>
    <script type="text/javascript">
        $(document).ready(function() {
            $("input[name='ds_ghe[]'").change(function() {
                var max = $('.bx-number').val();
                var sl_check = $("input[name='ds_ghe[]']:checked").length;
                var sl_dis = $("input[name='ds_ghe[]']:disabled").length;
                number = sl_check - sl_dis;
                
                if (number > max){
                    $(this).prop("checked", "");
                    alert("Bạn chỉ được chọn tối đa " + max + " ghế");
                }
            });
        });
    </script>
    
    <div class="form-check-vt" style="margin-top: -20px;">
        <h5 class="text-chect-h5 mx-3">A</h5>
        <?php
            for($i = 1; $i < 10; $i++) { ?>
                <input class="form-check-input input-check-vt" name="ds_ghe[]" id="A<?= $i ?>" type="checkbox" value="A<?= $i ?>"
                <?php 
                    foreach($list as $value) if($value == 'A'.$i) echo "checked disabled";
                ?>>
                <label for="A<?= $i ?>" class="mx-2"></label>
            <?php }
        ?>
    </div>

    <div class="form-check-vt mt-5">
        <h5 class="text-chect-h5 mx-3">B</h5>
        <?php
            for($i = 1; $i < 10; $i++) { ?>
                <input class="form-check-input input-check-vt" name="ds_ghe[]" id="B<?= $i ?>" type="checkbox" value="B<?= $i ?>"
                <?php 
                    foreach($list as $value) if($value == 'B'.$i) echo "checked disabled";
                ?>>
                <label for="B<?= $i ?>" class="mx-2"></label>
            <?php }
        ?>
    </div>

    <div class="form-check-vt mt-5">
        <h5 class="text-chect-h5 mx-3">C</h5>
        <?php
            for($i = 1; $i < 10; $i++) { ?>
                <input class="form-check-input input-check-vt" name="ds_ghe[]" id="C<?= $i ?>" type="checkbox" value="C<?= $i ?>"
                <?php 
                    foreach($list as $value) if($value == 'C'.$i) echo "checked disabled";
                ?>>
                <label for="C<?= $i ?>" class="mx-2"></label>
            <?php }
        ?>
    </div>

    <div class="form-check-vt mt-5">
        <h5 class="text-chect-h5 mx-3">D</h5>
        <?php
            for($i = 1; $i < 10; $i++) { ?>
                <input class="form-check-input input-check-vt" name="ds_ghe[]" id="D<?= $i ?>" type="checkbox" value="D<?= $i ?>"
                <?php 
                    foreach($list as $value) if($value == 'D'.$i) echo "checked disabled";
                ?>>
                <label for="D<?= $i ?>" class="mx-2"></label>
            <?php }
        ?>
    </div>