<?php
    include '../thong_ke/database.php';

    if(isset($_POST['key_nhanvien'])){
        $key = $_POST['key_nhanvien'];

        if(!isset($_GET['page'])){
            $_GET['page'] = 1;
        }
        $rowPerPage = 6;
        // vị trí của mẩu tin đầu tiên trên mỗi trang
        $offset = ($_GET['page'] - 1) * $rowPerPage;

        $query = "SELECT * FROM nhan_vien, quyen_han WHERE nhan_vien.ID_Quyen = quyen_han.ID_Quyen AND HoTen LIKE '%$key%' LIMIT $offset, $rowPerPage";
        $result = $conn->query($query);
        if(!$result) echo "Câu truy vấn bị lỗi";
        $i = 1;
        if($result->num_rows != 0) {
            while($row = $result->fetch_array()){ 
                    if($row['Gioi_tinh'] == 1) $gioitinh = 'Nam';
                    else $gioitinh = 'Nữ';
                ?>
                <tr>
                    <td><?= $i ?></td>

                    <td class="text-start">
                        <?= $row['HoTen'] ?>
                    </td>

                    <td class="text-start" width="290px">
                        <?= $row['Dia_chi'] ?>
                    </td>

                    <td>
                        <?= $row['SDT'] ?>
                    </td>

                    <td>
                        <?= $gioitinh ?>
                    </td>

                    <td class="text-start">
                        <?= $row['Email'] ?>
                    </td>

                    <td> ******** </td>

                    <td>
                        <?= $row['Ten_quyen_han'] ?>
                    </td>

                    <td>
                        <a href="#" class="btn btn-table-details">Xem chi tiết</a>
                        <a href="edit_nhanvien.php?id_nv=<?= $row['ID_NV'] ?>" class="btn btn-table-edit">Sửa</a>
                        <a href="nhanvien.php?delete=<?= $row['ID_NV'] ?>" onclick="return confirm('Bạn có chắc muốn xóa?');" class="btn btn-table-delete">Xóa</a>
                    </td>
                </tr>
            <?php $i++;}
        }
        else{ ?>
            <tr>
                <td colspan="9" class="text-center" style="font-size:18px; height:160px; color: rebeccapurple;">
                    <img src="../images/thongbao.png" class="img-search">
                </td>
            </tr>
        <?php }
    }
?>