<?php
    include '../thong_ke/database.php';

    if(isset($_POST['key_phim'])){
        $key = $_POST['key_phim'];

        if(!isset($_GET['page'])){
            $_GET['page'] = 1;
        }
        $rowPerPage = 6;
        // vị trí của mẩu tin đầu tiên trên mỗi trang
        $offset = ($_GET['page'] - 1) * $rowPerPage;

        $query = "SELECT * FROM phim, the_loai WHERE phim.ID_TL = the_loai.ID_TL AND Ten_phim LIKE '%$key%' LIMIT $offset, $rowPerPage";
        $result = $conn->query($query);
        if(!$result) echo "Câu truy vấn bị lỗi";

        if($result->num_rows != 0) {
            while($row = $result->fetch_array()){ ?>
                <tr>
                    <td>
                        <?= $row['Ten_phim'] ?>
                    </td>

                    <td>
                        <?= $row['Thoi_luong'] ?>
                    </td>

                    <td>
                        <?= $row['Luot_xem'] ?>
                    </td>

                    <td class="text-justify text-table">
                        <?= $row['Tom_tat'] ?>
                    </td>

                    <td>
                        <?= $row['Quoc_gia'] ?>
                    </td>

                    <td>
                        <?= $row['Ten_TL'] ?>
                    </td>

                    <td>
                        <a href="Action_admin/action_movie.php" class="btn btn-table-details">Xem chi tiết</a>
                        <a href="edit_movie.php?id_phim=<?= $row['ID_phim']; ?>" class="btn btn-table-edit">Sửa</a>
                        <a href="movie.php?delete=<?= $row['ID_phim']; ?>" class="btn btn-table-delete" onclick="return confirm('Bạn có chắc muốn xóa?');">Xóa</a>
                    </td>
                </tr>
            <?php }
        }
        else{ ?>
            <tr>
                <td colspan="6" class="text-center" style="font-size:18px; height:160px; color: rebeccapurple;">
                    <img src="../images/thongbao.png" class="img-search">
                </td>
            </tr>
        <?php }
    }

    if(isset($_POST['key_tap'])){
        $key_tap = $_POST['key_tap'];

        if(!isset($_GET['page1'])){
            $_GET['page1'] = 1;
        }
        $rowPerPage = 6;
        // vị trí của mẩu tin đầu tiên trên mỗi trang
        $offset = ($_GET['page1'] - 1) * $rowPerPage;

        $query = "SELECT * FROM tap_phim, phim WHERE tap_phim.ID_Phim = phim.ID_Phim AND Ten_tap LIKE '%$key_tap%' LIMIT $offset, $rowPerPage";
        $result = $conn->query($query);
        if(!$result) echo "Câu truy vấn bị lỗi";

        if($result->num_rows != 0) {
            while($row = $result->fetch_array()){ ?>
                <tr>
                    <td>
                        <?= $row['Ten_phim'] ?>
                    </td>

                    <td>
                        <?= $row['Ten_tap'] ?>
                    </td>

                    <td class="text-start">
                        <?= $row['Url_video'] ?>
                    </td>

                    <td>
                        <a href="#" class="btn btn-table-details">Xem chi tiết</a>
                        <a href="edit_tap.php?id_tap=<?= $row['ID_Tap'] ?>" class="btn btn-table-edit">Sửa</a>
                        <a href="movie.php?delete_tap=<?= $row['ID_Tap'] ?>" onclick="return confirm('Bạn có chắc muốn xóa?');" class="btn btn-table-delete">Xóa</a>
                    </td>
                </tr>
            <?php }
        }
        else{ ?>
            <tr>
                <td colspan="6" class="text-center" style="font-size:18px; height:160px; color: rebeccapurple;">
                    <img src="../images/thongbao.png" class="img-search">
                </td>
            </tr>
        <?php }
    }
?>