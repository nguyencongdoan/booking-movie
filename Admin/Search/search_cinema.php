<?php
    include '../thong_ke/database.php';

    if(isset($_POST['key_rap'])){
        $key = $_POST['key_rap'];

        if(!isset($_GET['page'])){
            $_GET['page'] = 1;
        }
        $rowPerPage = 6;
        // vị trí của mẩu tin đầu tiên trên mỗi trang
        $offset = ($_GET['page'] - 1) * $rowPerPage;

        $query = "SELECT * FROM rap_phim WHERE Ten_rap LIKE '%$key%' LIMIT $offset, $rowPerPage";
        $result = $conn->query($query);
        if(!$result) echo "Câu truy vấn bị lỗi";

        $i = 1;

        if($result->num_rows != 0) {
            while($row = $result->fetch_array()){ ?>
                <tr>
                    <td>
                        <?= $i ?>
                    </td>

                    <td class="text-start">
                        <?= $row['Ten_rap'] ?>
                    </td>

                    <td class="text-start">
                        <?= $row['Dia_chi'] ?>
                    </td>

                    <td>
                        <?= $row['Email'] ?>
                    </td>

                    <td>
                        <?= $row['SDT'] ?>
                    </td>

                    <td>
                        <a href="#" class="btn btn-table-details">Xem chi tiết</a>
                        <a href="edit_cinema.php?id_rap=<?= $row['ID_Rap'] ?>" class="btn btn-table-edit">Sửa</a>
                        <a href="cinema_admin.php?delete=<?= $row['ID_Rap'] ?>" class="btn btn-table-delete" onclick="return confirm('Bạn có chắc muốn xóa?');">Xóa</a>
                    </td>
                </tr>
            <?php $i++;}
        }
        else{ ?>
            <tr>
                <td colspan="6" class="text-center" style="font-size:18px; height:160px; color: rebeccapurple;">
                    <img src="../images/thongbao.png" class="img-search">
                </td>
            </tr>
        <?php }
    }

    if(isset($_POST['key_phong'])){
        $key = $_POST['key_phong'];

        if(!isset($_GET['page1'])){
            $_GET['page1'] = 1;
        }
        $rowPerPage = 6;
        // vị trí của mẩu tin đầu tiên trên mỗi trang
        $offset = ($_GET['page1'] - 1) * $rowPerPage;

        $query = "SELECT * FROM phong_chieu, rap_phim WHERE phong_chieu.ID_Rap = rap_phim.ID_Rap AND Ten_phong LIKE '%$key%' LIMIT $offset, $rowPerPage";
        $result = $conn->query($query);
        if(!$result) echo "Câu truy vấn bị lỗi";

        $i = 1;

        if($result->num_rows != 0) {
            while($row = $result->fetch_array()){ ?>
                <tr>
                    <td>
                        <?= $i ?>
                    </td>

                    <td class="text-start">
                        <?= $row['Ten_rap'] ?>
                    </td>

                    <td class="text-start">
                        <?= $row['Ten_phong'] ?>
                    </td>

                    <td>
                        <a href="edit_phong.php?id_phong=<?= $row['ID_Phong']; ?>" class="btn btn-table-edit">Sửa</a>
                        <a href="cinema_admin.php?delete_phong=<?= $row['ID_Phong']; ?>" class="btn btn-table-delete" onclick="return confirm('Bạn có chắc muốn xóa?');">Xóa</a>
                    </td>
                </tr>
            <?php $i++;}
        }
        else{ ?>
            <tr>
                <td colspan="6" class="text-center" style="font-size:18px; height:160px; color: rebeccapurple;">
                    <img src="../images/thongbao.png" class="img-search">
                </td>
            </tr>
        <?php }
    }
?>