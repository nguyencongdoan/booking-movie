<?php
    include '../thong_ke/database.php';

    if(isset($_POST['key_theloai'])){
        $key = $_POST['key_theloai'];

        if(!isset($_GET['page'])){
            $_GET['page'] = 1;
        }
        $rowPerPage = 6;
        // vị trí của mẩu tin đầu tiên trên mỗi trang
        $offset = ($_GET['page'] - 1) * $rowPerPage;

        $query = "SELECT * FROM the_loai WHERE Ten_TL LIKE '%$key%'LIMIT $offset, $rowPerPage";
        $result = $conn->query($query);
        if(!$result) echo "Câu truy vấn bị lỗi";

        if($result->num_rows != 0) {
            while($row = $result->fetch_array()){ ?>
                <tr>

                    <td class="text-center">
                        <?= $row['ID_TL'] ?>
                    </td>

                    <td class="text-start">
                        <?= $row['Ten_TL'] ?>
                    </td>

                    <td>
                        <a href="theloai.php?id_tl=<?= $row['ID_TL']; ?>" class="btn btn-table-edit">Sửa</a>
                        <a href="theloai.php?delete_tl=<?= $row['ID_TL']; ?>" class="btn btn-table-delete" onclick="return confirm('Bạn có chắc muốn xóa?');">Xóa</a>
                    </td>
                </tr>
            <?php }
        }
        else{ ?>
            <tr>
                <td colspan="6" class="text-center" style="font-size:18px; height:160px; color: rebeccapurple;"><img src="../images/thongbao.png" alt=""></td>
            </tr>
        <?php }
    }

    if(isset($_POST['key_qh'])){
        $key = $_POST['key_qh'];

        if(!isset($_GET['page1'])){
            $_GET['page1'] = 1;
        }
        $rowPerPage = 6;
        // vị trí của mẩu tin đầu tiên trên mỗi trang
        $offset = ($_GET['page1'] - 1) * $rowPerPage;

        $query = "SELECT * FROM quyen_han WHERE Ten_quyen_han LIKE '%$key%' LIMIT $offset, $rowPerPage";
        $result = $conn->query($query);
        if(!$result) echo "Câu truy vấn bị lỗi";

        if($result->num_rows != 0) {
            while($row = $result->fetch_array()){ ?>
                <tr>

                    <td class="text-center">
                        <?= $row['ID_Quyen'] ?>
                    </td>

                    <td class="text-start">
                        <?= $row['Ten_quyen_han'] ?>
                    </td>

                    <td>
                    <a href="theloai.php?id_quyen=<?= $row['ID_Quyen']; ?>" class="btn btn-table-edit">Sửa</a>
                        <a href="theloai.php?delete_quyen=<?= $row['ID_Quyen']; ?>" class="btn btn-table-delete" onclick="return confirm('Bạn có chắc muốn xóa?');">Xóa</a>
                    </td>
                </tr>
            <?php }
        }
        else{ ?>
            <tr>
                <td colspan="6" class="text-center" style="font-size:18px; height:160px; color: rebeccapurple;"><img src="../images/thongbao.png" alt=""></td>
            </tr>
        <?php }
    }
?>