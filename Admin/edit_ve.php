<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Chỉnh sửa thông tin vé bán</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-starter.css">

    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">
    
    <!-- BOX ICONS -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>

     <!-- AJAX -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>
    <script src="../js/timkiem.js"></script>
</head>

<body class="sidebar-menu-collapsed">
    <section>
        <?php 
            include '../Model/config.php';
            include './Action_admin/action_ve.php';
            require_once("./Layout_page/Layout_header.php"); 
        ?>

        <!-- main content start -->
        <div class="main-content">
            <!-- content -->
            <div class="container-fluid content-top-gap">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="suatchieu_ve.php">Quản lý suất chiếu & vé </a></li>
                        <li class="breadcrumb-item active" aria-current="page">Chỉnh sửa thông tin vé bán</li>
                    </ol>
                    <h2 class="fw-bold text-center h2" style="color: rebeccapurple;">CHỈNH SỬA THÔNG TIN VÉ BÁN</h2>
                    <a href="suatchieu_ve.php" class="btn mt-2 mb-3">Quay về trang trước</a>
                </nav>

                <?php if(isset($thongbao)){ ?>
                    <div class="col-md-10 alert alert-<?= $type; ?> mb-5" align="center">
                        <?php echo $thongbao; ?>
                    </div>
                <?php } unset($thongbao) ?>

                <div class="container">
                    <div class="row">
                        <form action="" method="post">
                            <div class="row">
                                <input type="hidden" name="id_ve" value="<?= $id_ve; ?>">
                                <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                    <span class="mt-2">Suất chiếu</span>
                                    <select class="form-select col-md-4 mx-2" name="id_sc">
                                        <?php
                                            $query = "SELECT * FROM suat_chieu WHERE Tinh_trang_chieu = 1";
                                            $result = $conn->query($query);
                                            if(!$result) echo "Câu truy vấn bị lỗi";
                                            
                                            if($result->num_rows != 0) {
                                                while($row = $result->fetch_array()) { 
                                                    if($row['ID_SC'] == $id_tl) { ?>
                                                        <option value="<?= $row['ID_SC']; ?>" selected><?= $row['Ngay_chieu'].' '.$row['Gio_BD'].' ~ '.$row['Gio_KT']; ?></option>
                                                    <?php }
                                                    else { ?>
                                                        <option value="<?= $row['ID_SC']; ?>"><?= $row['Ngay_chieu'].' '.$row['Gio_BD'].' ~ '.$row['Gio_KT']; ?></option>
                                                    <?php }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                    <span class="mt-2">Ngày bán</span>
                                    <input type="date" class="form-control col-md-4 mx-2" name="ngayban" value="<?php if(isset($ngayban)) echo $ngayban; ?>" required>
                                </div>

                                <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                    <span class="mt-2">Giá vé</span>
                                    <input type="number" class="form-control col-md-4 mx-2" name="giave" value="<?php if(isset($giave)) echo $giave; ?>" required>
                                </div>

                                <div class="col-md-12 col-sm-12 mt-3 create-item-movie" style="margin-left: -110px;">
                                    <span class="mt-2" style="transform: translateX(-10px);">Số lượng</span>
                                    <i class='bx bx-minus'></i>
                                    <input type="text" class="form-control col-md-1 mx-2 bx-number" min="1" max="27" name="soluong" value="<?php if(isset($soluong)) echo $soluong; ?>" required>
                                    <i class='bx bx-plus'></i>
                                </div>

                                <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                    <span class="mt-2">Nhân viên bán vé</span>
                                    <select name="id_nv" class="form-select col-md-4 mx-2">
                                        <?php
                                            $query = "SELECT * FROM nhan_vien, quyen_han WHERE nhan_vien.ID_Quyen = quyen_han.ID_Quyen AND Ten_quyen_han = 'Nhân viên'";
                                            $result = $conn->query($query);
                                            if(!$result) echo "Câu truy vấn bị lỗi";
                                            
                                            if($result->num_rows != 0) {
                                                while($row = $result->fetch_array()) { ?>
                                                   <option value="<?= $row['ID_NV']; ?>" <?php if($row['ID_NV'] == $id_nv) echo 'selected'; ?>><?= $row['HoTen']; ?></option>
                                                <?php }
                                            }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-12 col-sm-12 mt-3" style="margin-left: 434px;">
                                    <input type="submit" class="btn" name="edit" value="Cập nhật">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- //content -->
        </div>
        <!-- main content end-->

    </section>
    
    <?php require_once("./Layout_page/Layout_footer.php"); ?>

</body>

</html>
  