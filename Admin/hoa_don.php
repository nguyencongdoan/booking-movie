<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Quản lý hóa đơn</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-starter.css">

    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">
</head>

<body class="sidebar-menu-collapsed">
    <section>
        <?php 
            include '../Model/config.php';
            require_once("./Layout_page/Layout_header.php"); 
        ?>

        <!-- main content start -->
        <div class="main-content">
            <!-- content -->
            <div class="container-fluid content-top-gap">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Quản lý hóa đơn</li>
                    </ol>
                    <h2 class="fw-bold text-center h2" style="color: rebeccapurple;">QUẢN LÝ HÓA ĐƠN</h2>
                    <!-- <a href="create_datve.php" class="btn mt-2 mb-3">Thêm mới</a> -->
                </nav>

                <?php
                    if(isset($_GET['them']) || isset($_GET['sua'])){
                        if(isset($_GET['them']) && $_GET['them'] == 'success'){
                            $_SESSION['thongbao'] = "Thêm mới thành công!";
                        }
                        if(isset($_GET['sua']) && $_GET['sua'] == 'success'){
                            $_SESSION['thongbao'] = "Sửa thông tin thành công";
                        }
                        $_SESSION['type'] = 'success';
                    }
                ?>

                <?php if(isset($_SESSION['thongbao'])){ ?>
                    <div class="col-md-10 alert alert-<?= $_SESSION['type']; ?> mb-5" align="center">
                        <?php echo $_SESSION['thongbao']; ?>
                    </div>
                <?php } unset($_SESSION['thongbao']);?>

                <table class="table mt-5">
                    <thead>
                        <tr>
                            <th>Mã hóa đơn</th>
                            <th>Rạp phim</th>
                            <th>Suất chiếu</th>
                            <th>Tên phim</th>
                            <th>Ngày đặt</th>
                            <th>Vị trí ghế ngồi</th>
                            <th>Tình trạng</th>
                            <th width="255px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(!isset($_GET['page'])){
                                $_GET['page'] = 1;
                            }
                            $rowPerPage = 6;
                            // vị trí của mẩu tin đầu tiên trên mỗi trang
                            $offset = ($_GET['page'] - 1) * $rowPerPage;

                            $query = "SELECT * FROM hoa_don, chi_tiet_hd, phim, rap_phim, phong_chieu, suat_chieu WHERE hoa_don.ID_HD = chi_tiet_hd.ID_HD AND chi_tiet_hd.ID_SC = suat_chieu.ID_SC AND chi_tiet_hd.ID_Phim = phim.ID_phim AND chi_tiet_hd.ID_Rap =rap_phim.ID_Rap AND chi_tiet_hd.ID_Phong = phong_chieu.ID_Phong ORDER BY Tinh_trang LIMIT $offset, $rowPerPage";
                            $result = $conn->query($query);
                            if(!$result) echo "Câu truy vấn bị lỗi";
                            $i = 1;
                            if($result->num_rows != 0) {
                                while($row = $result->fetch_array()){ 
                                        if($row['Tinh_trang'] == 0){
                                            $tinhtrang = 'Chưa xem';
                                            $color = "#f95252";
                                        }
                                        else{
                                            $tinhtrang = 'Đã xem';
                                            $color = "lightgreen";
                                        }
                                    ?>
                                    <tr>
                                        <td>HD<?= $row['ID_HD'] ?></td>

                                        <td>
                                            <?= $row['Ten_rap'] ?>
                                        </td>

                                        <td>
                                            <?= $row['Gio_BD'].' ~ '.$row['Gio_KT'] ?>
                                            <p><?= $row['Ngay_chieu']; ?></p>
                                        </td>

                                        <td>
                                            <?= $row['Ten_phim'] ?>
                                        </td>

                                        <td>
                                            <?= $row['Ngay_dat'] ?>
                                        </td>

                                        <td>
                                            <?= $row['List_ghe'] ?>
                                        </td>

                                        <td>
                                            <span class="fw-bold" style="color: <?= $color ?>"><?= $tinhtrang ?></span>
                                        </td>

                                        <td>
                                            <?php
                                                if($row['Tinh_trang'] == 0){ ?>
                                                    <a href="./Action_admin/action_hoadon.php?id_hd=<?= $row['ID_HD'] ?>&id_nv=<?= $_SESSION['ID_User'] ?>" class="btn" style="height: 38px; border: none !important;">
                                                        <box-icon name='check' color='#36e152'></box-icon>
                                                    </a>
                                                <?php }
                                            ?>
                                            <a href="chitiet_hd.php?xem_ct=<?= $row['ID_HD'] ?>" class="btn btn-table-details">Xem chi tiết</a>
                                            <a href="dat_ve.php?delete=<?= $row['ID_HD'] ?>" onclick="return confirm('Bạn có chắc muốn xóa?');" class="btn btn-table-delete">Xóa</a>
                                        </td>
                                    </tr>
                                <?php $i++;}
                            }
                            else{ ?>
                                <tr>
                                    <td colspan="9" class="text-center" style="font-size:18px; height:160px; color: rebeccapurple;"><img src="../images/thongbao.png" alt=""></td>
                                </tr>
                            <?php }
                        ?>
                    </tbody>
                </table>
                
                <?php
                    $re = $conn->query("SELECT * FROM hoa_don, chi_tiet_hd, phim, rap_phim, phong_chieu, suat_chieu WHERE hoa_don.ID_HD = chi_tiet_hd.ID_HD AND chi_tiet_hd.ID_SC = suat_chieu.ID_SC AND chi_tiet_hd.ID_Phim = phim.ID_phim AND chi_tiet_hd.ID_Rap =rap_phim.ID_Rap AND chi_tiet_hd.ID_Phong = phong_chieu.ID_Phong");
                    $numRows = mysqli_num_rows($re);
                    $maxPage = ceil($numRows/$rowPerPage);
                ?>

                <nav class="d-flex justify-content-end p-4" aria-label="Page navigation example">
                    <ul class="pagination">
                    <li class="page-item">
                            <?php
                                if($_GET["page"] > 1){ ?>
                                    <a class="page-link" href="hoa_don.php?&page=<?= $_GET['page'] - 1 ?>" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                <?php }
                            ?>
                        </li>
                        <?php 
                            for ($i=1 ; $i<=$maxPage ; $i++)
                            {
                                if($i == $_GET['page']){ ?>
                                    <li class="page-item">
                                        <a class="page-link active" href="hoa_don.php?&page=<?= $i ?>"><?= $i ?></a> <!-- trang hiện tại sẽ được style -->
                                    </li>
                                <?php }
                                else { ?>
                                    <li class="page-item">
                                        <a class="page-link" href="hoa_don.php?&page=<?= $i ?>"><?= $i ?></a> 
                                    </li>
                                <?php }
                            }
                        ?>
                        <li class="page-item">
                            <?php
                                if($_GET["page"] < $maxPage){ ?>
                                    <a class="page-link" href="hoa_don.php?&page=<?= $_GET['page'] + 1 ?>" aria-label="Previous">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                <?php }
                                $conn->close();
                            ?>
                        </li>
                    </ul>
                </nav>

            </div>
            <!-- //content -->
        </div>
        <!-- main content end-->

    </section>
    
    <?php require_once("./Layout_page/Layout_footer.php"); ?>

</body>

</html>
  