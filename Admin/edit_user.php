<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Cập nhật thông tinh khách hàng</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-starter.css">

    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">
    <!-- BOX ICONS -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
</head>

<body class="sidebar-menu-collapsed">
    <section>
        <?php 
            include '../Model/config.php';
            include './Action_admin/action_user.php';
            require_once("./Layout_page/Layout_header.php"); 
        ?>

        <!-- main content start -->
        <div class="main-content">
            <!-- content -->
            <div class="container-fluid content-top-gap">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="user.php">Quản lý khách hàng</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Cập nhật thông tinh khách hàng</li>
                    </ol>
                    <h2 class="fw-bold text-center h2" style="color: rebeccapurple;">CẬP NHẬT THÔNG TIN KHÁCH HÀNG</h2>
                    <a href="user.php" class="btn mt-2 mb-3">Quay về trang trước</a>
                </nav>

                <?php if(isset($thongbao)){ ?>
                    <div class="col-md-10 alert alert-<?= $type; ?> mb-5" align="center">
                        <?php echo $thongbao; ?>
                    </div>
                <?php } unset($thongbao) ?>

                <div class="container">
                    <div class="row">
                        <form action="" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <input type="hidden" name="id_kh" value="<?= $id_kh; ?>">
                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span class="mt-2">Họ tên</span>
                                            <input type="text" class="form-control col-md-8 mx-2" name="hoten" value="<?php if(isset($hoten)) echo $hoten; ?>" required>
                                        </div>

                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span class="mt-2">Địa chỉ</span>
                                            <textarea class="form-control col-md-8 mx-2" name="diachi" id="" cols="50" rows="10"><?php if(isset($diachi)) echo $diachi;  ?></textarea>
                                        </div>

                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span class="mt-2">Số điện thoại</span>
                                            <input type="text" class="form-control col-md-8 mx-2" name="sdt" value="<?php if(isset($sdt)) echo $sdt; ?>" required>
                                        </div>

                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span class="mt-2" style="transform: translateX(-130px);">Giới tính</span>
                                            <input type="radio" class="form-check-input mx-2 mx-2" name="gioitinh" value="1" id="nam" <?php if($gioitinh==1) echo 'checked'?>> 
                                            <label for="nam">Nam</label>
                                            <input type="radio" class="form-check-input mx-2 mx-2" name="gioitinh" id="nu" value="0" <?php if($gioitinh==0) echo 'checked'?>>
                                            <label for="nu">Nữ</label>
                                        </div>

                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span class="mt-2">Email</span>
                                            <input type="text" class="form-control col-md-8 mx-2" name="email" value="<?php if(isset($email)) echo $email; ?>" required>
                                        </div>

                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span class="mt-2">Password</span>
                                            <input type="password" class="form-control col-md-8 mx-2" name="password" value="<?php if(isset($password)) echo $password; ?>" required>
                                        </div>

                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span class="mt-2">Quyền hạn</span>
                                            <select class="form-select col-md-8 mx-2" name="id_quyen">
                                                <?php
                                                    $query = "SELECT * FROM quyen_han";
                                                    $result = $conn->query($query);
                                                    if(!$result) echo "Câu truy vấn bị lỗi";
                                                    
                                                    if($result->num_rows != 0) {
                                                        while($row = $result->fetch_array()) { 
                                                            if($row['ID_Quyen'] == $id_quyen) { ?>
                                                                <option value="<?= $row['ID_Quyen']; ?>" selected><?= $row['Ten_quyen_han']; ?></option>
                                                            <?php }
                                                            else{?> 
                                                                <option value="<?= $row['ID_Quyen']; ?>"><?= $row['Ten_quyen_han']; ?></option>
                                                            <?php }
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12"> 
                                    <span style="font-size: 17px; padding-left: 28px;">Hình ảnh</span>
                                    <label for="image" class="label-img mt-3"> 
                                        <?php 
                                            if(!empty($hinh)){?>
                                                <img src="../images/<?= $hinh ?>" alt="" class="img-edit">
                                            <?php }
                                            else{ ?>
                                                <i class='bx bx-plus' id="icon-images"></i> 
                                            <?php }
                                        ?>
                                    </label>
                                    <input type="file" class="mt-4" name="hinh" value="<?php if(isset($hinh)) echo $hinh; ?>" id="image">
                                </div>

                                <div class="col-md-12 col-sm-12 mt-3" style="margin-left: 152px;">
                                    <input type="submit" class="btn" name="edit" value="Cập nhật">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- //content -->
        </div>
        <!-- main content end-->

    </section>
    
    <?php require_once("./Layout_page/Layout_footer.php"); ?>

</body>

</html>
  