<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Quản lý đặt vé xem phim</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-starter.css">

    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">
</head>

<body class="sidebar-menu-collapsed">
    <section>
        <?php 
            include '../Model/config.php';
            include './Action_admin/action_datve.php';
            require_once("./Layout_page/Layout_header.php"); 
        ?>

        <!-- main content start -->
        <div class="main-content">
            <!-- content -->
            <div class="container-fluid content-top-gap">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="dat_ve.php">Đặt vé xem phim</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Xem chi tiết</li>
                    </ol>
                    <h2 class="fw-bold text-center h2" style="color: rebeccapurple;">XEM CHI TIẾT</h2>
                    <a href="dat_ve.php" class="btn mt-2 mb-3">Quay về trang trước</a>
                </nav>

                <div class="container mt-5">
                    <div class="row">
                        <div class="col-md-3">
                            <img src="../images/<?= $hinh ?>" style="border-radius: 12px;">
                        </div>
                        <div class="col-md-9">
                            <h5 class="h5-xemct">Mã vé đặt: <span><?= $id_dv ?> </span></h5> 
                            <h5 class="h5-xemct">Tên phim: <span><?= $tenphim ?></span></h5> 
                            <h5 class="h5-xemct">Rạp phim: <span><?= $tenrap ?></span></h5> 
                            <h5 class="h5-xemct">Phòng chiếu: <span><?= $tenphong ?></span></h5> 
                            <h5 class="h5-xemct">Suất chiếu: <span><?= $tensc ?></span></h5> 
                            <h5 class="h5-xemct">Vị trí ngồi: <span><?= $ds ?></span></h5> 
                            <h5 class="h5-xemct">Số lượng: <span><?= $soluong ?></span></h5> 
                            <h5 class="h5-xemct">Phương thức thanh toán: <span><?= $phuong_thuc_tt ?></span></h5> 
                            <h5 class="h5-xemct">Ngày đặt: <span><?= $ngaydat ?></span></h5> 
                        </div>
                    </div>
                </div>
            </div>
            <!-- //content -->
        </div>
        <!-- main content end-->

    </section>
    
    <?php require_once("./Layout_page/Layout_footer.php"); ?>

</body>

</html>
  