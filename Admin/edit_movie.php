<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Sửa thông tin phim</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="assets/css/style-starter.css">

    <!-- google fonts -->
    <link href="//fonts.googleapis.com/css?family=Nunito:300,400,600,700,800,900&display=swap" rel="stylesheet">
    <!-- BOX ICONS -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
</head>

<body class="sidebar-menu-collapsed">
    <section>
        <?php 
            include '../Model/config.php';
            include './Action_admin/action_movie.php';
            require_once("./Layout_page/Layout_header.php"); 
        ?>

        <!-- main content start -->
        <div class="main-content">
            <!-- content -->
            <div class="container-fluid content-top-gap">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb my-breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page"><a href="movie.php">Quản lý phim & tập phim </a></li>
                        <li class="breadcrumb-item active" aria-current="page">Chỉnh sửa thông tin phim </li>
                    </ol>
                    <h2 class="fw-bold text-center h2" style="color: rebeccapurple;">CHỈNH SỬA THÔNG TIN PHIM</h2>
                    <a href="movie.php" class="btn mt-2 mb-3">Quay về trang trước</a>
                </nav>
                
                <?php if(isset($thongbao)){ ?>
                    <div class="col-md-10 alert alert-<?= $type; ?> mb-5" align="center">
                        <?php echo $thongbao; ?>
                    </div>
                <?php } ?>

                <div class="container">
                    <div class="row">
                        <form action="" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <input type="hidden" name="id_phim" value="<?= $id_phim; ?>">
                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span class="mt-2">Tên Phim</span>
                                            <input type="text" class="form-control col-md-8 mx-2" name="tenphim" value="<?= $tenphim; ?>" required>
                                        </div>

                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span class="mt-2">Thời lượng</span>
                                            <input type="text" class="form-control col-md-8 mx-2" name="thoiluong" value="<?= $thoiluong; ?>" required>
                                        </div>

                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span class="mt-2">Tóm tắt</span>
                                            <textarea name="tomtat" class="form-control col-md-8 mx-2" cols="50" rows="10"><?= $tomtat; ?></textarea>
                                        </div>

                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span>Diễn viên</span>
                                            <input type="text" class="form-control col-md-8 mx-2" name="dienvien" value="<?= $dienvien; ?>" required>
                                        </div>

                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span class="mt-2">Đạo diễn</span>
                                            <input type="text" class="form-control col-md-8 mx-2" name="daodien" value="<?= $daodien; ?>" required>
                                        </div>

                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span class="mt-2">Quốc gia</span>
                                            <input type="text" class="form-control col-md-8 mx-2" name="quocgia" value="<?= $quocgia; ?>" required>
                                        </div>

                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span>Năm phát hành</span>
                                            <input type="text" class="form-control col-md-8 mx-2" name="namph" value="<?= $nam_phat_hanh; ?>" required>
                                        </div>

                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span>Lượt xem</span>
                                            <input type="text" class="form-control col-md-8 mx-2" name="luot_xem" value="<?= $luot_xem; ?>" required>
                                        </div>

                                        <div class="col-md-12 col-sm-12 mt-3 create-item-movie">
                                            <span class="mt-2">Thể loại</span>
                                            <select class="form-select col-md-8 mx-2" name="id_tl">
                                                <?php
                                                    $query = "SELECT * FROM the_loai";
                                                    $result = $conn->query($query);
                                                    if(!$result) echo "Câu truy vấn bị lỗi";
                                                    
                                                    if($result->num_rows != 0) {
                                                        while($row = $result->fetch_array()) { 
                                                            if($row['ID_TL'] == $id_tl) { ?>
                                                                <option value="<?= $row['ID_TL']; ?>" selected><?= $row['Ten_TL']; ?></option>
                                                            <?php }
                                                            else { ?>
                                                                <option value="<?= $row['ID_TL']; ?>"><?= $row['Ten_TL']; ?></option>
                                                            <?php }
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12"> 
                                    <span style="font-size: 17px; padding-left: 28px;">Hình ảnh</span>
                                    <label for="image" class="label-img mt-3"> 
                                        <?php 
                                            if(!empty($hinh)){?>
                                                <img src="../images/<?= $hinh ?>" alt="" class="img-edit">
                                            <?php }
                                            else{ ?>
                                                <i class='bx bx-plus' id="icon-images"></i> 
                                            <?php }
                                        ?>
                                    </label>
                                    <input type="file" class="mt-4" name="hinh" value="<?= $hinh; ?>" id="image">
                                </div>
                                <div class="col-md-12 col-sm-12 mt-3"  style="margin-left: 153px;">
                                    <input type="submit" class="btn" name="edit" value="Cập nhật">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- //content -->
        </div>
        <!-- main content end-->

    </section>
    
    <?php require_once("./Layout_page/Layout_footer.php"); ?>

</body>

</html>
  