$.post("thong_ke/data_phim.php", function(data) {
	console.log(data);
	var tenphim = [];
	var total = [];

	for(var i in data){
		tenphim.push(data[i].Ten_phim);
		total.push(data[i].Total);
	}

	var options = {
		responsive: true,

		legend: {
			display: true
		},
		scales: {
			xAxes: [{
				display: true
			}],
		yAxes: [{
				ticks: {
					beginAtZero: true
				}
			}]
		}
	}
	// const randomColor = colors[Math.floor(Math.random() * colors.length)];
	var data_chart = {
		labels: tenphim,
		datasets: [
			{
				label: 'Tổng tiền',
				borderColor: '#f7dddc',
				backgroundColor: ['#f7dddc', '#f7dddc', '#def1de', '#f7dddc', '#f7dddc', '#f7dddc'],
				hoverBackgroundColor: '#fcefdc',
				data: total
			}
		]
	};

	var bar = $("#barchart");
	var barGrap = new Chart(bar, {
		type: 'bar',
		data: data_chart,
		options: options
	});
});
