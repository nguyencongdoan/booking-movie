$.post("thong_ke/data_rap.php", function (data) {
    var labels = [];
    var result = [];
    for (var i in data) {
        labels.push(data[i].Ten_rap);
        result.push(data[i].Total);
    }
    var pie = $("#piechart");
    var myChart = new Chart(pie, {
        type: 'pie',
        data: {
            labels: labels,
            datasets: [
                {
                    data: result,
                    borderColor: ["rgba(217, 83, 79,1)","rgba(240, 173, 78, 1)","rgba(92, 184, 92, 1)", "#03c8957a", "#e39ebe", "#d6b5df", "#d6b5df"],
                    backgroundColor: ["rgba(217, 83, 79,0.2)","rgba(240, 173, 78, 0.2)","rgba(92, 184, 92, 0.2)", "#03c8957a", "#e39ebe", "#d6b5df", "#d6b5df"],
                }
            ]
        },
    });
});