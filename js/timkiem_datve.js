// xử lý dữ liệu cho tìm kiếm bên trang dat_ve.php của user

$(document).ready(function() {
    $("input[name='ds_ghe[]'").change(function() {
        var max = $('.bx-number').val();
        var sl_check = $("input[name='ds_ghe[]']:checked").length;
        var sl_dis = $("input[name='ds_ghe[]']:disabled").length;
        number = sl_check - sl_dis;
        
        if (number > max){
            $(this).prop("checked", "");
            alert("Bạn chỉ được chọn tối đa " + max + " ghế");
        }
        // lay gia tri the checkbox dang checked de thay doi gia tri cho the p ds_ghe
        var checkbox = document.getElementsByName('ds_ghe[]');
        var result = "";
        for (var i = 0; i < checkbox.length; i++){
            if (checkbox[i].checked === true && checkbox[i].disabled === false){
                result += " " + checkbox[i].value;
            }
        }
        $('p.ds_ghe').html(result);
    });

    $('.rap_phim').click(function() {
        var id = $('.rap_phim').val();

        // lay gia tri text cua the option selected de thay doi gia tri html cho the p tenrap
        var id1 = document.getElementById("id_rap");
        var giatri = id1.options[id1.selectedIndex].text;

        $.post('./Model/data_phongchieu.php', {data_id_rap: id}, function(data){
            $('.phong_chieu').html(data);
        });

        $('p.tenrap').html(giatri);
    });

    $('.phong_chieu').click(function() {
        var id_phong = $('.phong_chieu').val();
        var id_rap = $('.rap_phim').val();

        // lay gia tri text cua the option selected de thay doi gia tri html cho the p phongchieu
        var id1 = document.getElementById("id_phong");
        var giatri = id1.options[id1.selectedIndex].text;

        $.post('./Model/data_suatchieu.php', {data_id_rap: id_rap, data_id_phong: id_phong}, function(data){
            $('.suat_chieu').html(data);
        });

        $('p.phongchieu').html(giatri);
    });

    $('.suat_chieu').click(function() {
        var id_sc = document.getElementById("id_sc").value;
        var id_rap = $('.rap_phim').val();
        var id_phong = $('.phong_chieu').val();

        // lay gia tri text cua the option selected de thay doi gia tri html cho the p suatchieu
        var id1 = document.getElementById("id_sc");
        var giatri = id1.options[id1.selectedIndex].text;

        $.post('./Model/data_phim.php', {data_id_sc: id_sc, data_id_rap: id_rap, data_id_phong: id_phong}, function(data){
            $('.phim').html(data);
        });

        $('p.suatchieu').html(giatri);
    });

    $('.phim').click(function() {
        var id_phim = document.getElementById("id_phim").value;
        var id_sc = document.getElementById("id_sc").value;
        var id_rap = $('.rap_phim').val();
        var id_phong = document.getElementById("id_phong").value;
        
        // lay gia tri text cua the option selected de thay doi gia tri html cho the p tenphim
        var id1 = document.getElementById("id_phim");
        var giatri = id1.options[id1.selectedIndex].text;
        
        $.post('./Model/data_ghe.php', {data_id_phim: id_phim, data_id_sc: id_sc, data_id_rap: id_rap, data_id_phong: id_phong}, function(data){
            $('.list_ghe').html(data);
        });

        $('p.tenphim').html(giatri);
    });

     // xu ly + - so luong 
    var min = $('.bx-number').attr('min');
    var max = $('.bx-number').attr('max');

    $('.bx-minus').click(function(){
        var value = $('.bx-number').val();
        if(Number(value) > min) {
            $('.bx-number').val(Number(value) - 1);
            // xu ly thay doi html cua the p soluong
            $('p.soluong').html(Number(value) - 1)
        }

        // xu ly thay doi html cua the p total
        if(Number(value) != 1){
            var total = (Number(value) - 1) * 45000;
            const formatter = new Intl.NumberFormat('vi-VN', {
                style: 'currency',
                currency: 'VND'
            });
            $('p.total').html(formatter.format(total));
        }
    });

    $('.bx-plus').click(function(){
        var value = $('.bx-number').val();
        if(Number(value) < max) {
            $('.bx-number').val(Number(value) + 1);
            // xu ly thay doi html cua the p soluong
            $('p.soluong').html(Number(value) + 1)
        }

        // xu ly thay doi html cua the p total
        if(Number(value) >= 1){
            var total = (Number(value) + 1) * 45000;
            const formatter = new Intl.NumberFormat('vi-VN', {
                style: 'currency',
                currency: 'VND'
            });
            $('p.total').html(formatter.format(total));
        }
    });
});