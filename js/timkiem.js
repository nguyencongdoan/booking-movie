$(document).ready(function() {
    // den trang data de lay ra danh sach phong chieu dua vao id_rap
    $('.rap_phim').click(function() {
        var id = $('.rap_phim').val();
        $.post('data.php', {data_id_rap: id}, function(data){
            $('.phong_chieu').html(data);
        });
    });

    // den trang data_suatchieu de lay thong tin suat chieu dua vao id_rap va id_phong
    $('.phong_chieu').click(function() {
        var id_phong = $('.phong_chieu').val();
        var id_rap = $('.rap_phim').val();

        $.post('data_suatchieu.php', {data_id_rap: id_rap, data_id_phong: id_phong}, function(data){
            $('.suat_chieu').html(data);
        });
    });

    // den trang data_phim de lay thong tin phim dua vao id_sc
    $('.suat_chieu').click(function() {
        var id_sc = $('.suat_chieu').val();
        var id_phong = $('.phong_chieu').val();
        var id_rap = $('.rap_phim').val();

        $.post('data_phim.php', {data_id_sc: id_sc, data_id_rap: id_rap, data_id_phong: id_phong}, function(data){
            $('.phim').html(data);
        });
    });
    
    // xu ly + - so luong 
    var min = $('.bx-number').attr('min');
    var max = $('.bx-number').attr('max');

    $('.bx-minus').click(function(){
        var value = $('.bx-number').val();
        if(Number(value) > min) {
            $('.bx-number').val(Number(value) - 1);
        }
        // $('p.total').html(value * 45000);
        console.log(value);
    });

    $('.bx-plus').click(function(){
        var value = $('.bx-number').val();
        if(Number(value) < max) {
            $('.bx-number').val(Number(value) + 1);
        }
        // $('p.total').html(value * 45000);
        console.log(value);
    });
});