-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2022 at 02:18 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `booking_movie`
--

-- --------------------------------------------------------

--
-- Table structure for table `binh_luan`
--

CREATE TABLE `binh_luan` (
  `ID_BL` tinyint(4) NOT NULL,
  `ID_KH` tinyint(4) NOT NULL,
  `ID_Phim` tinyint(4) NOT NULL,
  `Noi_dung` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Ngay` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `binh_luan`
--

INSERT INTO `binh_luan` (`ID_BL`, `ID_KH`, `ID_Phim`, `Noi_dung`, `Ngay`) VALUES
(6, 1, 15, 'Phim hay quá', '2021-11-04 20:08:42'),
(7, 1, 15, 'Diễn viên chuyên nghiệp', '2021-11-04 20:53:50'),
(8, 1, 17, 'nhân vật chính hơi ngáo nha', '2021-11-04 20:54:26'),
(9, 9, 15, 'phim xứng đáng top 1', '2021-11-04 20:55:56'),
(11, 1, 16, 'Phim hay quá', '2021-11-08 12:15:10'),
(12, 1, 7, '123', '2021-11-09 01:42:32'),
(13, 1, 22, 'Diễn viên chuyên nghiệp', '2021-11-09 06:16:14');

-- --------------------------------------------------------

--
-- Table structure for table `chi_tiet_hd`
--

CREATE TABLE `chi_tiet_hd` (
  `ID_HD` tinyint(4) NOT NULL,
  `Ngay_dat` date NOT NULL,
  `So_luong` int(11) NOT NULL,
  `List_ghe` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Tong_tien` int(11) NOT NULL,
  `ID_SC` tinyint(4) NOT NULL,
  `ID_Phim` tinyint(4) NOT NULL,
  `ID_Rap` tinyint(4) NOT NULL,
  `ID_Phong` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chi_tiet_hd`
--

INSERT INTO `chi_tiet_hd` (`ID_HD`, `Ngay_dat`, `So_luong`, `List_ghe`, `Tong_tien`, `ID_SC`, `ID_Phim`, `ID_Rap`, `ID_Phong`) VALUES
(59, '2021-12-09', 2, 'A1, B2', 90000, 3, 4, 3, 27),
(60, '2021-12-09', 1, 'A1', 45000, 8, 7, 1, 1),
(62, '2022-02-16', 2, 'A1, A2', 90000, 1, 2, 1, 1),
(63, '2022-02-16', 1, 'A1', 45000, 9, 3, 2, 3),
(64, '2022-02-16', 4, 'A1, A2, B1, B2', 180000, 3, 4, 3, 5),
(65, '2022-02-16', 3, 'A3, A4, A5', 135000, 3, 4, 3, 5),
(66, '2022-02-16', 3, 'A1, A2, B2', 135000, 4, 5, 4, 7);

-- --------------------------------------------------------

--
-- Table structure for table `dat_ve`
--

CREATE TABLE `dat_ve` (
  `ID_Dat` tinyint(4) NOT NULL,
  `So_luong` int(11) NOT NULL,
  `Ngay_dat` datetime NOT NULL,
  `List_ghe` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ID_KH` tinyint(4) NOT NULL,
  `ID_Phim` tinyint(4) NOT NULL,
  `ID_SC` tinyint(4) NOT NULL,
  `ID_Rap` tinyint(4) NOT NULL,
  `ID_Phong` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dat_ve`
--

INSERT INTO `dat_ve` (`ID_Dat`, `So_luong`, `Ngay_dat`, `List_ghe`, `ID_KH`, `ID_Phim`, `ID_SC`, `ID_Rap`, `ID_Phong`) VALUES
(62, 2, '2021-12-09 10:37:34', 'A1, B2', 1, 4, 3, 3, 27),
(63, 1, '2021-12-09 10:39:42', 'A1', 1, 7, 8, 1, 1),
(65, 2, '2022-02-16 10:07:02', 'A1, A2', 1, 2, 1, 1, 1),
(66, 1, '2022-02-16 10:23:19', 'A1', 1, 3, 9, 2, 3),
(67, 4, '2022-02-16 10:24:45', 'A1, A2, B1, B2', 1, 4, 3, 3, 5),
(68, 3, '2022-02-16 10:34:38', 'A3, A4, A5', 1, 4, 3, 3, 5),
(69, 3, '2022-02-16 10:39:35', 'A1, A2, B2', 1, 5, 4, 4, 7);

-- --------------------------------------------------------

--
-- Table structure for table `ds_daxem`
--

CREATE TABLE `ds_daxem` (
  `ID_DS` tinyint(4) NOT NULL,
  `ID_KH` tinyint(4) NOT NULL,
  `ID_Phim` tinyint(4) NOT NULL,
  `Ngay_Xem` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ds_daxem`
--

INSERT INTO `ds_daxem` (`ID_DS`, `ID_KH`, `ID_Phim`, `Ngay_Xem`) VALUES
(1, 1, 7, '2021-11-04 16:43:02'),
(2, 1, 3, '2021-11-04 16:43:34'),
(3, 1, 15, '2021-11-04 18:50:40'),
(4, 1, 17, '2021-11-04 20:53:55'),
(5, 9, 22, '2021-11-04 20:55:17'),
(6, 9, 16, '2021-11-04 20:55:28'),
(7, 1, 4, '2021-11-07 21:20:00'),
(8, 1, 10, '2021-11-08 11:14:36'),
(9, 1, 21, '2021-11-09 01:09:53'),
(10, 1, 2, '2021-11-09 01:42:43'),
(11, 1, 9, '2021-11-09 06:15:33'),
(12, 1, 8, '2021-11-09 06:15:47'),
(13, 10, 22, '2021-11-09 06:19:32'),
(14, 10, 16, '2021-11-09 06:19:43'),
(15, 1, 19, '2021-11-09 07:29:20'),
(16, 1, 12, '2021-11-09 07:38:24'),
(17, 1, 18, '2021-11-09 07:40:20'),
(18, 1, 20, '2021-11-09 07:41:55'),
(19, 1, 6, '2021-11-09 07:44:46'),
(20, 1, 16, '2021-11-09 07:45:08'),
(21, 10, 17, '2021-11-09 08:16:23'),
(22, 10, 9, '2021-11-09 08:20:05');

-- --------------------------------------------------------

--
-- Table structure for table `hoa_don`
--

CREATE TABLE `hoa_don` (
  `ID_HD` tinyint(4) NOT NULL,
  `ID_KH` tinyint(4) NOT NULL,
  `ID_NV` tinyint(4) NOT NULL,
  `ID_Dat` tinyint(4) NOT NULL,
  `Phuong_thuc_tt` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Tinh_trang` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hoa_don`
--

INSERT INTO `hoa_don` (`ID_HD`, `ID_KH`, `ID_NV`, `ID_Dat`, `Phuong_thuc_tt`, `Tinh_trang`) VALUES
(59, 1, 0, 62, 'Thanh toán Online', 0),
(60, 1, 0, 63, 'Thanh toán Online', 0),
(62, 1, 0, 65, 'Thanh toán Online', 0),
(63, 1, 0, 66, 'Thanh toán Online', 0),
(64, 1, 0, 67, 'Thanh toán Online', 0),
(65, 1, 0, 68, 'Thanh toán Online', 0),
(66, 1, 0, 69, 'Thanh toán Online', 0);

-- --------------------------------------------------------

--
-- Table structure for table `khach_hang`
--

CREATE TABLE `khach_hang` (
  `ID_KH` tinyint(4) NOT NULL,
  `Ho_ten` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Hinh` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Dia_chi` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SDT` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Gioi_tinh` tinyint(1) NOT NULL,
  `So_du` int(11) NOT NULL,
  `ID_Quyen` tinyint(4) NOT NULL,
  `Email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Passwords` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `khach_hang`
--

INSERT INTO `khach_hang` (`ID_KH`, `Ho_ten`, `Hinh`, `Dia_chi`, `SDT`, `Gioi_tinh`, `So_du`, `ID_Quyen`, `Email`, `Passwords`) VALUES
(1, 'Nguyễn Công Đoan', 'avatar.png', 'Phú Yên', '0123456789', 1, 0, 1, 'doan.nc.60cntt@ntu.edu.vn', '202cb962ac59075b964b07152d234b70'),
(3, 'Nguyễn Văn A', '', 'CGV', '0123456789', 1, 0, 1, 'nguyenvana@gmail.com', '202cb962ac59075b964b07152d234b70'),
(9, 'Nguyễn Phan Hà Phương', 'avatar.png', 'Phú Yên', '0123456789', 0, 0, 1, 'phuong@gmail.com', '202cb962ac59075b964b07152d234b70'),
(10, 'Thanh Hải', 'avatar.png', 'Nha Trang', '0123456789', 1, 0, 1, 'hai@gmail.com', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Table structure for table `nhan_vien`
--

CREATE TABLE `nhan_vien` (
  `ID_NV` tinyint(4) NOT NULL,
  `HoTen` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Hinh` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Dia_chi` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SDT` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Gioi_tinh` tinyint(1) NOT NULL,
  `ID_Quyen` tinyint(4) NOT NULL,
  `Email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Passwords` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nhan_vien`
--

INSERT INTO `nhan_vien` (`ID_NV`, `HoTen`, `Hinh`, `Dia_chi`, `SDT`, `Gioi_tinh`, `ID_Quyen`, `Email`, `Passwords`) VALUES
(2, 'Lê Trọng Kha', 'hinh.png', 'Nha Trang', '0123456789', 1, 2, 'kha.lt.60cntt@ntu.edu.vn', '202cb962ac59075b964b07152d234b70'),
(6, 'Trần Thanh Hoài', '', 'Phú Yên', '0123456789', 1, 3, 'hoai@gmail.com', '202cb962ac59075b964b07152d234b70'),
(10, 'Phạm Quốc Đạt', '', 'Nha Trang', '0123456789', 1, 3, 'dat@gmail.com', '202cb962ac59075b964b07152d234b70'),
(11, 'thành tất1', '', 'Nha Trang', '0123456789', 0, 3, 'tat@gmail.com', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `order_id` tinyint(255) NOT NULL COMMENT 'Mã hóa đơn',
  `thanh_vien` varchar(50) NOT NULL COMMENT 'thành viên',
  `money` float NOT NULL COMMENT 'số tiền thanh toán',
  `note` varchar(255) DEFAULT NULL COMMENT 'ghi chú thanh toán',
  `vnp_response_code` varchar(255) NOT NULL COMMENT 'mã phản hồi',
  `code_vnpay` varchar(255) NOT NULL COMMENT 'mã giao dịch vnpay',
  `code_bank` varchar(255) NOT NULL COMMENT 'mã ngân hàng',
  `time` datetime NOT NULL COMMENT 'thời gian chuyển khoản'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `order_id`, `thanh_vien`, `money`, `note`, `vnp_response_code`, `code_vnpay`, `code_bank`, `time`) VALUES
(12, 63, 'Nguyễn Công Đoan', 45000, 'Đặt vé xem phim', '00', '13687926', 'NCB', '2022-02-16 10:00:00'),
(13, 64, 'Nguyễn Công Đoan', 180000, 'Đặt vé xem phim', '00', '13687929', 'NCB', '2022-02-16 10:00:00'),
(14, 65, 'Nguyễn Công Đoan', 135000, 'Đặt vé xem phim', '00', '13687945', 'NCB', '2022-02-16 10:00:00'),
(15, 66, 'Nguyễn Công Đoan', 135000, 'Đặt vé xem phim', '00', '13687956', 'NCB', '2022-02-16 10:39:58');

-- --------------------------------------------------------

--
-- Table structure for table `phim`
--

CREATE TABLE `phim` (
  `ID_phim` tinyint(4) NOT NULL,
  `Ten_phim` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Thoi_luong` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Hinh` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Tom_tat` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Luot_xem` int(11) NOT NULL,
  `Dien_vien` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Dao_dien` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Quoc_gia` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Nam_phat_hanh` year(4) NOT NULL,
  `ID_TL` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `phim`
--

INSERT INTO `phim` (`ID_phim`, `Ten_phim`, `Thoi_luong`, `Hinh`, `Tom_tat`, `Luot_xem`, `Dien_vien`, `Dao_dien`, `Quoc_gia`, `Nam_phat_hanh`, `ID_TL`) VALUES
(2, 'Theatre of the dead', '120 phút', 'theatre-dead.jpg', 'Tủ Quần Áo Bí Ẩn kể về Sang Won (Ha Jung Woo) cùng con gái Yi Na (Heo Yool) chuyển đến sống trong 1 căn nhà mới khi mẹ cô bé mất vì 1 tai nạn đáng tiếc. Kể từ ngày Yi Na khoe với bố mình có 1 người bạn mới, Sang Won bắt đầu nghe thấy những tiếng động kỳ lạ trong tủ quần áo và mơ thấy những cơn ác mộng đáng sợ. Sau khi Yi Na biến mất, Sang Woo nhận ra nơi duy nhất mình có thể tìm thấy con gái: tủ quần áo bí ẩn.', 80, 'La Vân Hi, Tống Thiến', 'Han Dong, Liu Yi Zhi', 'Mỹ', 2021, 1),
(3, 'Transformer', '120 phút', 'transformer.jpg', 'Tủ Quần Áo Bí Ẩn kể về Sang Won (Ha Jung Woo) cùng con gái Yi Na (Heo Yool) chuyển đến sống trong 1 căn nhà mới khi mẹ cô bé mất vì 1 tai nạn đáng tiếc. Kể từ ngày Yi Na khoe với bố mình có 1 người bạn mới, Sang Won bắt đầu nghe thấy những tiếng động kỳ lạ trong tủ quần áo và mơ thấy những cơn ác mộng đáng sợ. Sau khi Yi Na biến mất, Sang Woo nhận ra nơi duy nhất mình có thể tìm thấy con gái: tủ quần áo bí ẩn.', 80, 'Ha Jung Woo, Kim Nam Gil, Heo Yool, Kim Shi Ah, Sh', 'Kim Kwang Bin', 'Hàn Quốc', 2021, 1),
(4, 'Resident Evil', '120 phút', 'resident-evil.jpg', 'Phàm Nhân Tu Tiên vietsub | A Record Of A Mortal\'s Journey To Immortality vietsub | Fanren Xiu Xian Chuan Zhi Fanren Feng Qi Tian Nan vietsub: Chuyển thể từ tiểu thuyết tiên hiệp nổi tiếng cùng tên của tác giả Vong Ngữ. Câu chuyện kể về Hàn Lập - một người bình thường lập chí trở thành tiên nhân. Một thiếu niên sống trong sơn thôn, tư chất bình thường lại gặp vô vàn cơ duyên để bước đi trên con đường tu tiên. Hàn Lập từng bước khẳng định mình trong hàng ngũ tiên nhân. Mối tình gian truân của Hàn', 90, 'Ha Jung Woo, Kim Nam Gil, Heo Yool, Kim Shi Ah, Sh', 'Kim Kwang Bin', 'Mỹ', 2021, 1),
(5, 'Captain Marvel ', '100 phút', 'captain-marvel.png', 'Recipient of the Super-Soldier serum, World War II hero Steve Rogers fights for American ideals as one of the world’s mightiest heroes and the leader of the Avengers.', 12, 'Tom Cruise, Adam Sendler, Johnny Depp', 'Leonardo Dicaprio', 'Mỹ', 2021, 1),
(6, 'Hunter Killer', '115 phút', 'hunter-killer.jpg', 'Phim kể về hố khoan Kola Superdeep của Nga được khoan sâu 12.000m vào lòng đất – là cơ sở nghiên cứu bí mật sâu nhất thế giới được ghi lại cho đến nay. Vào năm 1984, các nhà khoa học bắt đầu nghe được những âm thanh lạ vọng từ hố sâu Kola và quyết định đóng cửa cơ sở nghiên cứu này. Một nhóm nghiên cứu đã quyết định thám hiểm hố khoan để tìm ra bí mật mà nơi đây đang ẩn giấu. Tuy nhiên, từng thà', 24, 'Milena Radulovic, Sergey Ivanyuk, Nikolay Kovbas', 'Arseny Syuhin', 'Mỹ', 2021, 1),
(7, 'Bloodshot', '120 phút', 'blood-shot.jpg', 'Phim kể về hố khoan Kola Superdeep của Nga được khoan sâu 12.000m vào lòng đất – là cơ sở nghiên cứu bí mật sâu nhất thế giới được ghi lại cho đến nay. Vào năm 1984, các nhà khoa học bắt đầu nghe được những âm thanh lạ vọng từ hố sâu Kola và quyết định đóng cửa cơ sở nghiên cứu này. Một nhóm nghiên cứu đã quyết định thám hiểm hố khoan để tìm ra bí mật mà nơi đây đang ẩn giấu. Tuy nhiên, từng thà', 100, 'Milena Radulovic, Sergey Ivanyuk, Nikolay Kovbas', 'Arseny Syuhin', 'Mỹ', 2021, 1),
(8, 'Call', '120 phút', 'call.jpg', 'Phim kể về hố khoan Kola Superdeep của Nga được khoan sâu 12.000m vào lòng đất – là cơ sở nghiên cứu bí mật sâu nhất thế giới được ghi lại cho đến nay. Vào năm 1984, các nhà khoa học bắt đầu nghe được những âm thanh lạ vọng từ hố sâu Kola và quyết định đóng cửa cơ sở nghiên cứu này. Một nhóm nghiên cứu đã quyết định thám hiểm hố khoan để tìm ra bí mật mà nơi đây đang ẩn giấu. Tuy nhiên, từng thà', 40, 'Arseny Syuhin', 'Arseny Syuhin', 'Hàn Quốc', 2021, 1),
(9, 'Supergirl', '115 phút', 'supergirl.jpg', 'Supergirl is one of the most popular and oldest DC female characters since her first story\'s publication in 1959, starring in magazines as well as solo books. Superman\'s cousin Kara Zor-El is the most popular and most iconic version, but her character has been reinvented several times and replaced for a while.', 100, 'Milena Radulovic, Sergey Ivanyuk, Nikolay Kovbas', 'Arseny Syuhin', 'Mỹ', 2020, 2),
(10, 'Stranger Things', '120 phút', 'stranger-thing.jpg', 'Phim kể về hố khoan Kola Superdeep của Nga được khoan sâu 12.000m vào lòng đất – là cơ sở nghiên cứu bí mật sâu nhất thế giới được ghi lại cho đến nay. Vào năm 1984, các nhà khoa học bắt đầu nghe được những âm thanh lạ vọng từ hố sâu Kola và quyết định đóng cửa cơ sở nghiên cứu này. Một nhóm nghiên cứu đã quyết định thám hiểm hố khoan để tìm ra bí mật mà nơi đây đang ẩn giấu.', 120, 'Milena Radulovic, Sergey Ivanyuk, Nikolay Kovbas', 'Arseny Syuhin', 'Mỹ', 2021, 2),
(11, 'Star Trek', '120 phút', 'star-trek.jpg', 'Phim kể về hố khoan Kola Superdeep của Nga được khoan sâu 12.000m vào lòng đất – là cơ sở nghiên cứu bí mật sâu nhất thế giới được ghi lại cho đến nay. Vào năm 1984, các nhà khoa học bắt đầu nghe được những âm thanh lạ vọng từ hố sâu Kola và quyết định đóng cửa cơ sở nghiên cứu này. Một nhóm nghiên cứu đã quyết định thám hiểm hố khoan để tìm ra bí mật mà nơi đây đang ẩn giấu.', 10, 'Ha Jung Woo, Kim Nam Gil, Heo Yool, Kim Shi Ah, Sh', 'Kim Kwang Bin', 'Hàn Quốc', 2018, 2),
(12, 'Penthouses', '100 phút', 'penthouses.jpg', 'Phàm Nhân Tu Tiên vietsub | A Record Of A Mortal\'s Journey To Immortality vietsub | Fanren Xiu Xian Chuan Zhi Fanren Feng Qi Tian Nan vietsub: Chuyển thể từ tiểu thuyết tiên hiệp nổi tiếng cùng tên của tác giả Vong Ngữ. Câu chuyện kể về Hàn Lập - một người bình thường lập chí trở thành tiên nhân. Một thiếu niên sống trong sơn thôn, tư chất bình thường lại gặp vô vàn cơ duyên để bước đi trên con đường tu tiên. Hàn Lập từng bước khẳng định mình trong hàng ngũ tiên nhân. Mối tình gian truân của Hàn', 120, 'Tom Cruise, Adam Sendler, Johnny Depp', 'Leonardo Dicaprio', 'Hàn Quốc', 2021, 5),
(13, 'Mandalorian', '90 phút', 'mandalorian.jpg', 'Phim kể về hố khoan Kola Superdeep của Nga được khoan sâu 12.000m vào lòng đất – là cơ sở nghiên cứu bí mật sâu nhất thế giới được ghi lại cho đến nay. Vào năm 1984, các nhà khoa học bắt đầu nghe được những âm thanh lạ vọng từ hố sâu Kola và quyết định đóng cửa cơ sở nghiên cứu này. Một nhóm nghiên cứu đã quyết định thám hiểm hố khoan để tìm ra bí mật mà nơi đây đang ẩn giấu.', 50, 'Milena Radulovic, Sergey Ivanyuk, Nikolay Kovbas', 'Arseny Syuhin', 'Mỹ', 2019, 5),
(14, 'The Falcon And The Winter Soldier', '90 phút', 'the-falcon.webp', 'Phim kể về hố khoan Kola Superdeep của Nga được khoan sâu 12.000m vào lòng đất – là cơ sở nghiên cứu bí mật sâu nhất thế giới được ghi lại cho đến nay. Vào năm 1984, các nhà khoa học bắt đầu nghe được những âm thanh lạ vọng từ hố sâu Kola và quyết định đóng cửa cơ sở nghiên cứu này. Một nhóm nghiên cứu đã quyết định thám hiểm hố khoan để tìm ra bí mật mà nơi đây đang ẩn giấu.', 66, 'Milena Radulovic, Sergey Ivanyuk, Nikolay Kovbas', 'Arseny Syuhin', 'Mỹ', 2010, 2),
(15, 'Demon Slayer', '25 phút', 'demon-slayer.jpg', 'Thanh Gươm Diệt Quỷ : Chuyến Tàu Vô Tận là phiên bản bao gồm 7 tập với nhiều phân đoạn mới chưa xuất hiện trong bản điện ảnh đã được ra mắt trước đó. Trong phần này, Tanjiro cùng những người bạn của mình đã tới vùng đất của nhiệm vụ tiếp theo: \"Chuyến tàu vô tận\". Chỉ trong thời gian ngắn, nơi này đã có hơi bốn mươi người mất tích. Mặc dù đã có một số kiếm sĩ được phái ra ngoài, nhưng tất cả đều bị mất liên lạc. Nhóm Tanjiro và Zenitsu, Inosuke cùng với Nezuko họp mặt với một trong những kiếm sĩ', 40, 'Milena Radulovic, Sergey Ivanyuk, Nikolay Kovbas', 'Haruo Sotozaki', 'Nhật Bản', 2020, 6),
(16, 'Croods', '24 phút', 'croods.jpg', 'Thanh Gươm Diệt Quỷ : Chuyến Tàu Vô Tận là phiên bản bao gồm 7 tập với nhiều phân đoạn mới chưa xuất hiện trong bản điện ảnh đã được ra mắt trước đó. Trong phần này, Tanjiro cùng những người bạn của mình đã tới vùng đất của nhiệm vụ tiếp theo: \"Chuyến tàu vô tận\". Chỉ trong thời gian ngắn, nơi này đã có hơi bốn mươi người mất tích. Mặc dù đã có một số kiếm sĩ được phái ra ngoài, nhưng tất cả đều bị mất liên lạc. Nhóm Tanjiro và Zenitsu, Inosuke cùng với Nezuko họp mặt với một trong những kiếm sĩ', 122, 'Milena Radulovic, Sergey Ivanyuk, Nikolay Kovbas', 'Haruo Sotozaki', 'Nhật Bản', 2020, 6),
(17, 'Dragonball', '30 phút', 'dragon.jpg', 'Movie lần này sẽ kể một câu chuyện hoàn toàn mới tách khỏi những câu chuyện đã cũ từ 2 movie trước: Pokemon the Movie: The Power of Us (2018) về truyền thuyết Lugia và Mewtwo Strikes Back Evolution (2019) một bản 3D CGI remake cho bộ phim năm 1998 về Mewtwo. Câu chuyện của bộ phim mới lấy bối cảnh ở Rừng Okoya, một thiên đường Pokémon được bảo vệ bởi các quy tắc nghiêm ngặt cấm người ngoài đặt chân vào bên trong. Bộ phim tập trung vào Coco, một cậu bé được nuôi dưỡng bởi Pokemon và cũng tự coi m', 90, 'Ha Jung Woo, Kim Nam Gil, Heo Yool, Kim Shi Ah, Sh', 'Tetsuo Yajima', 'Nhật Bản', 2018, 6),
(18, 'Over The Moon', '100 phút', 'over-the-moon.jpg', ' Sáu tháng trôi qua kể từ khi Thất đại tội tan rã sau trận chiến với Quỷ Vương và Cath Palug, trận chiến mà họ đã chiến thắng và cái giá phải trả là cái chết của anh chàng Escanor. Sau khi để Liones cùng Elizabeth tới thăm những nơi cô đã sống trong kiếp trước của mình trước lúc đăng quang, meliodas đã gặp em trai của mình là Zeldris và Gelda khi họ đang trên đường quay về vương quốc quỷ', 120, 'Tom Cruise, Adam Sendler, Johnny Depp', 'Hamana Takayuki', 'Nhật Bản', 2021, 6),
(19, 'Weathering With You', '90 phút', 'weathering.jpg', 'Sáu tháng trôi qua kể từ khi Thất đại tội tan rã sau trận chiến với Quỷ Vương và Cath Palug, trận chiến mà họ đã chiến thắng và cái giá phải trả là cái chết của anh chàng Escanor. Sau khi để Liones cùng Elizabeth tới thăm những nơi cô đã sống trong kiếp trước của mình trước lúc đăng quang, meliodas đã gặp em trai của mình là Zeldris và Gelda khi họ đang trên đường quay về vương quốc quỷ...', 121, 'Milena Radulovic, Sergey Ivanyuk, Nikolay Kovbas', 'Arseny Syuhin', 'Nhật Bản', 2019, 6),
(20, 'Your Name', '20 phút', 'your-name.jpg', 'Sáu tháng trôi qua kể từ khi Thất đại tội tan rã sau trận chiến với Quỷ Vương và Cath Palug, trận chiến mà họ đã chiến thắng và cái giá phải trả là cái chết của anh chàng Escanor. Sau khi để Liones cùng Elizabeth tới thăm những nơi cô đã sống trong kiếp trước của mình trước lúc đăng quang, meliodas đã gặp em trai của mình là Zeldris và Gelda khi họ đang trên đường quay về vương quốc quỷ...', 66, 'Milena Radulovic, Sergey Ivanyuk, Nikolay Kovbas', 'Arseny Syuhin', 'Nhật Bản', 2010, 6),
(21, 'Coco', '40 phút', 'coco.jpg', 'Sáu tháng trôi qua kể từ khi Thất đại tội tan rã sau trận chiến với Quỷ Vương và Cath Palug, trận chiến mà họ đã chiến thắng và cái giá phải trả là cái chết của anh chàng Escanor. Sau khi để Liones cùng Elizabeth tới thăm những nơi cô đã sống trong kiếp trước của mình trước lúc đăng quang, meliodas đã gặp em trai của mình là Zeldris và Gelda khi họ đang trên đường quay về vương quốc quỷ...', 122, 'Ge Bu, Huang Kang Xiang, Li Fei, Wang Wen Xuan, Ya', 'Hei Zi', 'Nhật Bản', 2021, 6),
(22, 'Wanda Vision', '50 phút', 'wanda.png', 'WandaVision sẽ là loạt phim khai thác tất cả mọi điều về quá khứ của Scarlet Witch và lý do tại sao cô lại có được năng lực của mình. Elizabeth Olsen đã đạt được sự công nhận với vai diễn Wanda Maximoff / Scarlet Witch trong các bộ phim siêu anh hùng của Vũ trụ Điện ảnh Marvel, bao gồm Avengers: Age of Ultron (2015), Captain America: Civil War (2016), Avengers: Infinity War (2018) và Avengers: Endgame (2019).', 200, 'Ge Bu, Huang Kang Xiang, Li Fei, Wang Wen Xuan, Ya', 'Hei Zi', 'Mỹ', 2021, 2);

-- --------------------------------------------------------

--
-- Table structure for table `phong_chieu`
--

CREATE TABLE `phong_chieu` (
  `ID_Phong` tinyint(4) NOT NULL,
  `Ten_phong` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ID_Rap` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `phong_chieu`
--

INSERT INTO `phong_chieu` (`ID_Phong`, `Ten_phong`, `ID_Rap`) VALUES
(1, 'Phòng Starium', 1),
(3, 'Phòng 4DX', 2),
(5, 'Phòng Screen X', 3),
(7, 'Phòng Gold Class', 4),
(9, 'Phòng Cine & Living Room', 5),
(11, 'Phòng Art House', 6),
(21, 'Phòng Dolby Atmos', 7),
(25, 'Phòng Premium', 1),
(26, 'Phòng Premium', 2),
(27, 'Phòng 4DX', 3),
(28, 'Phòng Premium', 4),
(29, 'Phòng Screen X', 5),
(30, 'Phòng Starium', 6),
(31, 'Phòng Gold Class', 7),
(32, 'Phòng Premium', 8),
(33, 'Phòng Cine & Living Room', 8),
(34, 'Phòng Starium', 9),
(35, 'Phòng Art House', 9),
(36, 'Phòng Cine & Living Room', 10),
(37, 'Phòng Premium', 10),
(38, 'Phòng Art House', 11),
(39, 'Phòng Starium', 11);

-- --------------------------------------------------------

--
-- Table structure for table `quyen_han`
--

CREATE TABLE `quyen_han` (
  `ID_Quyen` tinyint(4) NOT NULL,
  `Ten_quyen_han` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quyen_han`
--

INSERT INTO `quyen_han` (`ID_Quyen`, `Ten_quyen_han`) VALUES
(1, 'User'),
(2, 'Admin'),
(3, 'Nhân viên');

-- --------------------------------------------------------

--
-- Table structure for table `rap_phim`
--

CREATE TABLE `rap_phim` (
  `ID_Rap` tinyint(4) NOT NULL,
  `Ten_rap` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Dia_chi` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `SDT` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Url_map` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rap_phim`
--

INSERT INTO `rap_phim` (`ID_Rap`, `Ten_rap`, `Dia_chi`, `Email`, `SDT`, `Url_map`) VALUES
(1, 'CGV Vincom Bà Triệu', 'Tầng 6, VinCom Center Hà Nội, 191 Bà Triệu, Q. Hai Bà Trưng, Tp. Hà Nội', 'cgv.cinema.vn@gmail.com', '0123456789', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.5395147021454!2d105.84732201478425!3d21.011088093757515!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab8c636ccf95%3A0xee8c463dd46033b4!2sCGV%20Vincom%20Center%20B%C3%A0%20Tri%E1%BB%87u!5e0!3m2!1svi!2s!4v1634303347947!5m2!1svi!2s'),
(2, 'CGV Mipec Tower', 'Tầng 5, MIPEC Tower, 229 Tây Sơn, Q. Đống Đa, Tp. Hà Nội', 'cgv.cinema.vn@gmail.com', '0123456789', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.677953135134!2d105.82115301478417!3d21.00554269394673!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac816762f289%3A0x45790755a9b4ac7e!2sCGV%20Mipec%20Tower!5e0!3m2!1svi!2s!4v1634303413744!5m2!1svi!2s'),
(3, 'CGV Hồ Gươm Plaza', 'Tầng 3, TTTM Hồ Gươm Plaza, 110 Trần Phú, P. Mỗ Lao, Q. Hà Đông, Tp. Hà Nội', 'cgv.cinema.vn@gmail.com', '0123456789', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.345873133539!2d105.78324771478378!3d20.978768294859304!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acd2c0e21d7b%3A0xec2205f220faeb2!2zQ0dWIEjhu5MgR8awxqFtIFBsYXph!5e0!3m2!1svi!2s!4v1634303443220!5m2!1svi!2s'),
(4, 'CGV IPH Hà Nội', 'Tầng 4, Indochina Plaza Hà Nội, 241 Xuân Thủy, Q. Cầu Giấy, Tp. Hà Nội', 'cgv.cinema.vn@gmail.com', '0123456789', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.923501286751!2d105.7806583147846!3d21.035746692915893!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab4ab82e8a2d%3A0xe52fd6b755ba654c!2zMjQxIFh1w6JuIFRo4buneSwgROG7i2NoIFbhu41uZyBI4bqtdSwgQ-G6p3UgR2nhuqV5LCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1634303481141!5m2!1svi!2s'),
(5, 'CGV Aeon Long Biên', 'Tầng 4 - TTTM AEON Long Biên, Số 27 Cổ Linh, Q. Long Biên, Tp. Hà Nội', 'cgv.cinema.vn@gmail.com', '0123456789', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.1469947288324!2d105.89845411478444!3d21.0268035932212!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135a96a85453f37%3A0x39ce34aaf20a939!2zVHJ1bmcgdMOibSB0aMawxqFuZyBt4bqhaSBBZW9uIE1hbGwgTG9uZyBCacOqbg!5e0!3m2!1svi!2s!4v1634303510395!5m2!1svi!2s'),
(6, 'CGV CT Plaza', 'Tầng 10, CT Plaza, 60A Trường Sơn, P.2, Q. Tân Bình, Tp. Hồ Chí Minh', 'cgv.cinema.vn@gmail.com', '0123456789', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.9771438203493!2d106.66336691468409!3d10.813060861473463!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529180a52601b%3A0xaf95c7444b843c70!2zQ0dWIFRyxrDhu51uZyBTxqFuIChDR1YgQ1QgUGxhemEp!5e0!3m2!1svi!2s!4v1634303535760!5m2!1svi!2s'),
(7, 'CGV Hùng Vương Plaza', 'Tầng 7, Hùng Vương Plaza, 126 Hùng Vương, Q.5, Tp. Hồ Chí Minh', 'cgv.cinema.vn@gmail.com', '0123456789', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.7154025143823!2d106.66023601468392!3d10.756403162504505!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752ef05f38e56b%3A0x1f8850db32555ade!2zVOG6p25nIDcsIFBhcmtzb24gSHVuZyBWdW9uZyBQbGF6YSwgMTI2IMSQxrDhu51uZyBIw7luZyBWxrDGoW5nLCBQaMaw4budbmcgMTIsIFF14bqtbiA1LCBUaMOgbmggcGjhu5EgSOG7kyBDaMOtIE1pbmgsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v163'),
(8, 'CGV Crescent Mall', 'Lầu 5, Crescent Mall Đại lộ Nguyễn Văn Linh, Phú Mỹ Hưng, Q.7 Tp. Hồ Chí Minh', 'cgv.cinema.vn@gmail.com', '0123456789', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3920.0726833476256!2d106.71666811468366!3d10.728877463003565!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f894ec205e9%3A0x2a5bb83950a26d5b!2sCGV%20Crescent%20Mall!5e0!3m2!1svi!2s!4v1634303597955!5m2!1svi!2s'),
(9, 'CGV Pandora City', 'Lầu 3, Pandora City 1/1 Trường Chinh, Q. Tân Phú, Tp. Hồ Chí Minh', 'cgv.cinema.vn@gmail.com', '0123456789', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.061608819967!2d106.63207341468409!3d10.806593461591412!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3175295978943725%3A0x4012c0c89a5b6635!2sCGV%20Pandora%20City!5e0!3m2!1svi!2s!4v1634303627093!5m2!1svi!2s'),
(10, 'CGV Aeon Tân Phú', 'Lầu 3, Aeon Mall 30 Bờ Bao Tân Thắng, P. Sơn Kỳ, Q. Tân Phú, Tp. Hồ Chí Minh', 'cgv.cinema.vn@gmail.com', '0123456789', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.1233946252487!2d106.61586831468401!3d10.801860161677649!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752b9535b60699%3A0x4737f3be8bd41d5b!2zQUVPTiBNQUxMIFTDgk4gUEjDmg!5e0!3m2!1svi!2s!4v1634303693382!5m2!1svi!2s'),
(11, 'CGV Vĩnh Trung Plaza', '255-257 đường Hùng Vương, Q. Thanh Khê, Tp. Đà Nẵng', 'cgv.cinema.vn@gmail.com', '0123456789', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.4355699549387!2d106.690801214684!3d10.777913562113746!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f3a11c3ffff%3A0x1eec2a371020e263!2zc-G7kSAyNTUsIDI1NyBIw7luZyBWxrDGoW5nLCBQaMaw4budbmcgNiwgUXXhuq1uIDMsIMSQw6AgTuG6tW5nLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1634303721813!5m2!1svi!2s');

-- --------------------------------------------------------

--
-- Table structure for table `suat_chieu`
--

CREATE TABLE `suat_chieu` (
  `ID_SC` tinyint(4) NOT NULL,
  `Ngay_chieu` date NOT NULL,
  `Gio_BD` time NOT NULL,
  `Gio_KT` time NOT NULL,
  `Tinh_trang_chieu` tinyint(1) NOT NULL,
  `ID_Phim` tinyint(4) NOT NULL,
  `ID_Rap` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suat_chieu`
--

INSERT INTO `suat_chieu` (`ID_SC`, `Ngay_chieu`, `Gio_BD`, `Gio_KT`, `Tinh_trang_chieu`, `ID_Phim`, `ID_Rap`) VALUES
(1, '2021-10-14', '20:00:00', '21:00:00', 1, 2, 1),
(2, '2021-10-14', '16:00:00', '18:00:00', 1, 3, 2),
(3, '2021-10-14', '14:00:00', '15:00:00', 1, 4, 3),
(4, '2021-10-12', '09:00:00', '10:00:00', 1, 5, 4),
(5, '2021-10-10', '08:00:00', '09:00:00', 1, 6, 5),
(6, '2021-10-12', '18:00:00', '20:00:00', 1, 7, 6),
(7, '2021-10-08', '20:00:00', '21:00:00', 1, 8, 7),
(8, '2021-10-31', '20:00:00', '21:00:00', 1, 7, 1),
(9, '2021-10-14', '20:00:00', '21:00:00', 1, 3, 2),
(10, '2021-11-01', '10:00:00', '11:30:00', 1, 3, 1),
(11, '2021-11-02', '14:30:00', '15:30:00', 1, 4, 2),
(12, '2021-11-02', '20:00:00', '21:00:00', 1, 4, 2),
(13, '2021-11-02', '22:00:00', '12:00:00', 1, 10, 3),
(14, '2021-11-04', '10:00:00', '12:00:00', 1, 12, 9),
(15, '2021-11-09', '10:55:00', '01:54:00', 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tap_phim`
--

CREATE TABLE `tap_phim` (
  `ID_Tap` tinyint(4) NOT NULL,
  `Ten_tap` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Url_video` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ID_Phim` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tap_phim`
--

INSERT INTO `tap_phim` (`ID_Tap`, `Ten_tap`, `Url_video`, `ID_Phim`) VALUES
(1, 'Trailer', 'Theatre of the Dead (2013) Trailer #1 OFFICIAL Independent ZOMBIE Film.mp4', 2),
(2, 'Trailer', 'WandaVision Official Trailer (2020) - Marvel - Wandavision - Official - Trailer.mp4', 22),
(3, 'Trailer', 'THE CROODS- FAMILY TREE Trailer (2021).mp4', 16),
(4, 'Trailer', 'Vietsub- Penthouse - Cuộc Chiến Thượng Lưu 3 - Highlight.mp4', 2),
(5, 'Tập 1', 'WandaVision Official Trailer (2020) - Marvel - Wandavision - Official - Trailer.mp4', 22),
(6, 'Tập 2', 'WandaVision Official Trailer (2020) - Marvel - Wandavision - Official - Trailer.mp4', 22),
(7, 'Trailer', 'Coco Official Final Trailer.mp4', 21),
(8, 'Tập 3', 'WandaVision Official Trailer (2020) - Marvel - Wandavision - Official - Trailer.mp4', 22),
(9, 'Tập 4', 'WandaVision Official Trailer (2020) - Marvel - Wandavision - Official - Trailer.mp4', 22),
(10, 'Tập 5', 'WandaVision Official Trailer (2020) - Marvel - Wandavision - Official - Trailer.mp4', 22),
(11, 'Tập 1', 'Vietsub- Penthouse - Cuộc Chiến Thượng Lưu 3 - Highlight.mp4', 10),
(12, 'Tập 2', 'Coco Official Final Trailer.mp4', 10),
(13, 'Tập 3', 'Vietsub- Penthouse - Cuộc Chiến Thượng Lưu 3 - Highlight.mp4', 10),
(14, 'Tập 4', 'Vietsub- Penthouse - Cuộc Chiến Thượng Lưu 3 - Highlight.mp4', 10),
(15, 'Tập 1', 'Coco Official Final Trailer.mp4', 2),
(16, 'Tập 1', 'THE CROODS- FAMILY TREE Trailer (2021).mp4', 16),
(37, 'Tập 2', 'THE CROODS- FAMILY TREE Trailer (2021).mp4', 16),
(38, 'Trailer', 'THE CROODS- FAMILY TREE Trailer (2021).mp4', 7),
(39, 'Trailer', 'Phim -Venom- Đối Mặt Tử Thù- Trailer - Sắp Công Chiếu.mp4', 4),
(40, 'Trailer', 'Phim -Venom- Đối Mặt Tử Thù- Trailer - Sắp Công Chiếu.mp4', 3),
(41, 'Tập 1', 'Phim -Venom- Đối Mặt Tử Thù- Trailer - Sắp Công Chiếu.mp4', 7),
(42, 'Trailer', 'Coco Official Final Trailer.mp4', 12),
(43, 'Trailer', 'THE CROODS- FAMILY TREE Trailer (2021).mp4', 9),
(44, 'Trailer', 'Coco Official Final Trailer.mp4', 19),
(45, 'Trailer', 'THE CROODS- FAMILY TREE Trailer (2021).mp4', 18),
(46, 'Trailer', 'THE CROODS- FAMILY TREE Trailer (2021).mp4', 17),
(47, 'Trailer', 'THE CROODS- FAMILY TREE Trailer (2021).mp4', 20),
(48, 'Trailer', 'THE CROODS- FAMILY TREE Trailer (2021).mp4', 15),
(49, 'Trailer', 'Phim -Venom- Đối Mặt Tử Thù- Trailer - Sắp Công Chiếu.mp4', 14),
(50, 'Trailer', 'Theatre of the Dead (2013) Trailer #1 OFFICIAL Independent ZOMBIE Film.mp4', 13),
(51, 'Trailer', 'WandaVision Official Trailer (2020) - Marvel - Wandavision - Official - Trailer.mp4', 11),
(52, 'Trailer', 'Vietsub- Penthouse - Cuộc Chiến Thượng Lưu 3 - Highlight.mp4', 8),
(53, 'Trailer', 'Vietsub- Penthouse - Cuộc Chiến Thượng Lưu 3 - Highlight.mp4', 6),
(54, 'Trailer', 'WandaVision Official Trailer (2020) - Marvel - Wandavision - Official - Trailer.mp4', 5);

-- --------------------------------------------------------

--
-- Table structure for table `the_loai`
--

CREATE TABLE `the_loai` (
  `ID_TL` tinyint(4) NOT NULL,
  `Ten_TL` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `the_loai`
--

INSERT INTO `the_loai` (`ID_TL`, `Ten_TL`) VALUES
(1, 'Phim chiếu rạp'),
(2, 'Hành động'),
(3, 'Tình cảm'),
(4, 'Phim bộ'),
(5, 'Phim lẻ'),
(6, 'Anime');

-- --------------------------------------------------------

--
-- Table structure for table `ve_ban`
--

CREATE TABLE `ve_ban` (
  `ID_Ve` tinyint(4) NOT NULL,
  `Ngay_ban` date NOT NULL,
  `Gia_ve` int(11) NOT NULL,
  `So_luong` int(11) NOT NULL,
  `ID_SC` tinyint(4) NOT NULL,
  `ID_NV` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ve_ban`
--

INSERT INTO `ve_ban` (`ID_Ve`, `Ngay_ban`, `Gia_ve`, `So_luong`, `ID_SC`, `ID_NV`) VALUES
(1, '2021-11-01', 12000, 5, 2, 10),
(3, '2021-11-02', 40000, 10, 1, 6),
(4, '2021-11-03', 40000, 27, 1, 6),
(5, '2021-11-02', 50000, 30, 2, 6),
(6, '2021-11-02', 40000, 12, 2, 6),
(7, '2021-11-03', 100000, 1, 3, 10),
(8, '2021-11-04', 20000, 22, 4, 10),
(9, '2021-11-03', 30000, 40, 6, 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `binh_luan`
--
ALTER TABLE `binh_luan`
  ADD PRIMARY KEY (`ID_BL`),
  ADD KEY `FK_bl_kh` (`ID_KH`),
  ADD KEY `FK_bl_phim` (`ID_Phim`);

--
-- Indexes for table `chi_tiet_hd`
--
ALTER TABLE `chi_tiet_hd`
  ADD KEY `FK_ct_hd` (`ID_HD`),
  ADD KEY `FK_ct_phim` (`ID_Phim`),
  ADD KEY `FK_ct_sc` (`ID_SC`),
  ADD KEY `FK_ct_rap` (`ID_Rap`),
  ADD KEY `FK_ct_pc` (`ID_Phong`);

--
-- Indexes for table `dat_ve`
--
ALTER TABLE `dat_ve`
  ADD PRIMARY KEY (`ID_Dat`),
  ADD KEY `FK_dv_kh` (`ID_KH`),
  ADD KEY `FK_dv_phim` (`ID_Phim`),
  ADD KEY `FK_dv_rap` (`ID_Rap`),
  ADD KEY `FK_dv_sc` (`ID_SC`),
  ADD KEY `FK_dv_pc` (`ID_Phong`);

--
-- Indexes for table `ds_daxem`
--
ALTER TABLE `ds_daxem`
  ADD PRIMARY KEY (`ID_DS`),
  ADD KEY `FK_ds_kh` (`ID_KH`),
  ADD KEY `FK_ds_phim` (`ID_Phim`);

--
-- Indexes for table `hoa_don`
--
ALTER TABLE `hoa_don`
  ADD PRIMARY KEY (`ID_HD`),
  ADD KEY `FK_hd_kh` (`ID_KH`);

--
-- Indexes for table `khach_hang`
--
ALTER TABLE `khach_hang`
  ADD PRIMARY KEY (`ID_KH`),
  ADD KEY `FK_kh_qh` (`ID_Quyen`);

--
-- Indexes for table `nhan_vien`
--
ALTER TABLE `nhan_vien`
  ADD PRIMARY KEY (`ID_NV`),
  ADD KEY `FK_nv_qh` (`ID_Quyen`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_payments_hd` (`order_id`);

--
-- Indexes for table `phim`
--
ALTER TABLE `phim`
  ADD PRIMARY KEY (`ID_phim`),
  ADD KEY `FK_phim_tl` (`ID_TL`);

--
-- Indexes for table `phong_chieu`
--
ALTER TABLE `phong_chieu`
  ADD PRIMARY KEY (`ID_Phong`),
  ADD KEY `FK_phong_rap` (`ID_Rap`);

--
-- Indexes for table `quyen_han`
--
ALTER TABLE `quyen_han`
  ADD PRIMARY KEY (`ID_Quyen`);

--
-- Indexes for table `rap_phim`
--
ALTER TABLE `rap_phim`
  ADD PRIMARY KEY (`ID_Rap`);

--
-- Indexes for table `suat_chieu`
--
ALTER TABLE `suat_chieu`
  ADD PRIMARY KEY (`ID_SC`),
  ADD KEY `FK_sc_phim` (`ID_Phim`),
  ADD KEY `FK_sc_rap` (`ID_Rap`);

--
-- Indexes for table `tap_phim`
--
ALTER TABLE `tap_phim`
  ADD PRIMARY KEY (`ID_Tap`),
  ADD KEY `FK_phim_tap` (`ID_Phim`);

--
-- Indexes for table `the_loai`
--
ALTER TABLE `the_loai`
  ADD PRIMARY KEY (`ID_TL`);

--
-- Indexes for table `ve_ban`
--
ALTER TABLE `ve_ban`
  ADD PRIMARY KEY (`ID_Ve`),
  ADD KEY `FK_vb_sc` (`ID_SC`),
  ADD KEY `FK_vb_nv` (`ID_NV`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `binh_luan`
--
ALTER TABLE `binh_luan`
  MODIFY `ID_BL` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `dat_ve`
--
ALTER TABLE `dat_ve`
  MODIFY `ID_Dat` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `ds_daxem`
--
ALTER TABLE `ds_daxem`
  MODIFY `ID_DS` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `hoa_don`
--
ALTER TABLE `hoa_don`
  MODIFY `ID_HD` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `khach_hang`
--
ALTER TABLE `khach_hang`
  MODIFY `ID_KH` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `nhan_vien`
--
ALTER TABLE `nhan_vien`
  MODIFY `ID_NV` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `phim`
--
ALTER TABLE `phim`
  MODIFY `ID_phim` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `phong_chieu`
--
ALTER TABLE `phong_chieu`
  MODIFY `ID_Phong` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `quyen_han`
--
ALTER TABLE `quyen_han`
  MODIFY `ID_Quyen` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rap_phim`
--
ALTER TABLE `rap_phim`
  MODIFY `ID_Rap` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `suat_chieu`
--
ALTER TABLE `suat_chieu`
  MODIFY `ID_SC` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tap_phim`
--
ALTER TABLE `tap_phim`
  MODIFY `ID_Tap` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `the_loai`
--
ALTER TABLE `the_loai`
  MODIFY `ID_TL` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ve_ban`
--
ALTER TABLE `ve_ban`
  MODIFY `ID_Ve` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `binh_luan`
--
ALTER TABLE `binh_luan`
  ADD CONSTRAINT `FK_bl_kh` FOREIGN KEY (`ID_KH`) REFERENCES `khach_hang` (`ID_KH`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_bl_phim` FOREIGN KEY (`ID_Phim`) REFERENCES `phim` (`ID_phim`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chi_tiet_hd`
--
ALTER TABLE `chi_tiet_hd`
  ADD CONSTRAINT `FK_ct_hd` FOREIGN KEY (`ID_HD`) REFERENCES `hoa_don` (`ID_HD`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ct_pc` FOREIGN KEY (`ID_Phong`) REFERENCES `phong_chieu` (`ID_Phong`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ct_phim` FOREIGN KEY (`ID_Phim`) REFERENCES `phim` (`ID_phim`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ct_rap` FOREIGN KEY (`ID_Rap`) REFERENCES `rap_phim` (`ID_Rap`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ct_sc` FOREIGN KEY (`ID_SC`) REFERENCES `suat_chieu` (`ID_SC`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dat_ve`
--
ALTER TABLE `dat_ve`
  ADD CONSTRAINT `FK_dv_kh` FOREIGN KEY (`ID_KH`) REFERENCES `khach_hang` (`ID_KH`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_dv_pc` FOREIGN KEY (`ID_Phong`) REFERENCES `phong_chieu` (`ID_Phong`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_dv_phim` FOREIGN KEY (`ID_Phim`) REFERENCES `phim` (`ID_phim`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_dv_rap` FOREIGN KEY (`ID_Rap`) REFERENCES `rap_phim` (`ID_Rap`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_dv_sc` FOREIGN KEY (`ID_SC`) REFERENCES `suat_chieu` (`ID_SC`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ds_daxem`
--
ALTER TABLE `ds_daxem`
  ADD CONSTRAINT `FK_ds_kh` FOREIGN KEY (`ID_KH`) REFERENCES `khach_hang` (`ID_KH`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ds_phim` FOREIGN KEY (`ID_Phim`) REFERENCES `phim` (`ID_phim`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hoa_don`
--
ALTER TABLE `hoa_don`
  ADD CONSTRAINT `FK_hd_kh` FOREIGN KEY (`ID_KH`) REFERENCES `khach_hang` (`ID_KH`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `khach_hang`
--
ALTER TABLE `khach_hang`
  ADD CONSTRAINT `FK_kh_qh` FOREIGN KEY (`ID_Quyen`) REFERENCES `quyen_han` (`ID_Quyen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `nhan_vien`
--
ALTER TABLE `nhan_vien`
  ADD CONSTRAINT `FK_nv_qh` FOREIGN KEY (`ID_Quyen`) REFERENCES `quyen_han` (`ID_Quyen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `FK_payments_hd` FOREIGN KEY (`order_id`) REFERENCES `hoa_don` (`ID_HD`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `phim`
--
ALTER TABLE `phim`
  ADD CONSTRAINT `FK_phim_tl` FOREIGN KEY (`ID_TL`) REFERENCES `the_loai` (`ID_TL`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `phong_chieu`
--
ALTER TABLE `phong_chieu`
  ADD CONSTRAINT `FK_phong_rap` FOREIGN KEY (`ID_Rap`) REFERENCES `rap_phim` (`ID_Rap`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `suat_chieu`
--
ALTER TABLE `suat_chieu`
  ADD CONSTRAINT `FK_sc_phim` FOREIGN KEY (`ID_Phim`) REFERENCES `phim` (`ID_phim`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_sc_rap` FOREIGN KEY (`ID_Rap`) REFERENCES `rap_phim` (`ID_Rap`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tap_phim`
--
ALTER TABLE `tap_phim`
  ADD CONSTRAINT `FK_phim_tap` FOREIGN KEY (`ID_Phim`) REFERENCES `phim` (`ID_phim`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ve_ban`
--
ALTER TABLE `ve_ban`
  ADD CONSTRAINT `FK_vb_nv` FOREIGN KEY (`ID_NV`) REFERENCES `nhan_vien` (`ID_NV`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_vb_sc` FOREIGN KEY (`ID_SC`) REFERENCES `suat_chieu` (`ID_SC`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
