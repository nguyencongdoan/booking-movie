<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách phim đang xem</title>
    <?php require_once('Layout_page/Layout_file_top.php'); ?>
    <style>
        input.form-control{
            background-color: #ffffff;
            border-radius: 10px;
            border: 1px solid rebeccapurple;
        }
    </style>
</head>
<body>
    <?php
        require_once('./Model/config.php');
        require_once('./Layout_page/Layout_header.php'); 
        require_once('./Model/action_user.php');
    ?>

    <div class="container main" style="max-width: 1240px;">
        <div class="row mt-md-5 mt-4 res-row-sm-profile">
            <div class="col-md-3 col-sm-12 section-left mx-md-0 mx-2 mb-md-0 mb-3 mr-md-0 mr-2">
                <div class="section-left-item">
                    <a href="thongtin_user.php?id_kh=<?= $id_kh ?>" class="btn btn-tab">
                        <box-icon name='user-pin' color='#fff'></box-icon>
                        <span>Thông tin khách hàng</span>
                    </a>
                </div>
                <div class="section-left-item active">
                    <a href="danhsach_xem.php?id_kh=<?= $id_kh ?>" class="btn btn-tab active">
                        <box-icon name='slideshow' color='red'></box-icon>
                        <span>Danh sách phim đang xem</span>
                    </a>
                </div>
                <div class="section-left-item">
                    <a href="lichsu_giaodich.php?id_kh=<?= $id_kh ?>" class="btn btn-tab">
                        <box-icon name='history' color='#fff'></box-icon>
                        <span>Lịch sử giao dịch</span>
                    </a>
                </div>
                <div class="section-left-item" style="padding: 0">
                    <a class="btn btn-tab" href="login.php?logout">
                        <box-icon name='log-out' rotate='180' color='#fff' id="logout"></box-icon>
                        <span>Đăng xuất</span>
                    </a>
                </div>
            </div>
        
            <div class="col-md-9 col-sm-12 col-sm-modal-12 section-right mx-2">
                <div class="row">
                    <!-- MOVIE ITEM -->
                    <?php
                        $id_kh = $_SESSION['ID_User'];

                        $query = "SELECT * FROM phim, ds_daxem WHERE phim.ID_phim = ds_daxem.ID_Phim AND ds_daxem.ID_KH = '$id_kh'";
                        $result = $conn->query($query);

                        if(!$result) echo 'Câu truy vấn bị lỗi';

                        if($result->num_rows != 0){
                            while($row = $result->fetch_array()) { ?>
                                <div class="col-md-3 col-sm-6 mb-2 mt-2">
                                    <a href="details.php?id_phim=<?= $row['ID_phim'] ?>" class="movie-item">
                                        <img src="./images/<?= $row['Hinh'] ?>" alt="">
                                        <div class="movie-item-content">
                                            <div class="movie-item-title mx-3">
                                                <?= $row['Ten_phim'] ?>
                                            </div>
                                            <div class="movie-infos mx-3">
                                                <div class="movie-info">
                                                    <i class="bx bxs-star"></i>
                                                    <span>9.5</span>
                                                </div>
                                                <div class="movie-info">
                                                    <i class="bx bxs-time"></i>
                                                    <span><?= $row['Thoi_luong'] ?></span>
                                                </div>
                                                <div class="movie-info">
                                                    <span>HD</span>
                                                </div>
                                                <div class="movie-info">
                                                    <span>16+</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php }
                        }
                    ?>
                    <!-- END MOVIE ITEM -->
                </div>
            </div>
        </div>
    </div>

    <?php require_once('Layout_page/Layout_footer.php'); ?>
</body>
</html>