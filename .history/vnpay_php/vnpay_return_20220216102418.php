<?php
require_once("../Model/config.php");
require_once("./config.php");
if (!isset($_SESSION)) {
    session_start();
}
$vnp_SecureHash = $_GET['vnp_SecureHash'];
$inputData = array();
foreach ($_GET as $key => $value) {
    if (substr($key, 0, 4) == "vnp_") {
        $inputData[$key] = $value;
    }
}

unset($inputData['vnp_SecureHash']);
ksort($inputData);
$i = 0;
$hashData = "";
foreach ($inputData as $key => $value) {
    if ($i == 1) {
        $hashData = $hashData . '&' . urlencode($key) . "=" . urlencode($value);
    } else {
        $hashData = $hashData . urlencode($key) . "=" . urlencode($value);
        $i = 1;
    }
}

$secureHash = hash_hmac('sha512', $hashData, $vnp_HashSecret);

if ($secureHash == $vnp_SecureHash) {
    if ($_GET['vnp_ResponseCode'] == '00') {
        $order_id = $_GET['vnp_TxnRef'];
        $money = $_GET['vnp_Amount'] / 100;
        $note = $_GET['vnp_OrderInfo'];
        $vnp_response_code = $_GET['vnp_ResponseCode'];
        $code_vnpay = $_GET['vnp_TransactionNo'];
        $code_bank = $_GET['vnp_BankCode'];
        $time = $_GET['vnp_PayDate'];
        $date_time = substr($time, 0, 4) . '-' . substr($time, 4, 2) . '-' . substr($time, 6, 2) . ' ' . substr($time, 8, 2) . ' ' . substr($time, 10, 2) . ' ' . substr($time, 12, 2);
        include("../Model/config.php");
        $user = $_GET['vnp_username'];
        $sql = "SELECT * FROM payments WHERE order_id = '$order_id'";
        $query = mysqli_query($conn, $sql);
        $row = mysqli_num_rows($query);

        if ($row > 0) {
            $sql = "UPDATE payments SET order_id = '$order_id', money = '$money', note = '$note', vnp_response_code = '$vnp_response_code', code_vnpay = '$code_vnpay', code_bank = '$code_bank' WHERE order_id = '$order_id'";

            $conn->query($sql);
        } else {
            $sql = "INSERT INTO payments(order_id, thanh_vien, money, note, vnp_response_code, code_vnpay, code_bank, time) VALUES ('$order_id', '$user', '$money', '$note', '$vnp_response_code', '$code_vnpay', '$code_bank','$date_time')";
            $conn->query($sql);
        }

        $_SESSION['thongbao'] = 'Đặt vé thành công!';
        $_SESSION['title'] = 'Thành công!';
        $_SESSION['type'] = 'success';
        // header('Location: ../dat_ve.php');
    } else {
        // khi giao dịch thất bại thì sẽ xóa hóa đơn của khách hàng
        $order_id = $_GET['vnp_TxnRef'];
        // lấy ra ID_Dat dựa vào ID_HD (order_id) 
        $sql_madv = "SELECT dat_ve.ID_Dat FROM hoa_don, dat_ve WHERE hoa_don.ID_Dat = dat_ve.ID_Dat AND ID_HD = '$order_id'";
        $result = $conn->query($sql_madv);
        $row = $result->fetch_assoc();
        $id_dv = $row['ID_Dat'];

        $sql_hd = "DELETE FROM hoa_don WHERE ID_HD = '$order_id'";
        $sql_dv = "DELETE FROM dat_ve WHERE ID_Dat = '$id_dv'";
        if ($conn->query($sql_hd) === TRUE && $conn->query($sql_dv) === TRUE) {
            $_SESSION['thongbao'] = 'Đặt vé thất bại!';
            $_SESSION['type'] = 'error';
            $_SESSION['title'] = 'Thất bại!';
            header('Location: ../dat_ve.php');
        }
    }
} else {
    $_SESSION['thongbao'] = 'Chữ ký không hợp lệ!';
    $_SESSION['type'] = 'error';
    $_SESSION['title'] = 'Thất bại!';
    header('Location: ../dat_ve.php');
}
