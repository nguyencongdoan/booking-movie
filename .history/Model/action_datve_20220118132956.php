<?php
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    $date = date('Y-m-d H:i:s'); 
    $id_dv = "";
    $ngaydat = "";
    $soluong = 1;
    $id_kh = "";
    $id_phim = "";
    $id_rap = "";
    $id_sc = "";
    $tenrap = "";
    $tensc = "";
    $tenphim = "";
    $id_hd = "";
    $phuong_thuc_tt = "";
    $id_phong = "";

    if(isset($_POST['redirect'])){
        $ngaydat = $date;
        $soluong = $_POST['soluong'];
        $id_kh = $_POST['id_kh'];
        $id_rap = $_POST['id_rap'];
        $id_phong = $_POST['id_phong'];     

        if(isset($_POST['id_phim'])){
            $id_phim = $_POST['id_phim'];
        }

        if(isset($_POST['id_sc'])){
            $id_sc = $_POST['id_sc'];
        }
        
        if(isset($_POST['ds_ghe'])){
            $ds_ghe = $_POST['ds_ghe'];
            $ds = implode(', ', $ds_ghe);
        } 
        
        // xử lý để lấy ra tên rạp phim
        if(isset($id_rap)){
            $query = "SELECT * FROM rap_phim WHERE ID_Rap = '$id_rap'";
            $result = $conn->query($query);
            if(!$result) echo "Câu truy vấn bị lỗi";
            $row = $result->fetch_assoc();
            $tenrap = $row['Ten_rap'];
        }

        // xử lý để lấy ra tên phòng
        if(isset($id_phong) && $id_phong != ''){
            $query = "SELECT * FROM phong_chieu WHERE ID_phong = '$id_phong'";
            $result = $conn->query($query);
            if(!$result) echo "Câu truy vấn bị lỗi";
            $row = $result->fetch_assoc();
            $tenphong = $row['Ten_phong'];
        }

        // xử lý để lấy ra thông tin suất chiếu
        if(isset($id_sc) && $id_sc != ''){
            $query = "SELECT * FROM suat_chieu WHERE ID_SC = '$id_sc'";
            $result = $conn->query($query);
            if(!$result) echo "Câu truy vấn bị lỗi";
            $row = $result->fetch_assoc();
            $tensc = $row['Gio_BD'] .' ~ '.$row['Gio_KT'].' '.$row['Ngay_chieu'];
        }

        // xử lý để lấy ra tên phim 
        if(isset($id_phim) && $id_phim != ''){
            $query = "SELECT * FROM phim WHERE ID_phim = '$id_phim'";
            $result = $conn->query($query);
            if(!$result) echo "Câu truy vấn bị lỗi";
            $row = $result->fetch_assoc();
            $tenphim = $row['Ten_phim'];
        }

        // Kiểm tra nếu chưa chọn thông tin suất chiếu hoặc thông tin rạp phim mà bấm nút đặt vé thì thông báo
        if(empty($id_sc) || empty($id_phim) || empty($id_phong))
        {
            $_SESSION['thongbao'] = "Vui lòng chọn đầy đủ thông tin trước khi đặt vé!";
            $_SESSION['type'] = "error";
            $_SESSION['title'] = 'Thất bại!';
        } 
        else if(empty($ds_ghe)){ // Kiểm tra nếu chưa chọn vị trí ngồi mà đặt vé thì thông báo
            $_SESSION['thongbao'] = "Vui lòng chọn vị trí ghế ngồi trước khi đặt vé!";
            $_SESSION['type'] = "error";
            $_SESSION['title'] = 'Thất bại!';
        }
        else{
            // Kiểm tra nếu số lượng ghế chọn nhỏ hơn số lượng đặt mua hoặc số lượng ghế chọn lớn hơn số lượng đặt mua thì thông báo
            if(count($ds_ghe) < $soluong || count($ds_ghe) > $soluong){
                $_SESSION['thongbao'] = "Vui lòng chọn đủ số lượng ghế!";
                $_SESSION['type'] = "error";
                $_SESSION['title'] = 'Thất bại!';
            }
            else{ // ngược lại thì tiến hành đặt vé
                $query = "INSERT INTO dat_ve (So_luong, Ngay_dat, List_ghe, ID_KH, ID_Phim, ID_SC, ID_Rap, ID_Phong) VALUES ('$soluong', '$ngaydat', '$ds', '$id_kh', '$id_phim', '$id_sc', '$id_rap', '$id_phong')";
  
                if($conn->query($query) === TRUE) {
                    $max_id_dv = "SELECT MAX(ID_Dat) FROM dat_ve";
                    $result = $conn->query($max_id_dv);
                    if(!$result) echo "Câu truy vấn bị lỗi";
                    $row = $result->fetch_assoc();
                    $id_dv = $row['MAX(ID_Dat)'];
                    
                    $query_hd = "INSERT INTO hoa_don (ID_KH, ID_NV, ID_Dat, Phuong_thuc_tt, Tinh_trang) VALUES ('$id_kh', '', '$id_dv', 'Thanh toán Online', 0)";
                   
                    if($conn->query($query_hd) === TRUE) {
                        $max_id_hd = "SELECT MAX(ID_HD) FROM hoa_don";
                        $result = $conn->query($max_id_hd);
                        if(!$result) echo "Câu truy vấn bị lỗi";
                        $row = $result->fetch_assoc();
                        $id_hd = $row['MAX(ID_HD)'];

                        // Lấy đơn giá thông qua bảng ve_ban
                        // $query_price = "SELECT * FROM ve_ban, suat_chieu WHERE ve_ban.ID_SC = suat_chieu.ID_SC AND suat_chieu.ID_SC = '$id_sc'";
                        // $result_price = $conn->query($query_price);
                        // if(!$result_price) echo 'Cau truy van bi loi';
                        // $row_price = $result_price->fetch_assoc();
                        // $giave = $row_price['Gia_ve'];
                        
                        $total = $soluong * 45000;
                        $query_cthd = "INSERT INTO chi_tiet_hd (ID_HD, Ngay_dat, So_luong, List_ghe, Tong_tien, ID_SC, ID_Phim, ID_Rap, ID_Phong) VALUES ('$id_hd', '$ngaydat', '$soluong', '$ds', '$total', '$id_sc', '$id_phim', '$id_rap', '$id_phong')";
                        
                        if($conn->query($query_cthd) === TRUE) {

                            // Tich hop thanh toan vnpay
                            error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
                            date_default_timezone_set('Asia/Ho_Chi_Minh');

                            /**
                             * Description of vnpay_ajax
                             *
                             * @author xonv
                             */
                            require_once("./vnpay_php/config.php");

                            $vnp_TxnRef = $id_hd; // $_POST['order_id']; // Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
                            $vnp_OrderInfo = "Đặt vé xem phim"; //$_POST['order_desc']; // Nội dung thanh toán
                            $vnp_OrderType =  "billpayment"; // $_POST['order_type']; // loại hóa đơn "billpayment" là thanh toán hóa đơn
                            $vnp_Amount = $total * 100; // $_POST['amount'] * 100; // đơn giá cần phải thanh toán của đơn hàng
                            $vnp_Locale = "vn"; // $_POST['language']; // lựa chọn ngôn ngữ Có 2 loại Việt Nam: "vn", English: en
                            $vnp_BankCode = "NCB"; // $_POST['bank_code']; // danh sách ngân hàng được VNPAY hỗ trợ. Tuy nhiên do demo thì VNPAY chỉ hỗ trợ cho ngân hàng NCB
                            $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];

                            $inputData = array(
                                "vnp_Version" => "2.1.0",
                                "vnp_TmnCode" => $vnp_TmnCode,
                                "vnp_Amount" => $vnp_Amount,
                                "vnp_Command" => "pay",
                                "vnp_CreateDate" => date('YmdHis'),
                                "vnp_CurrCode" => "VND",
                                "vnp_IpAddr" => $vnp_IpAddr,
                                "vnp_Locale" => $vnp_Locale,
                                "vnp_OrderInfo" => $vnp_OrderInfo,
                                "vnp_OrderType" => $vnp_OrderType,
                                "vnp_ReturnUrl" => $vnp_Returnurl,
                                "vnp_TxnRef" => $vnp_TxnRef,
                            );

                            if (isset($vnp_BankCode) && $vnp_BankCode != "") {
                                $inputData['vnp_BankCode'] = $vnp_BankCode;
                            }

                            ksort($inputData);
                            $query = "";
                            $i = 0;
                            $hashdata = "";
                            foreach ($inputData as $key => $value) {
                                if ($i == 1) {
                                    $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
                                } else {
                                    $hashdata .= urlencode($key) . "=" . urlencode($value);
                                    $i = 1;
                                }
                                $query .= urlencode($key) . "=" . urlencode($value) . '&';
                            }

                            $vnp_Url = $vnp_Url . "?" . $query;
                            if (isset($vnp_HashSecret)) {
                                $vnpSecureHash =   hash_hmac('sha512', $hashdata, $vnp_HashSecret);//  
                                $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
                            }
                            $returnData = array('code' => '00'
                                , 'message' => 'success'
                                , 'data' => $vnp_Url);
                                if (isset($_POST['redirect'])) {
                                    header('Location: ' . $vnp_Url);
                                    die();
                                } else {
                                    echo json_encode($returnData);
                                }
                            
                        }
                        else {
                            $_SESSION['thongbao'] = "Thêm thông tin chi tiết hóa đơn thất bại!";
                            $_SESSION['type'] = "error";
                            $_SESSION['title'] = 'Thất bại!';
                        }
                    }
                    else {
                        $_SESSION['thongbao'] = "Thêm thông tin hóa đơn thất bại!";
                        $_SESSION['type'] = "error";
                        $_SESSION['title'] = 'Thất bại!';
                    }
                }
                else {
                    $_SESSION['thongbao'] = "Thêm thông tin thất bại!";
                    $_SESSION['type'] = "error";
                    $_SESSION['title'] = 'Thất bại!';
                }
            }
        }
    }

?>