<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thông tin khách hàng</title>
    <?php require_once('Layout_page/Layout_file_top.php'); ?>
    <style>
        input.form-control{
            background-color: #ffffff;
            border-radius: 10px;
            border: 1px solid rebeccapurple;
        }
    </style>
</head>
<body>
    <?php
        require_once('./Model/config.php');
        require_once('./Layout_page/Layout_header.php'); 
        require_once('./Model/action_user.php');
    ?>

    <div class="container main" style="max-width: 1240px;">
        <div class="row mt-md-5 mt-4 res-row-sm-profile">
            <div class="col-md-3 col-sm-12 section-left mx-md-0 mx-2 mb-md-0 mb-3 mr-md-0 mr-2">
                <div class="section-left-item active">
                    <a href="thongtin_user.php?id_kh=<?= $id_kh ?>" class="btn btn-tab active">
                        <box-icon name='user-pin' color='red'></box-icon>
                        <span>Thông tin khách hàng</span>
                    </a>
                </div>
                <div class="section-left-item">
                    <a href="danhsach_xem.php?id_kh=<?= $id_kh ?>" class="btn btn-tab">
                        <box-icon name='slideshow' color='#fff'></box-icon>
                        <span>Danh sách phim đang xem</span>
                    </a>
                </div>
                <div class="section-left-item">
                    <a href="lichsu_giaodich.php?id_kh=<?= $id_kh ?>" class="btn btn-tab">
                        <box-icon name='history' color='#fff'></box-icon>
                        <span>Lịch sử giao dịch</span>
                    </a>
                </div>
                <div class="section-left-item" style="padding: 0">
                    <a class="btn btn-tab" href="login.php?logout">
                        <box-icon name='log-out' rotate='180' color='#fff' id="logout"></box-icon>
                        <span>Đăng xuất</span>
                    </a>
                </div>
            </div>
        
            <div class="col-md-9 col-sm-12 col-sm-modal-12 section-right mx-2">
                <div class="row">
                    <div class="col-md-12" style="display:flex; justify-content:end;">
                        <button class="btn btn-profile" type="button" data-bs-toggle="modal" data-bs-target="#chinhsua">
                            <box-icon name='pencil' type='solid' color='#fff'></box-icon>
                            <span class="mx-2">Sửa thông tin</span>
                        </button>
                    </div>
                    <div class="col-md-12" style="display:flex; justify-content:center;">
                        <img src="./images/<?= $hinh ?>" class="img-profile" alt="">
                    </div>
                    <div class="section-right-item col-md-6 col-sm-6">
                        <div class="row row-section-nd mt-md-0 mt-3">
                            <div class="col-md-1">
                                <box-icon name='user-pin' color='#fff'></box-icon>
                            </div>
                            <div class="col-md-11">
                                <p class="fw-bold nd-p">Họ tên</p>
                                <p class="text-2"><?= $hoten ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="section-right-item col-md-6 col-sm-6">
                        <div class="row row-section-nd mt-md-0 mt-3">
                            <div class="col-md-1">
                                <box-icon name='mobile-alt' color='#fff'></box-icon>
                            </div>
                            <div class="col-md-11">
                                <p class="fw-bold nd-p">Số điện thoại</p>
                                <p class="text-2"><?= $sdt ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="section-right-item col-md-6 col-sm-6">
                        <div class="row row-section-nd mt-md-0 mt-3">
                            <div class="col-md-1">
                                <box-icon name='map' color='#fff'></box-icon>
                            </div>
                            <div class="col-md-11">
                                <p class="fw-bold nd-p">Địa chỉ</p>
                                <p class="text-2"><?= $diachi ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="section-right-item col-md-6 col-sm-6">
                        <div class="row row-section-nd mt-md-0 mt-3">
                            <div class="col-md-1">
                                <?php
                                    if($gioitinh == 'Nam' || $gioitinh == 1){
                                        echo "<box-icon name='male' color='#fff'></box-icon>";
                                        $gioitinh = 'Nam';
                                    }
                                    else
                                        echo "<box-icon name='female' color='#fff'></box-icon>";
                                ?>
                            </div>
                            <div class="col-md-11">
                                <p class="fw-bold nd-p">Giới tính</p>
                                <p class="text-2"><?= $gioitinh ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="section-right-item col-md-6 col-sm-6">
                        <div class="row row-section-nd mt-md-0 mt-3">
                            <div class="col-md-1">
                                <box-icon name='envelope' color='#fff'></box-icon>
                            </div>
                            <div class="col-md-11">
                                <p class="fw-bold nd-p">Email</p>
                                <p class="text-2"><?= $email ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="section-right-item col-md-6 col-sm-6">
                        <div class="row row-section-nd mt-md-0 mt-3">
                            <div class="col-md-1">
                                <box-icon type='solid' name='lock' color='#fff'></box-icon>
                            </div>
                            <div class="col-md-9">
                                <p class="fw-bold nd-p">Password</p>
                                <p class="text-2">********</p>
                            </div>
                            <div class="col-md-1">
                                <button class="btn btn-pencil"type="button" data-bs-toggle="modal" data-bs-target="#changePassword">
                                    <box-icon name='pencil' type='solid' color='red'></box-icon>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal chỉnh sửa thông tin user -->
    <div class="modal fade" id="chinhsua">
        <div class="modal-dialog modal-lg">
          <div class="modal-content" style="background: #ffffff;">
            <div class="container-fluid">
                <form method="post" action="" enctype="multipart/form-data">
                    <div class="row">
                        <button class="btn btn-x ml-auto toggler-sm-icon" data-bs-dismiss="modal">
                            <box-icon class="toggler-sm-icon" name='x-circle' color='rebeccapurple'></box-icon>
                        </button>

                        <div class="col-md-4 col-sm-12 modal-left bg-sm-none">
                            <label for="file-img" class="label-modal-icon">
                                <i class='bx bx-plus' style='color: rebeccapurple; font-size: 60px; background: #ffffff; border: none;'></i>
                            </label>
                            <input type="file" id="file-img" name="image" value="" style="display: none;">
                        </div>
                        
                        <div class="col-md-7 col-sm-12 mx-0 mx-md-4 mt-2 modal-right">
                            <button class="btn btn-x ml-auto toggler-lg-icon" data-bs-dismiss="modal">
                                <box-icon name='x-circle' color='rebeccapurple' ></box-icon>
                            </button>
                            <h1 class="text-center fw-bold" style="color: rebeccapurple">Thay đổi thông tin</h1>
                            <div class="row modal-profile mt-4">
                                <input type="hidden" name="id_kh" value="<?= $id_kh ?>" />
                                <div class="col-md-6 col-sm-12">
                                    <span class="fw-bold">Họ tên</span>
                                    <input type="text" name="hoten" class="form-control mt-1" value="<?= $hoten ?>">
                                </div>
                                <div class="col-md-6 col-sm-12 mt-md-0 mt-3">
                                    <span class="fw-bold">Số điện thoại</span>
                                    <input type="text" name="sdt" class="form-control mt-1" value="<?= $sdt ?>">
                                </div>
                                <div class="col-md-6 col-sm-12 mt-3">
                                    <span class="fw-bold">Email</span>
                                    <input type="text" name="email" class="form-control mt-1" value="<?= $email ?>">
                                </div>
                                <div class="col-md-6 col-sm-12 mt-3">
                                    <span class="fw-bold">Địa chỉ</span>
                                    <input type="text" name="diachi" class="form-control mt-1" value="<?= $diachi ?>">
                                </div>
                                <div class="col-md-12 col-sm-12 mt-3" style="display:flex; justify-content:center;">
                                    <label class="fw-bold">Giới tính</label>
                                    <div class="form-check mx-2">
                                        <input class="form-check-input" type="radio" name="gioitinh" value="1" id="radio-nam" <?php if($gioitinh == 'Nam') echo 'checked'; ?> >
                                        <label class="form-check-label-gt mx-1" for="radio-nam">Nam</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="0" name="gioitinh" id="radio-nu" <?php if($gioitinh == 'Nữ') echo 'checked'; ?>>
                                        <label class="form-check-label-gt mx-1" for="radio-nu">Nữ</label>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 mt-4" style="display:flex; justify-content:center;">
                                    <input class="btn btn-info" type="submit" name="capnhat" value="Cập nhật">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
  
    <!-- End modal chỉnh sửa thông tin user -->

    <!-- Modal change password -->
    <div class="modal fade" id="changePassword">
        <div class="modal-dialog modal-lg" style="position: relative;">
            <div class="modal-content" style="background: #ffffff;">
                <button class="btn btn-x-modal" data-bs-dismiss="modal">
                    <i class='bx bx-x-circle' style='color:rebeccapurple; font-size: 26px;'></i>
                </button>
                <div class="container-fluid" style="width: 500px;">
                    <form action="" method="post">
                        <input type="hidden" name="id_kh" value="<?= $id_kh ?>" />
                        <div class="row modal-changePassword">
                            <h1 class="text-center fw-bold">Thay đổi mật khẩu</h1>
                            <?php if(isset($thongbao)) echo $thongbao; ?>
                            <div class="col-md-8 col-sm-12 modal-pass-item mt-3">
                                <label class="fw-bold">Mật khẩu hiện tại</label>
                                <input type="password" name="password" value="<?= $password ?>" class="form-control mt-1">
                            </div>
                            <div class="col-md-8 col-sm-12 mt-md-3 mt-3 modal-pass-item">
                                <label class="fw-bold">Mật khẩu mới</label>
                                <input type="password" name="passnew" value="<?= $passnew ?>" class="form-control mt-1 modal-pass-item">
                            </div>
                            <div class="col-md-8 col-sm-12 mt-3">
                                <label class="fw-bold">Nhập lại mật khẩu</label>
                                <input type="password" name="passconf" value="<?= $passconf ?>" class="form-control mt-1 modal-pass-item">
                            </div>
                            <div class="col-md-12 col-sm-12 mt-4" style="display:flex; justify-content:center;">
                                <input class="btn btn-info" type="submit" name="changePassword" value="Thay đổi">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End modal change password -->

    <?php require_once('Layout_page/Layout_footer.php'); ?>
</body>
</html>