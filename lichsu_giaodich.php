<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lịch sử giao dịch</title>
    <?php require_once('Layout_page/Layout_file_top.php'); ?>
    <style>
        input.form-control{
            background-color: #ffffff;
            border-radius: 10px;
            border: 1px solid rebeccapurple;
        }

        td{
            padding: 10px;
        }

        th{
            color: #3ec461;
        }

        tr:nth-child(even){
            background-color: #272727;
        }
    </style>
</head>
<body>
    <?php
        require_once('./Model/config.php');
        require_once('./Layout_page/Layout_header.php'); 
        require_once('./Model/action_user.php');
    ?>

    <div class="container main" style="max-width: 1240px;">
        <div class="row mt-md-5 mt-4 res-row-sm-profile">
            <div class="col-md-3 col-sm-12 section-left mx-md-0 mx-2 mb-md-0 mb-3 mr-md-0 mr-2">
                <div class="section-left-item">
                    <a href="thongtin_user.php?id_kh=<?= $id_kh ?>" class="btn btn-tab">
                        <box-icon name='user-pin' color='#fff'></box-icon>
                        <span>Thông tin khách hàng</span>
                    </a>
                </div>
                <div class="section-left-item">
                    <a href="danhsach_xem.php?id_kh=<?= $id_kh ?>" class="btn btn-tab">
                        <box-icon name='slideshow' color='#fff'></box-icon>
                        <span>Danh sách phim đang xem</span>
                    </a>
                </div>
                <div class="section-left-item active">
                    <a href="lichsu_giaodich.php?id_kh=<?= $id_kh ?>" class="btn btn-tab active">
                        <box-icon name='history' color='red'></box-icon>
                        <span>Lịch sử giao dịch</span>
                    </a>
                </div>
                <div class="section-left-item" style="padding: 0">
                    <a class="btn btn-tab" href="login.php?logout">
                        <box-icon name='log-out' rotate='180' color='#fff' id="logout"></box-icon>
                        <span>Đăng xuất</span>
                    </a>
                </div>
            </div>
        
            <div class="col-md-9 col-sm-12 col-sm-modal-12 section-right mx-2">
                <div class="row">
                    <?php
                        if(isset($_GET['id_kh'])){
                            $id_kh = $_GET['id_kh'];
                            $query = "SELECT * FROM hoa_don, chi_tiet_hd, phim, rap_phim, phong_chieu, suat_chieu WHERE hoa_don.ID_HD = chi_tiet_hd.ID_HD AND chi_tiet_hd.ID_SC = suat_chieu.ID_SC AND chi_tiet_hd.ID_Phim = phim.ID_phim AND chi_tiet_hd.ID_Rap =rap_phim.ID_Rap AND chi_tiet_hd.ID_Phong = phong_chieu.ID_Phong AND hoa_don.ID_KH = '$id_kh'";
                            $result = $conn->query($query);
                            if(!$result) echo "Câu truy vấn bị lỗi";
                        }
                    ?>
                   <table class="mt-4 mx-md-2 mx-0" cellpadding="5" style="width: 99%;">
                        <tbody style="color: #ffffff;">
                            <tr style="text-align: center;">
                            <th>Mã hóa đơn</th>
                            <th>Rạp phim</th>
                            <th>Suất chiếu</th>
                            <th>Tên phim</th>
                            <th>Vị trí ghế ngồi</th>
                            <th></th>
                        </tr>
                        <?php
                            if($result->num_rows != 0){
                                while($row = $result->fetch_array()) { ?>
                                    <tr>
                                        <td>HD<?= $row['ID_HD']; ?></td>
                                        <td><?= $row['Ten_rap']; ?></td>
                                        <td><?= $row['Gio_BD'] .' ~ '.$row['Gio_KT']; ?></td>
                                        <td><?= $row['Ten_phim']; ?></td>
                                        <td><?= $row['List_ghe']; ?></td>
                                        <td>
                                            <a href="xem_ct_hd.php?id_kh=<?= $id_kh ?>" style="color:lightgoldenrodyellow;">Xem chi tiết</a>
                                        </td>
                                    </tr>
                                <?php }
                            }
                        ?>
                        </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>

    <?php require_once('Layout_page/Layout_footer.php'); ?>
</body>
</html>