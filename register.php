<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/login.css">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <?php
        include './Model/config.php';

        if(isset($_POST['register'])){
            $hoten = $_POST['hoten'];
            $diachi = $_POST['diachi'];
            $sdt = $_POST['sdt'];
            $gioitinh = $_POST['gioitinh'];
            $email = $_POST['email'];
            $password = $_POST['password'];

            if(empty($hoten) || empty($diachi) || empty($sdt) || empty($email) || empty($password)){
                $_SESSION['thongbao'] = "Vui lòng nhập lại thông tin !";
            }
            else{
                $query = "SELECT * FROM khach_hang WHERE Email = '$email' OR Ho_ten ='$hoten'";
                $result = $conn->query($query);
                if(!$result) echo 'Cau truy van bi loi';
                $row = $result->fetch_assoc();

                if($result->num_rows == 0){
                    $pass = md5($password);
                    $query = "INSERT INTO khach_hang (Ho_ten, Hinh, Dia_chi, SDT, Gioi_tinh, So_du, ID_Quyen, Email, Passwords) VALUES ('$hoten', 'avatar.png', '$diachi', '$sdt', '$gioitinh', 0, '1', '$email', '$pass')";
                    
                    if($conn->query($query) === TRUE) {
                        $_SESSION['thongbao'] = "Đăng ký thành công";
                        // $_SESSION['type'] = "success";
                        header('Location: login.php?message=success');
                    }
                    else{
                        $_SESSION['thongbao'] = "Đăng ký thất bại";
                        // $_SESSION['type'] = "danger";
                    }
                }
                else{
                    $_SESSION['thongbao'] = "Email hoặc họ tên đã tồn tại. Vui lòng chọn Email khác!";
                }
            }
        }
    ?>
    <form action="" method="post">
        <?php
            if(isset($_SESSION['thongbao'])){
                echo "<label class='thongbao'>";
                    echo $_SESSION['thongbao'];
                echo "</label>";
                unset($_SESSION['thongbao']);
            }
        ?>

        <div id="register">
            <div class="login-form">
                <div class="form-title">
                    <h1>Đăng ký</h1>
                </div>

                <label for="">
                    <input type="text" placeholder="Họ tên" name="hoten" id="input" value="<?php if(isset($hoten)) echo $hoten; ?>">
                </label>

                <label for="">
                    <input type="text" placeholder="Địa chỉ" name="diachi" id="input" value="<?php if(isset($diachi)) echo $diachi; ?>">
                </label>

                <label for="">
                    <input type="text" placeholder="Số điện thoại" name="sdt" id="input" value="<?php if(isset($sdt)) echo $sdt; ?>">
                </label>

                <div class="form-check">
                    <div class="radio-left">
                        <input class="form-check-input" type="radio" name="gioitinh" id="radio-nam" checked>
                        <label class="form-check-label" for="radio-nam">
                            Nam
                        </label>
                    </div>
                    <div>
                        <input class="form-check-input" type="radio" name="gioitinh" id="radio-nu">
                        <label class="form-check-label" for="radio-nu">
                            Nữ
                        </label>
                    </div>
                </div>

                <label for="">
                    <input type="email" placeholder="Email" name="email" id="input" value="<?php if(isset($email)) echo $email; ?>">
                </label>

                <label for="">
                    <input type="password" placeholder="Mật khẩu" name="password" id="input" value="<?php if(isset($password)) echo $password; ?>">
                </label> 
                
                <button class="red" type="submit" name="register">
                    <i class="fa fa-user-plus"></i>&nbsp;Đăng ký
                </button>
            </div>
        </div>
    </form>
</body>

</html>