<?php
    include './Model/config.php';

    if(isset($_POST['data_id'])){
        $key = $_POST['data_id'];

        if(!isset($_GET['page'])){
            $_GET['page'] = 1;
        }
        $rowPerPage = 6;
        // vị trí của mẩu tin đầu tiên trên mỗi trang
        $offset = ($_GET['page'] - 1) * $rowPerPage;
        $query = "SELECT DISTINCT Ten_phim,Thoi_luong,phim.ID_phim,Hinh FROM phim,suat_chieu WHERE phim.ID_phim = suat_chieu.ID_Phim AND ID_TL = 1 AND Tinh_trang_chieu = 1 AND Ten_phim LIKE '%$key%' ORDER BY Luot_xem DESC LIMIT $offset, $rowPerPage";
        $result = $conn->query($query);

        if(!$result) echo 'Câu truy vấn bị lỗi';

        if($result->num_rows != 0){
            while($row = $result->fetch_array()) { ?>
                <div class="col-md-2 col-sm-6 mt-3">
                    <a href="details.php?id_phim=<?= $row['ID_phim'] ?>" class="movie-item">
                        <img src="./images/<?= $row['Hinh'] ?>" alt="">
                        <div class="movie-item-content">
                            <div class="movie-item-title mx-3">
                                <?= $row['Ten_phim'] ?>
                            </div>
                            <div class="movie-infos mx-3">
                                <div class="movie-info">
                                    <i class="bx bxs-star"></i>
                                    <span>9.5</span>
                                </div>
                                <div class="movie-info">
                                    <i class="bx bxs-time"></i>
                                    <span><?= $row['Thoi_luong'] ?></span>
                                </div>
                                <div class="movie-info">
                                    <span>HD</span>
                                </div>
                                <div class="movie-info">
                                    <span>16+</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            <?php }
        } else echo 'Chưa có phim thuộc thể loại này';
    }
?>