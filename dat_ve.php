<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đặt vé xem phim</title>
    <?php require_once('Layout_page/Layout_file_top.php'); ?>
    <!-- AJAX -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>
    <script src="./js/timkiem_datve.js"></script>
</head>
<body>
    <?php 
        require_once('./Model/Config.php');
        if(!isset($_SESSION)) 
        { 
            session_start(); 
        }
        if(!isset($_SESSION['ID_User'])){
            header('Location: login.php');
        }
        require_once('./Model/action_datve.php');
        require_once('Layout_page/Layout_header.php');

        if(isset($_GET['id_rap']) || isset($_GET['id_phim']) || isset($_GET['id_sc'])){
            if(isset($_GET['id_rap'])) $id_rap = $_GET['id_rap'];
            if(isset($_GET['id_phim'])) $id_phim = $_GET['id_phim'];
            if(isset($_GET['id_sc'])) $id_sc = $_GET['id_sc'];

            // xử lý để lấy ra tên rạp phim
            if(isset($id_rap) && $id_rap != ''){
                $query = "SELECT * FROM rap_phim WHERE ID_Rap = '$id_rap'";
                $result = $conn->query($query);
                if(!$result) echo "Câu truy vấn bị lỗi";
                $row = $result->fetch_assoc();
                $tenrap = $row['Ten_rap'];
            }

            // xử lý để lấy ra thông tin suất chiếu
            if(isset($id_sc) && $id_sc != ''){
                $query = "SELECT * FROM suat_chieu WHERE ID_SC = '$id_sc'";
                $result = $conn->query($query);
                if(!$result) echo "Câu truy vấn bị lỗi";
                $row = $result->fetch_assoc();
                $tensc = $row['Gio_BD'] .' ~ '.$row['Gio_KT'].' '.$row['Ngay_chieu'];
            }

            // xử lý để lấy ra tên phim 
            if(isset($id_phim) && $id_phim != ''){
                $query = "SELECT * FROM phim WHERE ID_phim = '$id_phim'";
                $result = $conn->query($query);
                if(!$result) echo "Câu truy vấn bị lỗi";
                $row = $result->fetch_assoc();
                $tenphim = $row['Ten_phim'];
            }
        }
        
        if(isset($_SESSION['thongbao']))
        { ?>
            <div id="toast" style="top: 65px;">
                <div class="toast toast--<?= $_SESSION['type'] ?>" style="animation: 0.3s ease 0s 1 normal none running slideInLeft, 1s linear 5s 1 normal forwards running fadeOut; z-index: 1000;">
                    <div class="toast__icon">
                        <?php
                            if($_SESSION['type'] == 'success')
                                echo "<i class='bx bxs-check-circle' ></i>";
                            else 
                                echo "<i class='bx bxs-error-circle'></i>"
                        ?>
                    </div>
                    <div class="toast__body">
                        <h3 class="toast__title"><?= $_SESSION['title']; ?></h3>
                        <p class="toast__msg"><?= $_SESSION['thongbao']; ?></p>
                    </div>
                    <div class="toast__close">
                        <i class='bx bx-x'></i>
                    </div>
                </div>
            </div>
        <?php } unset($_SESSION['thongbao']);
    ?>
    

    <!-- Main -->

    <div class="container" style="max-width: 1240px; display: flex; justify-content: center;">
        <div class="row res-row-sm">
            <h2 class="fw-bold mt-4 text-title">Đặt vé xem phim</h2>

            <?php
                if(isset($_GET['them']) || isset($_GET['sua'])){
                    if(isset($_GET['them']) && $_GET['them'] == 'success'){
                        $_SESSION['thongbao'] = "Thêm mới thành công!";
                    }
                    if(isset($_GET['sua']) && $_GET['sua'] == 'success'){
                        $_SESSION['thongbao'] = "Sửa thông tin thành công";
                    }
                    $_SESSION['type'] = 'success';
                }
            ?>
            
            <form action="" method="post" class="mt-5">
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <div class="col-md-12 col-sm-12 main-item">
                            <span class="main-span">Rạp phim</span>
                            <select class="form-select main-item-select rap_phim" name="id_rap" id="id_rap">
                                <?php
                                    $query = "SELECT * FROM rap_phim";
                                    $result = $conn->query($query);
                                    if(!$result) echo "Câu truy vấn bị lỗi";
                                    
                                    if($result->num_rows != 0) {
                                        while($row = $result->fetch_array()) { ?>
                                            <option value="<?= $row['ID_Rap']; ?>" <?php if(isset($id_rap)) if($row['ID_Rap'] == $id_rap) echo 'selected'?>><?= $row['Ten_rap']; ?></option>
                                    <?php }
                                    }
                                ?>
                            </select>
                        </div>

                        <div class="col-md-12 col-sm-12 main-item mt-4">
                            <span class="main-span">Phòng chiếu</span>
                            <select class="form-select main-item-select phong_chieu" name="id_phong" id="id_phong">
                                <?php
                                    if(isset($id_phong) && $id_phong != '') echo "<option value='$id_phong'>$tenphong</option>";
                                    else echo '<option value="" class="text-center">------------------------------------</option>';
                                ?>
                            </select>
                        </div>

                        <div class="col-md-12 col-sm-12 main-item mt-4">
                            <span class="main-span">Suất chiếu</span>
                            <select class="form-select main-item-select suat_chieu" name="id_sc" id="id_sc">
                                <?php
                                    if(isset($id_sc) && $id_sc != '') echo "<option value='$id_sc'>$tensc</option>";
                                    else echo '<option value="" class="text-center">------------------------------------</option>';
                                ?>
                            </select>
                        </div>

                        <div class="col-md-12 col-sm-12 main-item mt-4">
                            <span class="main-span">Danh sách phim</span>
                            <select class="form-select main-item-select phim" name="id_phim" id="id_phim">
                                <?php
                                    if(isset($id_phim) && $id_phim != '') echo "<option value='$id_phim'>$tenphim</option>";
                                    else echo '<option value="" class="text-center">------------------------------------</option>';
                                ?>
                            </select>
                        </div>

                        <div class="col-md-12 col-sm-12 main-item mt-4">
                            <span class="main-span">Số lượng</span>
                            <i class='bx bx-minus'></i>
                            <input type="text" class="form-control col-md-1 mx-2 bx-number" min="1" max="27" name="soluong" value="<?php if(isset($soluong)) echo $soluong; else echo 1; ?>" required>
                            <i class='bx bx-plus'></i>
                        </div>

                        <div class="col-md-12 col-sm-12 main-item mt-4">
                            <span class="main-span">Ngày đặt</span>
                            <input type="text" class="form-control main-input" name="ngay_dat" value="<?php echo $date; ?>" disabled>
                        </div>

                        <div class="col-md-12 col-sm-12 main-item mt-4">
                            <span class="main-span">Khách hàng</span>
                            <input type="hidden" class="form-control main-input" name="id_kh" value="<?php if(isset($_SESSION['ID_User'])) echo $_SESSION['ID_User']; ?>">
                            <input type="text" name="username" class="form-control main-input" value="<?php if(isset($_SESSION['ID_User'])) echo $_SESSION['HoTen']; ?>" disabled>
                        </div>

                        <div class="col-md-12 col-sm-12 mt-5">
                            <h3 class="fw-bold">Chọn vị trí ghế ngồi</h3>
                            <img src="./images/screen.png" class="mx-3" style="margin-top: -50px;"> 
                            
                            <div class="list_ghe">
                                <div class="form-check-vt" style="margin-top: -30px;">
                                    <h5 class="text-chect-h5 mx-3">A</h5>
                                    <?php
                                        for($i = 1; $i < 10; $i++) { ?>
                                            <input class="form-check-input input-check-vt" name="ds_ghe[]" id="A<?= $i ?>" type="checkbox" value="A<?= $i ?>">
                                            <label for="A<?= $i ?>" class="mx-2"></label>
                                        <?php }
                                    ?>
                                </div>

                                <div class="form-check-vt mt-5">
                                    <h5 class="text-chect-h5 mx-3">B</h5>
                                    <?php
                                        for($i = 1; $i < 10; $i++) { ?>
                                            <input class="form-check-input input-check-vt" name="ds_ghe[]" id="B<?= $i ?>" type="checkbox" value="B<?= $i ?>">
                                            <label for="B<?= $i ?>" class="mx-2"></label>
                                        <?php }
                                    ?>
                                </div>

                                <div class="form-check-vt mt-5">
                                    <h5 class="text-chect-h5 mx-3">C</h5>
                                    <?php
                                        for($i = 1; $i < 10; $i++) { ?>
                                            <input class="form-check-input input-check-vt" name="ds_ghe[]" id="C<?= $i ?>" type="checkbox" value="C<?= $i ?>">
                                            <label for="C<?= $i ?>" class="mx-2"></label>
                                        <?php }
                                    ?>
                                </div>

                                <div class="form-check-vt mt-5">
                                    <h5 class="text-chect-h5 mx-3">D</h5>
                                    <?php
                                        for($i = 1; $i < 10; $i++) { ?>
                                            <input class="form-check-input input-check-vt" name="ds_ghe[]" id="D<?= $i ?>" type="checkbox" value="D<?= $i ?>">
                                            <label for="D<?= $i ?>" class="mx-2"></label>
                                        <?php }
                                    ?>
                                </div>
                            </div>
                            
                            <div class="mt-4 mx-1" style="padding-left: 35px;"> 
                                <input class="form-check-input input-check-vt" type="checkbox">
                                <label class="mx-2">Chưa chọn</label>
                                <input class="form-check-input input-check-vt" type="checkbox" checked>
                                <label class="mx-2">Đã chọn</label>
                                <input class="form-check-input input-check-vt" type="checkbox" checked disabled>
                                <label class="mx-2">Không thể chọn</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="col-md-12 main-pay-right mt-md-0 mt-5" style="background: #202020;">
                            <h3 class="fw-bold text-center">Thông tin thanh toán</h3>
                            <div class="row">
                                <div class="col-md-12 main-pay-select mt-3">
                                    <p>Khách hàng:</p>
                                    <p class="fw-bold"><?php if(isset($_SESSION['ID_User'])) echo $_SESSION['HoTen']; ?></p>
                                </div>
                                <div class="col-md-12 main-pay-select mt-3">
                                    <p>Tên rạp phim:</p>
                                    <p class="fw-bold tenrap"><?php if(isset($tenrap) && $tenrap != '') echo $tenrap; else echo "Chưa chọn"; ?></p>
                                </div>
                                <div class="col-md-12 main-pay-select mt-3">
                                    <p>Phòng chiếu:</p>
                                    <p class="fw-bold phongchieu"><?php if(isset($tenphong) && $tenphong != '') echo $tenphong; else echo "Chưa chọn";?></p>
                                </div>
                                <div class="col-md-12 main-pay-select mt-3">
                                    <p>Suất chiếu:</p>
                                    <p class="fw-bold suatchieu"><?php if(isset($tensc) && $tensc != '') echo $tensc; else echo "Chưa chọn"; ?></p>
                                </div>
                                <div class="col-md-12 main-pay-select mt-3">
                                    <p>Tên phim:</p>
                                    <p class="fw-bold tenphim"><?php if(isset($tenphim) && $tenphim != '') echo $tenphim; else echo "Chưa chọn"; ?></p>
                                </div>
                                <div class="col-md-12 main-pay-select mt-3">
                                    <p>Số lượng:</p>
                                    <p class="fw-bold soluong">1</p>
                                </div>
                                <div class="col-md-12 main-pay-select mt-3">
                                    <p>Vị trí ghế ngồi:</p>
                                    <p class="fw-bold ds_ghe">Chưa chọn</p>
                                </div>
                                <hr class="mt-2 hr"/>
                                <div class="col-md-12 mt-2 main-pay-select mt-3">
                                    <p>Số tiền phải thanh toán:</p>
                                    <p class="fw-bold text-color total">45.000 ₫</p>
                                </div>
                                <button class="btn btn-org btn-hv mt-3" name="redirect" type="submit" >
                                    Đặt vé
                                </button>
                            </div>
                        </div>
                        <p class="fw-bold mt-2 text-center">Or</p>
                        <div class="col-md-12">
                            <div id="paypal-payment-button">
    
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="https://www.paypal.com/sdk/js?client-id=Abc35nb3SRxVNlCfOPKPFTZfVoCNhoIvxfulyeyprsQJ-CQANRRnt5N43wthCgYxmwctq-doepeLpjvw"></script>
    <script>paypal.Buttons().render('#paypal-payment-button');</script>
    <!-- End main -->
    <?php require_once('Layout_page/Layout_footer.php');  ?>
    <script src="./toast/toast.js"></script>

    <script>
        $(document).ready(function(){
            $("#toast").click(function(){
                $(this).hide();
            });
        });
    </script>
</body>
</html>