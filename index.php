<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Trang chủ
    </title>
    <?php require_once('Layout_page/Layout_file_top.php'); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"> </script>
</head>

<body>
    <?php 
        require_once('./Layout_page/Layout_header.php');  
        
        if(isset($_SESSION['message_login']))
        { ?>
            <div id="toast" style="top: 65px;">
                <div class="toast toast--<?= $_SESSION['type'] ?>" style="animation: 0.3s ease 0s 1 normal none running slideInLeft, 1s linear 5s 1 normal forwards running fadeOut; z-index: 1000;">
                    <div class="toast__icon">
                        <i class='bx bxs-check-circle' ></i>
                    </div>
                    <div class="toast__body">
                        <h3 class="toast__title">Thành công!</h3>
                        <p class="toast__msg"><?= $_SESSION['message_login']; ?></p>
                    </div>
                    <div class="toast__close">
                        <i class='bx bx-x'></i>
                    </div>
                </div>
            </div>
        <?php } unset($_SESSION['message_login']);
    ?>
    
    <!-- HERO SECTION -->
    <div class="hero-section">
        <!-- HERO SLIDE -->
        <div class="hero-slide">
            <div class="owl-carousel carousel-nav-center" id="hero-carousel">
                <!-- SLIDE ITEM -->
                <div class="hero-slide-item">
                    <img src="./images/black-banner.png" alt="">
                    <div class="overlay"></div>
                    <div class="hero-slide-item-content">
                        <div class="item-content-wraper">
                            <div class="item-content-title top-down">
                                Black Panther
                            </div>
                            <div class="movie-infos mx-3 top-down delay-2">
                                <div class="movie-info">
                                    <i class="bx bxs-star"></i>
                                    <span>9.5</span>
                                </div>
                                <div class="movie-info">
                                    <i class="bx bxs-time"></i>
                                    <span>120 mins</span>
                                </div>
                                <div class="movie-info">
                                    <span>HD</span>
                                </div>
                                <div class="movie-info">
                                    <span>16+</span>
                                </div>
                            </div>
                            <div class="item-content-description top-down delay-4 text-break">
                                “Black Panther” creates wonder with great flair and feeling partly through something Hollywood 
                                rarely dreams of anymore: myth. Most big studio fantasies take you out for a joy ride only to 
                                hit the same exhausted story and franchise-expanding beats. Not this one. Its axis point is the
                                fantastical nation of Wakanda, an African Eden where verdant-green landscapes meet blue-sky science
                                fiction. There, spaceships with undercarriages resembling tribal masks soar over majestic waterfalls, 
                                touching down in a story that has far more going for it than branding.
                            </div>
                            <div class="item-action top-down delay-6">
                                <a href="details.php" class="btn btn-hover">
                                    <i class="bx bxs-right-arrow"></i>
                                    <span>Xem ngay</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END SLIDE ITEM -->
                <!-- SLIDE ITEM -->
                <div class="hero-slide-item">
                    <img src="./images/supergirl-banner.jpg" alt="">
                    <div class="overlay"></div>
                    <div class="hero-slide-item-content">
                        <div class="item-content-wraper">
                            <div class="item-content-title top-down">
                                Supergirl
                            </div>
                            <div class="movie-infos mx-3 top-down delay-2">
                                <div class="movie-info">
                                    <i class="bx bxs-star"></i>
                                    <span>9.5</span>
                                </div>
                                <div class="movie-info">
                                    <i class="bx bxs-time"></i>
                                    <span>120 mins</span>
                                </div>
                                <div class="movie-info">
                                    <span>HD</span>
                                </div>
                                <div class="movie-info">
                                    <span>16+</span>
                                </div>
                            </div>
                            <div class="item-content-description top-down delay-4">
                                Supergirl is one of the most popular and oldest DC female characters since her 
                                first story's publication in 1959, starring in magazines as well as solo books. 
                                Superman's cousin Kara Zor-El is the most popular and most iconic version, but her 
                                character has been reinvented several times and replaced for a while.
                            </div>
                            <div class="item-action top-down delay-6">
                                <a href="details.php" class="btn btn-hover">
                                    <i class="bx bxs-right-arrow"></i>
                                    <span>Xem ngay</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END SLIDE ITEM -->
                <!-- SLIDE ITEM -->
                <div class="hero-slide-item">
                    <img src="./images/wanda-banner.jpg" alt="">
                    <div class="overlay"></div>
                    <div class="hero-slide-item-content">
                        <div class="item-content-wraper">
                            <div class="item-content-title top-down">
                                Wanda Vision
                            </div>
                            <div class="movie-infos mx-3 top-down delay-2">
                                <div class="movie-info">
                                    <i class="bx bxs-star"></i>
                                    <span>9.5</span>
                                </div>
                                <div class="movie-info">
                                    <i class="bx bxs-time"></i>
                                    <span>120 mins</span>
                                </div>
                                <div class="movie-info">
                                    <span>HD</span>
                                </div>
                                <div class="movie-info">
                                    <span>16+</span>
                                </div>
                            </div>
                            <div class="item-content-description top-down delay-4">
                                WandaVision sẽ là loạt phim khai thác tất cả mọi điều về quá khứ của Scarlet Witch 
                                và lý do tại sao cô lại có được năng lực của mình. Elizabeth Olsen đã đạt được sự 
                                công nhận với vai diễn Wanda Maximoff / Scarlet Witch trong các bộ phim siêu anh hùng 
                                của Vũ trụ Điện ảnh Marvel, bao gồm Avengers: Age of Ultron (2015), Captain America: 
                                Civil War (2016), Avengers: Infinity War (2018) và Avengers: Endgame (2019).
                            </div>
                            <div class="item-action top-down delay-6">
                                <a href="details.php" class="btn btn-hover">
                                    <i class="bx bxs-right-arrow"></i>
                                    <span>Xem ngay</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END SLIDE ITEM -->
            </div>
        </div>
        <!-- END HERO SLIDE -->
    </div>
    <!-- END HERO SECTION -->

    <!-- LATEST MOVIES SECTION -->
    <div class="section">
        <div class="container">
            <div class="section-header">
                Phim đang chiếu
            </div>
            <div class="movies-slide carousel-nav-center owl-carousel">
                <!-- MOVIE ITEM -->
                <?php
                    $query = 'SELECT DISTINCT Ten_phim,Thoi_luong,phim.ID_phim,Hinh FROM phim,suat_chieu WHERE phim.ID_phim = suat_chieu.ID_Phim AND ID_TL = 1 AND Tinh_trang_chieu = 1 ORDER BY Luot_xem DESC';
                    $result = $conn->query($query);

                    if(!$result) echo 'Câu truy vấn bị lỗi';

                    if($result->num_rows != 0){
                        while($row = $result->fetch_array()) { ?>
                            <a href="details.php?id_phim=<?= $row['ID_phim'] ?>" class="movie-item">
                                <img src="./images/<?= $row['Hinh'] ?>" alt="">
                                <div class="movie-item-content">
                                    <div class="movie-item-title mx-3">
                                        <?= $row['Ten_phim'] ?>
                                    </div>
                                    <div class="movie-infos mx-3">
                                        <div class="movie-info">
                                            <i class="bx bxs-star"></i>
                                            <span>9.5</span>
                                        </div>
                                        <div class="movie-info">
                                            <i class="bx bxs-time"></i>
                                            <span><?= $row['Thoi_luong'] ?></span>
                                        </div>
                                        <div class="movie-info">
                                            <span>HD</span>
                                        </div>
                                        <div class="movie-info">
                                            <span>16+</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                    }
                ?>
                <!-- END MOVIE ITEM -->
            </div>
        </div>
    </div>
    <!-- END LATEST MOVIES SECTION -->

    <!-- LATEST SERIES SECTION -->
    <div class="section">
        <div class="container">
            <div class="section-header">
                Top phim thịnh hành
            </div>
            <div class="movies-slide carousel-nav-center owl-carousel">
                <!-- MOVIE ITEM -->
                <?php 
                    $query = 'SELECT * FROM phim WHERE ID_TL != 1 AND ID_TL != 6 ORDER BY Luot_xem DESC';
                    $result = $conn->query($query);

                    if(!$result) echo 'Câu truy vấn bị lỗi';

                    if($result->num_rows != 0){
                        while($row = $result->fetch_array()) { ?>
                            <a href="details.php?id_phim=<?= $row['ID_phim'] ?>" class="movie-item">
                                <img src="./images/<?= $row['Hinh'] ?>" alt="">
                                <div class="movie-item-content">
                                    <div class="movie-item-title mx-3">
                                        <?= $row['Ten_phim'] ?>
                                    </div>
                                    <div class="movie-infos mx-3">
                                        <div class="movie-info">
                                            <i class="bx bxs-star"></i>
                                            <span>9.5</span>
                                        </div>
                                        <div class="movie-info">
                                            <i class="bx bxs-time"></i>
                                            <span><?= $row['Thoi_luong'] ?></span>
                                        </div>
                                        <div class="movie-info">
                                            <span>HD</span>
                                        </div>
                                        <div class="movie-info">
                                            <span>16+</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                       <?php }
                    }
                ?>
                <!-- END MOVIE ITEM -->
            </div>
        </div>
    </div>
    
    <!-- END LATEST SERIES SECTION -->

    <!-- LATEST CARTOONS SECTION -->
    <div class="section">
        <div class="container">
            <div class="section-header">
               Top Anime
            </div>
            <div class="movies-slide carousel-nav-center owl-carousel">
                <!-- MOVIE ITEM -->
                
                <?php
                    $query = 'SELECT * FROM phim WHERE ID_TL = 6 ORDER BY Luot_xem DESC';
                    $result = $conn->query($query);

                    if(!$result) echo 'Câu truy vấn bị lỗi';

                    if($result->num_rows != 0) {
                        while($row = $result->fetch_array()) { ?>
                            <a href="details.php?id_phim=<?= $row['ID_phim'] ?>" class="movie-item">
                                <img src="./images/<?= $row['Hinh'] ?>" alt="">
                                <div class="movie-item-content">
                                    <div class="movie-item-title mx-3">
                                        <?= $row['Ten_phim'] ?>
                                    </div>
                                    <div class="movie-infos mx-3">
                                        <div class="movie-info">
                                            <i class="bx bxs-star"></i>
                                            <span>9.5</span>
                                        </div>
                                        <div class="movie-info">
                                            <i class="bx bxs-time"></i>
                                            <span><?= $row['Thoi_luong'] ?></span>
                                        </div>
                                        <div class="movie-info">
                                            <span>HD</span>
                                        </div>
                                        <div class="movie-info">
                                            <span>16+</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                    }
                    $conn->close();
                ?>
                <!-- END MOVIE ITEM -->
            </div>
        </div>
    </div>
    <!-- END LATEST CARTOONS SECTION -->

    <!-- SPECIAL MOVIE SECTION -->
    <div class="section">
        <div class="hero-slide-item">
            <img src="./images/transformer-banner.jpg" alt="">
            <div class="overlay"></div>
            <div class="hero-slide-item-content">
                <div class="item-content-wraper">
                    <div class="item-content-title">
                        Transformer
                    </div>
                    <div class="movie-infos mx-3">
                        <div class="movie-info">
                            <i class="bx bxs-star"></i>
                            <span>9.5</span>
                        </div>
                        <div class="movie-info">
                            <i class="bx bxs-time"></i>
                            <span>120 mins</span>
                        </div>
                        <div class="movie-info">
                            <span>HD</span>
                        </div>
                        <div class="movie-info">
                            <span>16+</span>
                        </div>
                    </div>
                    <div class="item-content-description">
                        Bộ phim đầu tiên về Autobots đến năm 2007 và thu về 709 triệu USD. 
                        Đó là sự khởi đầu của một nhượng quyền thương mại Hollywood Transformers.
                        Năm 2014 khán giả tham dự buổi ra mắt của một bộ phim bom tấn mới. Nó được 
                        gọi là "Transformers 4: Tuổi của hủy diệt." Diễn viên trong phần 4 của loạt 
                        phim đã tham gia với những người mới. Thậm chí một gợi ý để lại trên các nhân 
                        vật cũ từ ba phần đầu tiên. Làm thế nào đã làm như vậy một twist âm mưu tại các 
                        phòng vé? Và những gì chúng tôi nói với các hình ảnh, có tên mã là "The Age of hủy diệt"?
                    </div>
                    <div class="item-action">
                        <a href="#" class="btn btn-hover">
                            <i class="bx bxs-right-arrow"></i>
                            <span>Xem ngay</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SPECIAL MOVIE SECTION -->

    <!-- PRICING SECTION -->
    <div class="section">
        <div class="container">
            <div class="pricing">
                <div class="pricing-header">
                    Fl<span class="main-color">i</span>x pricing
                </div>
                <div class="pricing-list mx-2 mx-md-0">
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="pricing-box">
                                <div class="pricing-box-header">
                                    <div class="pricing-name">
                                        Basic
                                    </div>
                                    <div class="pricing-price">
                                        Free
                                    </div>
                                </div>
                                <div class="pricing-box-content">
                                    <p>Originals</p>
                                    <p>Swich plans anytime</p>
                                    <p><del>65+ top Live</del></p>
                                    <p><del>TV Channels</del></p>
                                </div>
                                <div class="pricing-box-action">
                                    <a href="thanhtoan.php" class="btn btn-hover">
                                        <span>Đăng ký ngay</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="pricing-box hightlight">
                                <div class="pricing-box-header">
                                    <div class="pricing-name">
                                        Premium
                                    </div>
                                    <div class="pricing-price">
                                        $35.99 <span>/month</span>
                                    </div>
                                </div>
                                <div class="pricing-box-content">
                                    <p>Originals</p>
                                    <p>Swich plans anytime</p>
                                    <p><del>65+ top Live</del></p>
                                    <p><del>TV Channels</del></p>
                                </div>
                                <div class="pricing-box-action">
                                    <a href="thanhtoan.php" class="btn btn-hover">
                                        <span>Đăng ký ngay</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="pricing-box">
                                <div class="pricing-box-header">
                                    <div class="pricing-name">
                                        VIP
                                    </div>
                                    <div class="pricing-price">
                                        $65.99 <span>/month</span>
                                    </div>
                                </div>
                                <div class="pricing-box-content">
                                    <p>Originals</p>
                                    <p>Swich plans anytime</p>
                                    <p><del>65+ top Live</del></p>
                                    <p><del>TV Channels</del></p>
                                </div>
                                <div class="pricing-box-action">
                                    <a href="thanhtoan.php" class="btn btn-hover">
                                        <span>Đăng ký ngay</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PRICING SECTION -->

    <?php require_once('Layout_page/Layout_footer.php');  ?>

    <script src="./toast/toast.js"></script>

    <script>
        $(document).ready(function(){
            $("#toast").click(function(){
                $(this).hide();
            });
        });
    </script>
</body>

</html>